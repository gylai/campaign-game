<!DOCTYPE html>
<html>
    <head>
        <title>Test</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Germania+One">
        <script src="https://code.jquery.com/jquery-3.5.0.min.js"integrity="sha256-xNzN2a4ltkB44Mc/Jz3pT4iU1cmeR0FkXs4pru/JxaQ="crossorigin="anonymous"></script>
        <link rel="stylesheet" href="./css/index.css">
    </head>
    <body>
        <audio controls autoplay src="audio/music1.mp3">
            <source type="audio/mpeg">
        </audio>
        <p>This is a test.</p>
    </body>
</html>