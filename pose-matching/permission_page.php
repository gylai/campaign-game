<?php
  if($_GET['hardware']=='camera')
  {
?>
      <script>
        var hardware_name = "camera";
      </script>
<?php
  }
  else
  {
?>
    <script>
      var hardware_name = "mic";
    </script>
<?php
  }
?>

<html>
<head>
<title>Grant Permission</title>
<style>
    .content{
        text-align:left;
    }
</style>
</head>
<body onload="checkbrowser()">
    <div id="cameracontent" style="display:none; text-align:center">
        <h1>You have reached this page because we cannot access your camera</h1>
        <br><br>
        <img src="images/cross-sign.png" style="top:50%; right:50%;">
        <br><br>
        <span class="content">
            <h2>INSTRUCTION FOR THE WEBSITE TO ACCESS WEBCAM</h2>
            <br>
            <span id="firefox-camera" style="display:none">
                <p>- Click the menu button and select <b style="color:blue">"Settings"</b>.</p>
                <p>- Click <b style="color:blue">"Privacy & Security"</b> from the left menu.</p>
                <p>- Scroll down to the <b style="color:blue">Permissions</b> section.</p>
                <p>- Click the <b style="color:blue">"Settings..."</b> button for the Camera option.</p>
                <p>- Firefox displays the websites with saved Allow or Block permission.</p>
                <p>- Use the <b style="color:blue">Allow/Block selector</b> to change permission for the website.</p>
                <p>- Click the <b style="color:blue">"Save Changes"</b> button.</p>
            </span>
            <span id="chrome-camera" style="display:none">
                <p>- Open Chrome.</p>
                <p>- At the top right, click <b style="color:blue">"More" > "Settings"</b>.</p>
                <p>- Under <b style="color:blue">"Privacy and security"</b>, click <b style="color:blue">Site settings</b>.</p>
                <p>- Click <b style="color:blue">Camera</b>.</p>
                <p>  Review your blocked and allowed sites.</p>
                <p>  To remove an existing exception or permission: To the right of the site, click <b style="color:blue">"Delete"</b>.</p>
                <p>  To allow a site that you already blocked: Under "Blocked," select the site's name and change the camera permission to <b style="color:blue">"Allow"</b>.</p>
            </span>
            <span id="safari-camera" style="display:none">
                <p>- On your Mac, choose <b style="color:blue">Apple menu > System Preferences</b>, click <b style="color:blue">Security & Privacy</b>, then click <b style="color:blue">Privacy</b>.</p>
                <p>- Select <b style="color:blue">Camera</b>.</p>
                <p>- Select the checkbox next to an app to allow it to access your camera.</p>
                <p>- Deselect the checkbox to turn off access for that app.</p>
                <p>- If you turn off access for an app, you’re asked to turn it on again the next time that app tries to use your camera.</p>
            </span>
            <span id="edge-camera" style="display:none">
                <p>- Open Microsoft Edge.</p>
                <p>- Navigate to the website you want to manage.</p>
                <p>- Click the <b style="color:blue">Lock or Information icon</b> next to the website link in the address bar.</p>
                <p>- Use the drop-down menus to change the permissions.</p>
                <p>- Click the <b style="color:blue">Refresh button</b> on the site to apply the changes.</p>
            </span>
        </span>
    </div>
    <div id="miccontent" style="display:none">
    <h1>You have reached this page because we cannot access your microphone</h1>
        <br><br>
        <img src="assets/image/cross-sign.png" style="text-align:center">
        <br><br>
        <span class="content">
            <h2>INSTRUCTION FOR THE WEBSITE TO ACCESS MICROPHONE</h2>
            <br>
            <span id="firefox-mic" style="display:none">
                <p>- Click the menu button and select <b style="color:blue">"Settings"</b>.</p>
                <p>- Click <b style="color:blue">"Privacy & Security"</b> from the left menu.</p>
                <p>- Scroll down to the <b style="color:blue">Permissions</b> section.</p>
                <p>- Click the <b style="color:blue">"Settings..."</b> button for the Microphone option.</p>
                <p>  Firefox displays the websites with saved Allow or Block permission.</p>
                <p>- Use the <b style="color:blue">Allow/Block selector</b> to change permission for the website.</p>
                <p>- Click the <b style="color:blue">"Save Changes"</b> button.</p>
            </span>
            <span id="chrome-mic" style="display:none">
                <p>- Open Chrome.</p>
                <p>- At the top right, click <b style="color:blue">"More" > "Settings"</b>.</p>
                <p>- Under <b style="color:blue">"Privacy and security"</b>, click <b style="color:blue">Site settings</b>.</p>
                <p>- Click <b style="color:blue">Microphone</b>.</p>
                <p>  Review your blocked and allowed sites.</p>
                <p>  To remove an existing exception or permission: To the right of the site, click <b style="color:blue">"Delete"</b>.</p>
                <p>  To allow a site that you already blocked: Under "Blocked," select the site's name and change the microphone permission to <b style="color:blue">"Allow"</b>.</p>
            </span>
            <span id="safari-mic" style="display:none">
                <p>- On your Mac, choose <b style="color:blue">Apple menu > System Preferences</b>, click <b style="color:blue">Security & Privacy</b>, then click <b style="color:blue">Privacy</b>.</p>
                <p>- Select <b style="color:blue">Microphone</b>.</p>
                <p>- Select the checkbox next to an app to allow it to access the microphone.</p>
                <p>- Deselect the checkbox to turn off access for that app.</p>
                <p>- If you turn off access for an app, you’re asked to turn it on again the next time that app tries to use your microphone.</p>
            </span>
            <span id="edge-mic" style="display:none">
                <p>- Open Microsoft Edge.</p>
                <p>- Navigate to the website you want to manage.</p>
                <p>- Click the <b style="color:blue">Lock or Information icon</b> next to the website link in the address bar.</p>
                <p>- Use the drop-down menus to change the permissions.</p>
                <p>- Click the <b style="color:blue">Refresh button</b> on the site to apply the changes.</p>
            </span>
        </span>
    </div>
</body>
</html>

<script>
    function checkbrowser()
    {
        // Firefox 1.0+
        var isFirefox = typeof InstallTrigger !== 'undefined';

        // Safari 3.0+ "[object HTMLElementConstructor]" 
        var isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof safari !== 'undefined' && window['safari'].pushNotification));

        // Internet Explorer 6-11
        var isIE = /*@cc_on!@*/false || !!document.documentMode;

        // Edge 20+
        var isEdge = !isIE && !!window.StyleMedia;

        // Chrome 1 - 79
        var isChrome = !!window.chrome && (!!window.chrome.webstore || !!window.chrome.runtime);

        // Edge (based on chromium) detection
        var isEdgeChromium = isChrome && (navigator.userAgent.indexOf("Edg") != -1);

        filtercontent(isFirefox, isSafari, isEdge, isChrome, isEdgeChromium);
    }

    function filtercontent(isFirefox, isSafari, isEdge, isChrome, isEdgeChromium)
    {
        if(hardware_name == "camera")
        {
            document.getElementById("cameracontent").style.display = "block";
            if(isFirefox)
            {
                document.getElementById("firefox-camera").style.display = "block";
            }
            else if(isSafari)
            {
                document.getElementById("safari-camera").style.display = "block";
            }
            else if(isEdge || isEdgeChromium)
            {
                document.getElementById("edge-camera").style.display = "block";
            }
            else
            {
                document.getElementById("chrome-camera").style.display = "block";
            }
        }
        else
        {
            document.getElementById("miccontent").style.display = "block";
            if(isFirefox)
            {
                document.getElementById("firefox-mic").style.display = "block";
            }
            else if(isSafari)
            {
                document.getElementById("safari-mic").style.display = "block";
            }
            else if(isEdge || isEdgeChromium)
            {
                document.getElementById("edge-mic").style.display = "block";
            }
            else
            {
                document.getElementById("chrome-mic").style.display = "block";
            }
        }
    }
</script>