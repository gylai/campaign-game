﻿<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <script>
        let START_TIME = Date.now();
    </script>
    <script type="text/javascript" src="https://unpkg.com/webcam-easy/dist/webcam-easy.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.0.min.js" integrity="sha256-xNzN2a4ltkB44Mc/Jz3pT4iU1cmeR0FkXs4pru/JxaQ=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@tensorflow/tfjs"></script>
    <script src="https://cdn.jsdelivr.net/npm/@tensorflow-models/posenet"></script>

    <!-- <link rel="stylesheet" href="index.css" /> -->
    <link rel="stylesheet" href="css/animation.css" />
    <link rel="stylesheet" href="css/main.css" />
    <link rel="stylesheet" href="css/pre-game.css" />
    <link rel="stylesheet" href="css/peri-game.css" />
    <link rel="stylesheet" href="css/post-game.css" />
    <link rel="stylesheet" href="css/image-slide-show.css" />
    <link rel="stylesheet" href="../css/loading.css">

    <title>Pose Matching</title>
    <style>
        ::-webkit-scrollbar {
            width: 0px;
            background: transparent;
        }
    </style>

    <audio id="background-music" src="audio/background_music.wav" loop></audio>
    <audio id="correct" src="audio/correct.wav"></audio>
    <audio id="next" src="audio/next.wav"></audio>
    <audio id="timesup" src="../audio/time's_up.mp3"></audio>
    <audio id="complete" src="../audio/complete.mp3"></audio>
    <audio id="perfect" src="../audio/perfect.mp3"></audio>
    <audio id="endingbgm" src="../audio/scoreboard(edited).mp3"></audio>
    <audio id="wrong" src="audio/wrong.mp3"></audio>
</head>

<body>
    <!-- <div class="loading-foreground"></div> -->

    <!-- Intro and tutorial -->
    <div class="loading">Loading...</div>

    <div class="container-fullScreen" id="container-fullScreen">
        <video class="video" id="splashScreen" autoplay muted loop>
            <source type="video/mp4" src="assets/video/splashscreen.mp4" />
        </video>

        <video class="video" id="tutorial" muted hidden>
            <source type="video/mp4" src="assets/video/tutorial.mp4" />
        </video>
        <button id="btnSkipTutorial" class="tutorial-skipButton" hidden>&nbsp&nbspContinue&nbsp&nbsp</button>
        <button id="exit" onclick="backtoMenu()" hidden>exit</button>
    </div>

    <!-- Game section -->
    <div class="container-fullScreen" id="middlegame_img" style="display:none">
        <span id="cameraslow_warning">* Your computer have a slow response for this game</span>
        <img class="middleimg" id="bg1" src="assets/image/bg-03.png">
        <img class="innerblock1" id="bg2" src="assets/image/bg-04.png">
        <img class="innerblock2" id="bg3" src="assets/image/bg-05.png">
        <span class="adsblock" id="adsblock">
            <img class="innerblock3" src="assets/image/bg-06.png">
            <span class="slideshow-container" id="slideshow-8" interval="3000">
            </span>
        </span>
        <div class="container-middlescreen" id="divInGameUI">
            <div id="game_instruction">
                <div id="camera_instruction">
                    <img class="camera_range" src="assets/image/camera_range.png">
                    <p>Recommended to play this game with a range of 1.5m from your webcam</p>
                </div>
                <div id="num_person_instruction">
                    <img class="num_person" src="assets/image/num_of_person.png">
                    <p>Please ensure ONLY ONE PLAYER with SUFFICIENT ROOM LIGHTING for the camera detection</p>
                </div>
                <button id="btnSkipInstruction" class="instruction-skipButton">&nbsp&nbspContinue&nbsp&nbsp</button>
            </div>
            <div id="divGame-Info" class="divGame-Info">
                <!--<img class="refPose" style="border: 2px solid black;transform: scaleX(-1);" src="img/102.png" id="images" />-->
                <img class="refPose" src="img/102.png" id="images" />
                <p class="sec" id="sec"><span>&nbsp;</span></p>
                <p class="score"><span>&nbsp;</span></p>
                <p class="pose" id="pose">Pose<span>&nbsp;&nbsp;&nbsp;</span><br><span id="posecount">1/10</span></p>
                <!--<button type="button" id="developer_next_button"></button>-->
                <p class="score hide"></p>
                <!--<p>Please ensure ONLY ONE player is in the camera view</p>-->
            </div>
            <div id="divGame-CameraView" class="divGame-CameraView">
                <video id="webcam" class="webcam" autoplay></video>
                <canvas id="canvas" class="cvOverlay"></canvas>
            </div>
            <div id="countdown" hidden>&nbsp;</div>

            <div style="display: none">
                <!-- play and debug button -->
                <button type="submit" id="setTimer" class="blk">Play</button>
                <button id="debug">Debug</button>
            </div>


            <div id="content">
                <!-- fps position -->
                <p><span id="fps"></span></p>
            </div>
            <div id="divGame-Banner" class="divGame-Banner">
            </div>
        </div>
    </div>

    <!-- Game completed -->
    <div class="container-fullScreen divGameCompleted" id="divGameCompleted" style="display:none">
        <img id="img-game-completed" class="img-scoreboard" src="assets/image/posematching(Complete).jpg" />
        <img id="img-game-timeup" class="img-scoreboard" src="assets/image/posematching(Timesup).jpg" />
    </div>

    <!-- Scoreboard -->
    <div class="container-fullScreen divScoreboard" id="divScoreboard" style="display:none">
        <img id="img-scoreboard-fg" class="img-scoreboard" src="assets/image/scoreboard-fg.jpg" />
        <span id="scoreboard-txt-score" class="scoreboard-txt-score" style="z-index: 5;">SCORE</span>
        <span id="scoreboard-score" class="scoreboard-score" style="z-index: 5;">2179</span>

        <a class="fb-xfbml-parse-ignore" id="fbshare-link">
            <button id="scoreboard-btnShare" class="scoreboard-btn scoreboard-btnShare" style="z-index: 5;" data-layout="button" data-size="small">SHARE</button>
        </a>
        <button id="scoreboard-btnContinue" class="scoreboard-btn scoreboard-btnContinue" style="z-index: 5;">CONTINUE</button>
    </div>

    <span id="cmname" style="display:none"></span>
    <canvas id="confetti-canvas"></canvas>

    <script src="loaditem.js"></script>
    <script src="game.js" type="module"></script>
    <script src="../confetti/confetti.js"></script>
</body>

</html>

<script async defer crossorigin="anonymous" src="https://connect.facebook.net/zh_CN/sdk.js#xfbml=1&version=v10.0" nonce="1kHGg675"></script>
<script>
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    var url = new URL(window.location.href);
    var campaign_name = url.searchParams.get("campaign_name");

    if (campaign_name) {
        document.getElementById("cmname").innerHTML = campaign_name;
    } else {
        document.getElementById("cmname").innerHTML = "Pose Matching";
    }
</script>