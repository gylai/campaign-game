document.getElementById("middlegame_img").style.display = "none";
var itemObj = getItem();
function getItem() {
    var result = {};
    $.ajax({
        url: "items.json",
        async: false,
        success: function (data) {
            result = data;
        },
        error: function (xhr, textStatus, errorMessage) { //If error, everything is default
            console.log("Error: " + xhr.status + " " + errorMessage);
            result = {
                "ads": ["assets/image/iphone_ads.png","assets/image/dota2.png","assets/image/dota2_ads.jpg"]
            };
        }
    });
    return result;
}

console.log(itemObj);
let ads_count = 0;
ads_count = itemObj.ads.length;

for(let i=0; i<ads_count; i++)
{
    var img = document.createElement("IMG");
    document.getElementById("slideshow-8").appendChild(img);
    var att = document.createAttribute("class");
    att.value = "slideshow-img fade";
    var img_source = document.createAttribute("src");
    img_source.value = itemObj.ads[i];
    var slideshow = document.getElementById("slideshow-8");
    var slides = slideshow.getElementsByTagName("IMG")[i];
    slides.setAttributeNode(att);
    slides.setAttributeNode(img_source);
}