const TIME_PLAYABLE = 60000; // ms
const REF_POSE_ID = [
    "102",
    "109",
    "101",
    "110",
    "106",
    "112",
    "111",
    "100",
    "108",
    "114",
];

var imageArray = [];
for (let i = 0; i < REF_POSE_ID.length; i++)
    imageArray.push("img/" + REF_POSE_ID[i] + ".png");
var registeredSlideshow = []; // Store image slideshow id here, to prevent duplicated onload callback
var slideshowIndex = []; // Store slideshow index by slideshow id

const videoWidth = 640;
const videoHeight = 480;
const MIN_ACCEPTABLE_KEYPOINT_SCORE = 0.6;
const MAX_ACCEPTED_ANGLE_DIFF_REG_POSE = 25; // Threshold for register pose
const webcamElement = document.getElementById("webcam");
const canvasElement = document.getElementById("canvas");
const ctx = canvasElement.getContext("2d"); // for drawing
canvasElement.width = videoWidth;
canvasElement.height = videoHeight;
const backgroundMusic = document.getElementById("background-music");
const TIME_COUNT_DOWN_TO_START = 5000; // ms

const TIME_GAME_IS_ENDING = 15000; // ms, the threshold to tell the game is ending soon
const TIME_CHANGE_REF_POSE_COOL_DOWN = 1000; // ms, the threshold to tell the game is ending soon
const TIME_REGISTER_POSE = 1500; // ms, time to register a pose

const numReferencePose = REF_POSE_ID.length;
// let numReferencePose = 7;
const DEBUG = false;

//load camera
const webcam = new Webcam(webcamElement);
webcamElement.width = videoWidth;
webcamElement.height = videoHeight;
var iscameraon;
var iscameraasked = false;
let apromise;

//timer
let previousTime = 0;
let time_forceshutdown = 0;

//loop image
let gameStart = false;

let gameCompleted = true;
//store user input id purpose,
let userid = null;
let startTime;
let userName = null;

//Maintance the post
let maintanceTime = 0;
let coolDownTime = 0; // cool down timer for change reference pose
let loading = 0;

//  let file = {};
//counts all angle

//ref data
let reference_poses = []; //data from json file
let ref_pose_angle = []; // photo angle points
let accuracy = 0.0;
let total_accuracy = 0.0;
let final_accuracy = 0.0;
let final_accuracy_array = [];

//
let currentRefPose = -1;
let ref_ID = -1;
//pose matching
let lastdiff = [];
let angleDiff = [];
let timestamp = 0;
let current_ref_pose_for_render = [];
let TIME_TOTAL;
let pose_successful = 0;
let timetaken_each_pose = [];

//score
let maxScore = 100;
// let maxScore = 1;
let totalScore = 0;
// skeleton and keypoints color
let color = "#FFFFFF";
let poseRegistered = false;

// array to store data
let files = {
    gameSetting: [],
    gamePlay: [],
    performance: [],
};

document.getElementById("debug").disabled = true;

let frame = 0;
let net;
let indexxx = 0;
let Flag_ProcessFrame = true; // The process of pose estimation only conduct once at a time
let center = (videoWidth + 1) / 2; // center of x-axis
let c_left = Math.floor(center); // the left pixel of center of x-axis
let c_right = Math.ceil(center); // the right pixel of center of x-axis
let start;
let fps;
let skeletonPoints;
let skeletonPointsDistance; // the distance between points
let ref_current_angle_differences = []; //store the differences between ref and sample angle
let INIT_TIME;
let Flag_ProcessLogic = false; // Only conduct the processes that based on the skeleton when the new skeleton is presented.
let ignoreSkeletonPoint = [0, 1, 2, 3, 4, 11, 12, 13, 14, 15, 16]; // ignore head and lower body.
let skeletonDescription = [];
skeletonDescription[0] = "nose";
skeletonDescription[1] = "leftEye";
skeletonDescription[2] = "rightEye";
skeletonDescription[3] = "leftEar";
skeletonDescription[4] = "rightEar";
skeletonDescription[5] = "leftShoulder";
skeletonDescription[6] = "rightShoulder";
skeletonDescription[7] = "leftElbow";
skeletonDescription[8] = "rightElbow";
skeletonDescription[9] = "leftWrist";
skeletonDescription[10] = "rightWrist";
skeletonDescription[11] = "leftHip";
skeletonDescription[12] = "rightHip";
skeletonDescription[13] = "leftKnee";
skeletonDescription[14] = "rightKnee";
skeletonDescription[15] = "leftAnkle";
skeletonDescription[16] = "rightAnkle";
let ignoreSkeletonPart = [];
for (var i = 0; i < ignoreSkeletonPoint.length; i++)
    ignoreSkeletonPart.push(skeletonDescription[ignoreSkeletonPoint[i]]);

//console.log("Ignored body parts");
//console.log(ignoreSkeletonPart);


// document.getElementById("debug").addEventListener("click", function () {
//   debug = !debug;
//   if (debug) {
//     document.getElementById("debug").innerHTML = "Debug ON";
//   } else {
//     document.getElementById("debug").innerHTML = "Debug OFF";
//     document.getElementById("fps").innerHTML = "";
//   }
// });

// document.getElementById("setTimer").addEventListener("click", function () {
//   userid = localStorage.getItem("Usergmail");
//   userName = localStorage.getItem("Username");
//   startStore = false;
//   document.getElementById("setTimer").style.visibility = "hidden";
//   document.getElementById("debug").style.visibility = "hidden";

//   files.library.push({ Name: "Posenet", Achiterture: "Resnet50" });
//   files.UserInput.push({ Name: userName, Gmail: userid });

//   startTime = Date.now();
//   timer = true;
// });
document.getElementById("btnSkipTutorial").onclick = function() {
    document.getElementById("btnSkipTutorial").style.display = "none";
    document.getElementById("exit").style.display = "none";
    start_instruction();
};

document.getElementById("exit").onclick = function() {
    parent.location.href = "https://fuyoh-ads.com/covid-19/dashboard/";
};

window.onresize = screenResize;

function screenResize() {
    // w: width, h: height
    let w = window.innerWidth;
    let h = window.innerHeight;
    let targetWidth = w;
    if (h >= (w / 16) * 9) {
        $("body").css({
            width: "100vw",
            height: "calc(100vw/16*9)",
        });
    } else {
        (targetWidth = (h / 9) * 16),
        $("body").css({
            width: "calc(100vh/9*16)",
            height: "100vh",
        });
    }
    document.documentElement.style.setProperty(
        "--target-width",
        targetWidth + "px"
    );
}

window.addEventListener("load", function(event) {
    screenResize();
    document.getElementById("splashScreen").style.display = "block";
    document.getElementById("divInGameUI").style.display = "none";
    document.getElementById("divGameCompleted").style.display = "none";
    document.getElementById("divScoreboard").style.display = "none";
    document.getElementById("middlegame_img").style.display = "none";

    userid = localStorage.getItem("Usergmail");
    userName = localStorage.getItem("Username");
    gameCompleted = false;
    document.getElementById("setTimer").style.visibility = "hidden";
    document.getElementById("debug").style.visibility = "hidden";
    document.getElementById("sec").style.display = "block";
    files.gameSetting.push({ Library: "Posenet", Achiterture: "Resnet50" });
    iscameraasked = true;
    startCamera();
});

function initSlideShow() {
    $("[id^=slideshow]").each(function() {
        slideShow(this.id, $(this).attr("interval"));
    });
}

function slideShow(id, time) {
    if (!registeredSlideshow.includes(id)) {
        // console.log("register widget: " + id);
        var obj = document.getElementById(id);
        if (obj.getElementsByClassName("slideshow-img").length == 0) {
            //console.log(id + " " + 0);
            return;
        }
        registeredSlideshow.push(id);
        slideshowIndex[id] = 0;
        const widgetInterval = setInterval(function() {
            if (obj) {
                var slides = obj.getElementsByClassName("slideshow-img");
                for (let i = 0; i < slides.length; i++) {
                    slides[i].style.display = "none";
                }
                slideshowIndex[id]++;
                if (slideshowIndex[id] >= slides.length) slideshowIndex[id] = 0;
                slides[slideshowIndex[id]].style.display = "block";
                // console.log("Slideshow image change: " + id + " (" + new Date().toLocaleString()  + ")");
            } else {
                clearInterval(widgetInterval);
                registeredSlideshow = registeredSlideshow.filter(function(item) {
                    return item !== id;
                });
                //console.log("Current slideshow: " + registeredSlideshow);
            }
        }, time); // repeat the function refreshIt in time (ms)
        //console.log("Register slideshow: " + id + " " + (time/1000) + "sec");
    }
}

function addZero(num) {
    return num < 10 ? `0${num}` : num;
}

var worker = new Worker("estimatepose.js");
worker.addEventListener(
    "message",
    function(e) {
        // console.log('Single: ', e.data);
        //console.log(time_forceshutdown);
        skeletonPoints = e.data;
        fps = 1000 / (Date.now() - start);
        if (DEBUG) {
            //document.getElementById("fps").innerHTML = "FPS: " + fps;
            //console.log("fps for skeleton detection: " + fps);
        }
        if (fps >= 1) {
            // if camera is smooth, trigger this function
            time_forceshutdown = 0;
        }
        Flag_ProcessFrame = true;
        Flag_ProcessLogic = true;
    },
    false
);

function start_instruction() {
    document.getElementById("tutorial").pause();
    document.getElementById("tutorial").style.display = "none";
    document.getElementById("game_instruction").style.display = "block";
    document.getElementById("divInGameUI").style.display = "block";
    document.getElementById("middlegame_img").style.display = "block";
    document.getElementById("bg1").style.display = "block";
    document.getElementById("bg2").style.display = "none";
    document.getElementById("bg3").style.display = "none";
    document.getElementById("adsblock").style.display = "none";
    document.getElementById("divGame-Info").style.display = "none";
    document.getElementById("divGame-CameraView").style.display = "none";
    document.getElementById("btnSkipInstruction").onclick = function() {
        document.getElementById("game_instruction").style.display = "none";
        start_game();
    };
}

function start_game() {
    getRefData();
    startTime = Date.now();
    document.getElementById("bg2").style.display = "block";
    document.getElementById("bg3").style.display = "block";
    document.getElementById("adsblock").style.display = "block";
    initSlideShow();
    document.getElementById("divGame-Info").style.display = "block";
    document.getElementById("divGame-CameraView").style.display = "block";
    setInterval(function() {
        time_forceshutdown++;
    }, 1000);
    INIT_TIME = Date.now() - START_TIME;
    // console.log("INIT_TIME " + INIT_TIME);
    TIME_TOTAL = TIME_COUNT_DOWN_TO_START + TIME_PLAYABLE + INIT_TIME;
    // console.log("TIME_TOTAL " + TIME_TOTAL);
    requestAnimationFrame(main);
}

/*function startCamera() {
    if(navigator.mediaDevices.getUserMedia)
    {
        navigator.mediaDevices.getUserMedia({ video : true})
        .then(function(stream){
            video_cam = stream;
            webcam
            .start()
            .then((result) => {
            console.log("webcam started");
            setTimeout(function() {
                document.getElementById("splashScreen").style.display = "none";
                document.getElementById("tutorial").style.display = "block";
                document.getElementById("tutorial").play();
                document.getElementById("tutorial").onended =  function(){
                    document.getElementById("tutorial").play();
                    document.getElementById("btnSkipTutorial").style.display = "block";
                    document.getElementById("btnSkipTutorial").hidden = false;
                }
                //document.getElementById("btnSkipTutorial").hidden = false;
            }, 1000) // splash screen at least 3 seconds
            })
            iscameraon = true;
        })
        .catch((err) => {
            console.log(err);
            console.log("webcam ended")
            iscameraon = false;
            //window.parent.wsRequireHardwareMessage("camera");
            window.alert(
                "Please turn on your camera and visit this page agian\n" +
                "Tips: Click on the mic / video button by your URL"
            );
            window.location.href = "/";  
        });
    }
}*/

let loadPercentages = 0;
setTimeout(function() {
    if (loadPercentages < 99) {
        alert("We detect you are having a slow connection, you might experience longer loading time or abnormal gaming experience, please find an alternative reliable connectivity.");
    }
}, 30000);
let checkbuffer = function() {
    if (loadPercentages < 99) {
        let range = 0;
        let bf = this.buffered;
        let time = this.currentTime;
        let loadPercentage = bf.end(range) / this.duration * 100;
        loadPercentages = loadPercentage;
    } else {
        document.getElementById("tutorial").removeEventListener('timeupdate', checkbuffer);
    }
};
document.getElementById("tutorial").addEventListener('timeupdate', checkbuffer);

async function startCamera() {
    apromise = null;
    try {
        apromise = await navigator.mediaDevices.getUserMedia({ video: true });
        //console.log(apromise);
        webcam.start().then((result) => {
            //console.log("webcam started");
            setTimeout(function() {
                document.getElementById("splashScreen").style.display = "none";
                $(".loading").hide();
                backgroundMusic.play();
                document.getElementById("tutorial").style.display = "block";
                document.getElementById("tutorial").play();

                document.getElementById("tutorial").onended = function() {
                    document.getElementById("tutorial").play();
                    document.getElementById("exit").style.display = "block";
                    document.getElementById("exit").hidden = false;
                    document.getElementById("btnSkipTutorial").style.display = "block";
                    document.getElementById("btnSkipTutorial").hidden = false;
                };
                //document.getElementById("btnSkipTutorial").hidden = false;
            }, 1000); // splash screen at least 3 seconds
        });
        iscameraon = true;
    } catch (err) {
        apromise = null;
        //console.log(apromise);
        //console.log(err);
        //console.log("webcam ended")
        iscameraon = false;
        //window.parent.wsRequireHardwareMessage("camera");
        /*window.alert(
                "Please turn on your camera and visit this page agian\n" +
                "Tips: Click on the mic / video button by your URL"
            );*/
        parent.location.href = "../error/hardware_error_page.php?hardware=camera";
    }
}

function checkCamera() {
    if (apromise != null) {
        if (apromise.active == true) {
            //do nothing if camera is detected and function well
            iscameraon = true;
        } else {
            /*window.alert(
                      "Please turn on your camera and visit this page agian\n" +
                      "Tips: Click on the mic / video button by your URL"
                  );*/
            parent.location.href = "../error/hardware_error_page.php?hardware=camera";
        }
    } else {
        iscameraon = false;
    }
}

function main(time) {
    setTimeout(function() {
        /*Webcam.on( 'error', function(err) {
            window.alert(
                "Please turn on your camera and visit this page agian\n" +
                "Tips: Click on the mic / video button by your URL"
            );
            window.location.href = "/";
            } );
            Webcam.attach('#webcam');*/
        if (gameCompleted == false) {
            checkCamera();
            //console.log(iscameraon);
            if (iscameraon == true) {
                if (time_forceshutdown > 10) {
                    document.getElementById("cameraslow_warning").style.display =
                        "inline-block";
                }
                if (Math.round(totalScore * maxScore) <= 0) {
                    document.getElementsByClassName("score")[0].innerHTML =
                        "Score<br><span id='score'>" +
                        Math.round(totalScore * maxScore) +
                        "</span>";
                }
                if (time > TIME_TOTAL) {
                    document.getElementById("sec").innerHTML =
                        "Time<br><span id='sec_count'>" + 0 + "</span>";
                    gameCompleteAction("timeup");
                } else {
                    if (TIME_TOTAL - time <= TIME_GAME_IS_ENDING && gameStart) {
                        backgroundMusic.playbackRate = 1.5;
                    }
                    if (Math.ceil(TIME_TOTAL - time) >= TIME_PLAYABLE) {
                        document.getElementById("sec").innerHTML =
                            "Time<br><span id='sec_count'>" +
                            Math.ceil(TIME_PLAYABLE / 1000) +
                            "</span>";
                    } else {
                        document.getElementById("sec").innerHTML =
                            "Time<br><span id='sec_count'>" +
                            Math.ceil((TIME_TOTAL - time) / 1000) +
                            "</span>";
                    }
                }

                if (Flag_ProcessFrame) {
                    start = Date.now();
                    Flag_ProcessFrame = false;

                    // webcam feed to canvas, then get image from the canvas
                    var canvas = document.createElement("canvas");
                    canvas.width = webcamElement.width;
                    canvas.height = webcamElement.height;
                    canvas.getContext("2d").drawImage(webcamElement, 0, 0);
                    let img = canvas
                        .getContext("2d")
                        .getImageData(0, 0, videoWidth, videoHeight);

                    worker.postMessage(img);
                }

                if (Flag_ProcessLogic) {
                    //
                    //
                    //		PUT ALL THE PROCESSING HERE!!!!
                    //		chg image, save data, all here.

                    // countdown or timer event
                    let deltaTime = time - previousTime;
                    previousTime = time;
                    if (!gameStart && !gameCompleted && time < TIME_TOTAL) {
                        if (time > TIME_COUNT_DOWN_TO_START + INIT_TIME) {
                            document.getElementsByClassName("score")[0].style.display =
                                "block";
                            document.getElementsByClassName("score")[1].style.display =
                                "block";
                            changeReferencePose();
                            gameStart = true;

                        } else {
                            //document.getElementById("sec").innerHTML = "&nbsp;";
                            ctx.clearRect(0, 0, videoWidth, videoHeight);
                            ctx.beginPath();
                            ctx.fillStyle = "rgba(0,0,0,0.7)";
                            ctx.fillRect(0, 0, videoWidth, videoHeight);
                            ctx.font = "830% Arial";
                            ctx.fillStyle = "white";
                            ctx.fillText(
                                Math.ceil((TIME_COUNT_DOWN_TO_START - time + INIT_TIME) / 1000),
                                videoWidth / 2 - 30,
                                videoWidth / 2 - 30
                            );
                            ctx.closePath();
                            ctx.stroke();
                        }
                    }

                    if (gameStart) {
                        timestamp = Date.now();
                        // Save skeleton data
                        let file = {
                            RefPose_ID: ref_ID,
                            Timestamp: timestamp,
                            Keypoints: skeletonPoints.keypoints,
                        };
                        files.gamePlay.push(file);

                        // Pose matching
                        poseMatching(deltaTime, previousTime);

                        // Draw skeleton on screen
                        render(skeletonPoints);
                        if (currentRefPose >= 0)
                        //drawReferencePose(current_ref_pose_for_render);

                        // Draw score on screen
                            document.getElementsByClassName("score")[0].innerHTML =
                            "Score<br><span id='score'>" +
                            Math.round(totalScore * maxScore) +
                            "</span>";
                    }

                    // render skeletons
                    Flag_ProcessLogic = false;
                }
            }
        }
        requestAnimationFrame(main);
    }, 11); // 1000(ms) / 90(camera fps * 3) = 11.11
}

function inverseSkeletonXaxis(skeletonPoints, imageWidth) {
    for (let i = 0; i < skeletonPoints.keypoints.length; i++) {
        let newX, offset;
        let x = skeletonPoints.keypoints[i].position.x;
        if (x > center) {
            offset = imageWidth - x;
            newX = 1 + offset;
        } //(x < center)
        else {
            offset = c_left - x;
            newX = c_right + offset;
        }
        skeletonPoints.keypoints[i].position.x = newX;
    }
    return skeletonPoints;
}

function debugRender(poses) {
    for (let i = 0; i < poses.keypoints.length; i++) {
        if (ignoreSkeletonPoint.includes(i)) continue;

        let x = poses.keypoints[i].position.x;
        let y = poses.keypoints[i].position.y;
        let s = poses.keypoints[i].score.toFixed(2);

        ctx.beginPath();
        ctx.font = "15px Arial";
        if (s >= MIN_ACCEPTABLE_KEYPOINT_SCORE) {
            ctx.font = "15px Arial";
            ctx.fillStyle = "red";
            ctx.fillText(s, x + videoWidth * 0.01, y);
        } else {
            ctx.fillStyle = "#ffb3b3";
            ctx.fillText(s, x + videoWidth * 0.01, y);

            ctx.arc(x, y, 5, 0, 2 * Math.PI);
            ctx.fillStyle = "crimson";
            ctx.fill();
        }
    }
}

function storeData(files, userName, startTime) {
    $.ajax({
        type: "POST",
        url: "storeData.php",
        data: {
            keypoint: JSON.stringify(files, null, 3),
            id: "userName",
            currentTime: startTime,
        },
    });
}

function openDataBase(userName, userid, startTime, totalScore, data) {
    $.ajax({
        type: "POST",
        // url: "https://fuyoh-ads.com/pose-matching/database.php",
        url: "database.php",
        data: {
            // User_Name: userName,
            User_Name: "userName",
            // User_gmail: userid,
            User_gmail: "userid",
            Time: startTime,
            Score: totalScore,
            MatchData: JSON.stringify(data),
        },
    });
}

function render(poses, time) {
    let poses_for_render = inverseSkeletonXaxis(poses, videoWidth);
    ctx.clearRect(0, 0, videoWidth, videoHeight);
    drawSkeleton(poses_for_render);
    if (DEBUG) {
        debugRender(poses_for_render);
    }
}

function drawReferencePose(pose) {
    // y max = 1300 or 1400
    for (let i = 0; i < pose.keypoints.length; i++) {
        if (ignoreSkeletonPoint.includes(i)) continue;

        let x = pose.keypoints[i].position.x / 10;
        let y = pose.keypoints[i].position.y / 10;
        ctx.beginPath();
        ctx.arc(x, y, 5, 0, 2 * Math.PI);
        ctx.fillStyle = "Red";
        ctx.fill();
        ctx.closePath();
    }

    var skeleton = posenet.getAdjacentKeyPoints(pose.keypoints);
    for (let j = 0; j < skeleton.length; j++) {
        var segment = skeleton[j];
        if (
            ignoreSkeletonPart.includes(segment[0].part) ||
            ignoreSkeletonPart.includes(segment[1].part)
        )
            continue;

        ctx.beginPath();
        ctx.strokeStyle = "#00FFFF";
        ctx.moveTo(segment[0].position.x / 10, segment[0].position.y / 10);
        ctx.lineTo(segment[1].position.x / 10, segment[1].position.y / 10);
        ctx.stroke();
    }
}

function drawSkeleton(poses) {
    var drawpose = true;
    if (coolDownTime <= TIME_CHANGE_REF_POSE_COOL_DOWN && currentRefPose > 0) {
        ctx.clearRect(0, 0, videoWidth, videoHeight);
        drawpose = false;
        //color = rgba(0,0,0,0.5);
    } else {
        drawpose = true;
    }

    // draw all the key points
    if (drawpose) {
        for (let i = 0; i < poses.keypoints.length; i++) {
            if (ignoreSkeletonPoint.includes(i)) continue;

            let x = poses.keypoints[i].position.x;
            let y = poses.keypoints[i].position.y;
            let s = poses.keypoints[i].score;
            //let circlepoint = new Path2D();

            if (s >= MIN_ACCEPTABLE_KEYPOINT_SCORE || DEBUG) {
                ctx.beginPath();
                ctx.arc(x, y, 15, 0, 2 * Math.PI);
                //ctx.fillStyle = color;
                ctx.strokeStyle = color; //change drawing line color in webcam
                ctx.shadowColor = color;
                ctx.shadowBlur = 10;
                ctx.lineWidth = 8;
                ctx.stroke(); //plot the point
                //ctx.fill();
                ctx.closePath();
            }
        }
    }

    if (coolDownTime <= TIME_CHANGE_REF_POSE_COOL_DOWN && currentRefPose > 0) {
        ctx.beginPath();
        ctx.shadowColor = "#000000";
        ctx.shadowBlur = 0;
        ctx.fillStyle = "rgba(0,0,0,0.7)";
        ctx.fillRect(0, 0, videoWidth, videoHeight);
        ctx.font = "1300% MachineGunk";
        ctx.fillStyle = "#FFFFFF";
        ctx.fillText("NEXT", videoWidth / 2 - 170, videoWidth / 2 + 10);
        ctx.closePath();
    }

    // // draw lines to connect head points
    // ctx.beginPath();
    // ctx.strokeStyle = color;
    // if (poses.keypoints[0].score >= 0.5 && poses.keypoints[1].score >= 0.5) {
    //   ctx.lineTo(poses.keypoints[0].position.x, poses.keypoints[0].position.y);
    //   ctx.lineTo(poses.keypoints[1].position.x, poses.keypoints[1].position.y);
    // }

    // // if (!ignoreSkeletonPoint.includes(0) && !ignoreSkeletonPoint.includes(2) && poses.keypoints[0].score >= 0.5 && poses.keypoints[2].score >= 0.5) {
    // if (poses.keypoints[0].score >= 0.5 && poses.keypoints[2].score >= 0.5) {
    //   ctx.lineTo(poses.keypoints[0].position.x, poses.keypoints[0].position.y);
    //   ctx.lineTo(poses.keypoints[2].position.x, poses.keypoints[2].position.y);
    // }

    // if (poses.keypoints[1].score >= 0.5 && poses.keypoints[3].score >= 0.5) {
    //   ctx.lineTo(poses.keypoints[1].position.x, poses.keypoints[1].position.y);
    //   ctx.lineTo(poses.keypoints[3].position.x, poses.keypoints[3].position.y);
    //   ctx.lineTo(poses.keypoints[1].position.x, poses.keypoints[1].position.y);
    //   ctx.lineTo(poses.keypoints[0].position.x, poses.keypoints[0].position.y);
    // }
    // if (poses.keypoints[2].score >= 0.5 && poses.keypoints[4].score >= 0.5) {
    //   ctx.lineTo(poses.keypoints[2].position.x, poses.keypoints[2].position.y);
    //   ctx.lineTo(poses.keypoints[4].position.x, poses.keypoints[4].position.y);
    //   //  ctx.lineTo(poses.keypoints[2].position.x, poses.keypoints[2].position.y);
    // }
    // ctx.stroke();
    // ctx.closePath();

    // // draw lines to connect body points, if pose is registered.
    // if (!poseRegistered)
    //   return;

    var skeleton = posenet.getAdjacentKeyPoints(poses.keypoints);
    // 0 > 1; 0 > 2; 1 > 3; 2 > 4;
    if (drawpose) {
        for (let j = 0; j < skeleton.length; j++) {
            var segment = skeleton[j];
            if (
                ignoreSkeletonPart.includes(segment[0].part) ||
                ignoreSkeletonPart.includes(segment[1].part) ||
                segment[0].score < MIN_ACCEPTABLE_KEYPOINT_SCORE ||
                segment[1].score < MIN_ACCEPTABLE_KEYPOINT_SCORE
            )
                continue;

            var betweenVec = new vec2(
                segment[1].position.x - segment[0].position.x,
                segment[1].position.y - segment[0].position.y
            );
            betweenVec.normalize();

            var p1x = segment[0].position.x + 15 * betweenVec.x;
            var p1y = segment[0].position.y + 15 * betweenVec.y;

            var p2x = segment[1].position.x - 15 * betweenVec.x;
            var p2y = segment[1].position.y - 15 * betweenVec.y;

            ctx.beginPath();
            ctx.strokeStyle = color;
            //var gradient = ctx.createLinearGradient(segment[0].position.x, segment[0].position.y,segment[1].position.x, segment[1].position.y)
            ctx.moveTo(p1x, p1y);
            ctx.lineTo(p2x, p2y);
            ctx.stroke();
            //ctx.closePath();

            // // set color based on angle differences
            // let part1Idx = skeletonDescription.indexOf(segment[0].part);
            // let part2Idx = skeletonDescription.indexOf(segment[1].part);
            // let index = "'" + (Math.pow(2, part1Idx) + Math.pow(2, part2Idx)) + "'";
            // ctx.beginPath();
            // if(!ref_current_angle_differences[index])
            //   ctx.strokeStyle = 'rgb(255, 0, 0)';
            // else if (ref_current_angle_differences[index] <= 10)
            //   ctx.strokeStyle = 'rgb(0, 255, 0)';
            // else
            //   ctx.strokeStyle = 'rgb(255, 0, 0)';

            //   // ctx.strokeStyle = 'rgb(255 * ' + gradient + ', 0, 0)';
            // ctx.moveTo(segment[0].position.x, segment[0].position.y);
            // ctx.lineTo(segment[1].position.x, segment[1].position.y);
            // ctx.stroke();
        }
    }
}

function vec2(x, y) {
    this.length = function() {
        return Math.sqrt(this.x * this.x + this.y * this.y);
    };
    this.normalize = function() {
        var scale = this.length();
        this.x /= scale;
        this.y /= scale;
    };
    this.x = x;
    this.y = y;
}

function calculateAngle(pose) {
    let point = pose.keypoints;
    let angle = [];
    if (!ignoreSkeletonPoint.includes(5) && !ignoreSkeletonPoint.includes(7))
        angle["'" + (Math.pow(2, 5) + Math.pow(2, 7)) + "'"] = Angle(
            point[5].position,
            point[7].position
        );
    if (!ignoreSkeletonPoint.includes(7) && !ignoreSkeletonPoint.includes(9))
        angle["'" + (Math.pow(2, 7) + Math.pow(2, 9)) + "'"] = Angle(
            point[7].position,
            point[9].position
        );
    if (!ignoreSkeletonPoint.includes(6) && !ignoreSkeletonPoint.includes(8))
        angle["'" + (Math.pow(2, 6) + Math.pow(2, 8)) + "'"] = Angle(
            point[6].position,
            point[8].position
        );
    if (!ignoreSkeletonPoint.includes(8) && !ignoreSkeletonPoint.includes(10))
        angle["'" + (Math.pow(2, 8) + Math.pow(2, 10)) + "'"] = Angle(
            point[8].position,
            point[10].position
        );
    if (!ignoreSkeletonPoint.includes(11) && !ignoreSkeletonPoint.includes(13))
        angle["'" + (Math.pow(2, 11) + Math.pow(2, 13)) + "'"] = Angle(
            point[11].position,
            point[13].position
        );
    if (!ignoreSkeletonPoint.includes(13) && !ignoreSkeletonPoint.includes(15))
        angle["'" + (Math.pow(2, 13) + Math.pow(2, 15)) + "'"] = Angle(
            point[13].position,
            point[15].position
        );
    if (!ignoreSkeletonPoint.includes(12) && !ignoreSkeletonPoint.includes(14))
        angle["'" + (Math.pow(2, 12) + Math.pow(2, 14)) + "'"] = Angle(
            point[12].position,
            point[14].position
        );
    if (!ignoreSkeletonPoint.includes(14) && !ignoreSkeletonPoint.includes(16))
        angle["'" + (Math.pow(2, 14) + Math.pow(2, 16)) + "'"] = Angle(
            point[14].position,
            point[16].position
        );
    return angle;
}

function Angle(point1, point2) {
    let angle = 0;
    let m = 0;
    let xdiff = 0;
    let ydiff = 0;
    xdiff = point1.x - point2.x;
    ydiff = point1.y - point2.y;

    if (xdiff != 0) {
        if (ydiff > 0 && xdiff > 0) {
            m = ydiff / xdiff;
            angle = (Math.atan(m) * 180) / Math.PI + 180;
        }

        if (ydiff < 0 && xdiff > 0) {
            m = ydiff / xdiff;
            angle = (Math.atan(m) * 180) / Math.PI + 180;
        }

        if (ydiff < 0 && xdiff < 0) {
            m = ydiff / xdiff;
            angle = (Math.atan(m) * 180) / Math.PI;
        }

        if (ydiff > 0 && xdiff < 0) {
            m = ydiff / xdiff;
            angle = (Math.atan(m) * 180) / Math.PI + 360;
        }

        if (ydiff == 0 && xdiff > 0) {
            angle = 180;
        }

        if (ydiff == 0 && xdiff < 0) {
            angle = 0;
        }
    } else {
        if (ydiff < 0) {
            angle = 90;
        }

        if (ydiff > 0) {
            angle = 270;
        }
    }
    return angle;
}

function getRefData() {
    fetch("refpose_list_upper_body.json")
        .then(function(resp) {
            return resp.json();
        })
        .then(function(data) {
            reference_poses = data.data;
        });
}

function differentAngle(sam_angle, ref) {
    let dif_angle = 0;

    dif_angle = Math.abs(ref - sam_angle);

    if (dif_angle > 180) {
        dif_angle = 360 - dif_angle;
    }

    return dif_angle;
}

function poseMatching(deltaTime, previousTime, time) {
    coolDownTime += deltaTime;
    let current_angle = [];
    poseRegistered = true;

    // Check if skeleton keypoints' score is above the treshold
    for (let i = 0; i < skeletonPoints.keypoints.length && poseRegistered; i++)
        if (!ignoreSkeletonPoint.includes(i) &&
            skeletonPoints.keypoints[i].score < MIN_ACCEPTABLE_KEYPOINT_SCORE
        )
            poseRegistered = false;

        // Check if skeleton keypoints are in correct length
    if (poseRegistered) {
        skeletonPointsDistance = calculateSkeletonPointsDistance(skeletonPoints);
        poseRegistered = checkSkeletonValidity(skeletonPointsDistance);
    }
    if (poseRegistered) current_angle = calculateAngle(skeletonPoints);

    // if current number of angle less than reference, pose is not match
    if (Object.keys(current_angle).length != Object.keys(ref_pose_angle).length)
        poseRegistered = false;

    if (poseRegistered) {
        var count = 0;
        final_accuracy = 0;
        total_accuracy = 0;
        accuracy = 0;
        Object.keys(current_angle).forEach(function(key) {
            ref_current_angle_differences[key] = differentAngle(
                ref_pose_angle[key],
                current_angle[key]
            );
            //console.log(key + " " + current_angle[key] + " / " + ref_pose_angle[key]);
            //console.log(ref_current_angle_differences[key]);
            accuracy = ((ref_current_angle_differences[key] / 180) * 100).toFixed(2);
            accuracy = 100 - accuracy;
            total_accuracy = parseFloat(total_accuracy) + parseFloat(accuracy);
            count++;
        });
        final_accuracy = (total_accuracy / count).toFixed(2);
    }

    Object.keys(current_angle).forEach(function(key) {
        if (poseRegistered) {
            angleDiff[key] = Math.abs(
                ref_current_angle_differences[key] - lastdiff[key]
            );
            lastdiff[key] = ref_current_angle_differences[key];
            poseRegistered = angleDiff[key] <= MAX_ACCEPTED_ANGLE_DIFF_REG_POSE;
            // console.log(key + ": " + current_angle[key] + ref_pose_angle[key]);
        }
    });

    if (poseRegistered) {
        color = "#00FFFF";
        if (coolDownTime >= TIME_CHANGE_REF_POSE_COOL_DOWN)
            maintanceTime += deltaTime;

        if (maintanceTime >= TIME_REGISTER_POSE) {
            let currentScore = 0;
            Object.keys(current_angle).forEach(function(key) {
                let score = (180 - ref_current_angle_differences[key]) / 180;
                currentScore += score;
            });

            currentScore = currentScore / Object.keys(current_angle).length;
            if (currentScore >= 0.75) {
                var correct = document.getElementById("correct");
                correct.play();
                totalScore += currentScore;
            } else {
                var wrong = document.getElementById("wrong");
                wrong.play();
            }

            //console.log("Score: " + currentScore * maxScore);
            pose_successful += 1;
            timetaken_each_pose.push(previousTime / 1000);
            //console.log(previousTime);
            final_accuracy_array.push(final_accuracy);

            changeReferencePose();
            var next = document.getElementById("next");
            next.play();
            maintanceTime = 0;
            coolDownTime = 0;
        }
    } else {
        maintanceTime = 0;
        color = "#FFFFFF";
    }
}

//for developer purpose only
/*document.getElementById("developer_next_button").addEventListener("click", function(){
    ctx.beginPath();
    ctx.shadowColor = '#000000';
    ctx.shadowBlur = 0;
    ctx.fillStyle = "rgba(0,0,0,0.7)";
    ctx.fillRect(0, 0, videoWidth, videoHeight);
    ctx.font = "1300% MachineGunk";
    ctx.fillStyle = "#FFFFFF";
    ctx.fillText("NEXT", videoWidth / 2 - 170, videoWidth / 2 + 10);
    ctx.closePath();
    color = "red";
    let currentScore = 1.0;
    totalScore += currentScore;
    document.getElementsByClassName("score")[0].innerHTML =
        "Score<br><span id='score'>" + Math.round(totalScore * maxScore) + "</span>";
    currentScore = 0;
    var wrong = document.getElementById("wrong");
    wrong.play();
    changeReferencePose();
    var next = document.getElementById("next");
    next.play();
    console.log("Score: " + currentScore * maxScore);
});*/

// Change image purpose
let myImage = document.getElementById("images");
// inverse preview image

function changeReferencePose() {
    currentRefPose += 1;
    document.getElementById("posecount").innerHTML = currentRefPose + 1 + "/10";
    if (currentRefPose >= numReferencePose) {
        /*ctx.clearRect(0, 0, videoWidth, videoHeight);
            ctx.beginPath();
            ctx.fillStyle = "rgba(0,0,0,0.9)";
            ctx.fillRect(0, 0, videoWidth, videoHeight);
            ctx.font = "750% Arial";
            ctx.fillStyle = "red";
            ctx.fillText("Completed!", 5, videoWidth / 2 - 30);
            ctx.closePath();
            ctx.stroke();*/
        gameCompleteAction("completed");
    } else {
        let index;
        for (let i = 0; i < reference_poses.length; i++)
            if (reference_poses[i].refpose_ID == REF_POSE_ID[currentRefPose]) {
                index = i;
                break;
            }
        ref_pose_angle = calculateAngle(reference_poses[index]);
        current_ref_pose_for_render = inverseSkeletonXaxis(
            reference_poses[index],
            1920
        );
        ref_ID = reference_poses[index].refpose_ID;
        myImage.setAttribute("src", imageArray[currentRefPose]);
    }
}

function calculateSkeletonPointsDistance(skeletonPoints) {
    let distance = [];
    let skeleton = posenet.getAdjacentKeyPoints(skeletonPoints.keypoints);
    for (let i = 0; i < skeleton.length; i++) {
        var segment = skeleton[i];
        if (
            ignoreSkeletonPart.includes(segment[0].part) ||
            ignoreSkeletonPart.includes(segment[1].part) ||
            segment[0].score < MIN_ACCEPTABLE_KEYPOINT_SCORE ||
            segment[1].score < MIN_ACCEPTABLE_KEYPOINT_SCORE
        )
            continue;

        let part1Idx = skeletonDescription.indexOf(segment[0].part);
        let part2Idx = skeletonDescription.indexOf(segment[1].part);
        distance["'" + (Math.pow(2, part1Idx) + Math.pow(2, part2Idx)) + "'"] =
            Math.sqrt(
                Math.pow(segment[0].position.x - segment[1].position.x, 2) +
                Math.pow(segment[0].position.y - segment[1].position.y, 2)
            );
    }
    return distance;
}

// compare skeleton validity by using the ratio of longest length and shortest length
function checkSkeletonValidity(skeletonPointsDistance) {
    let min = 9999;
    let max = -1;
    Object.keys(skeletonPointsDistance).forEach(function(key) {
        if (skeletonPointsDistance[key] < min) min = skeletonPointsDistance[key];
        if (skeletonPointsDistance[key] > max) max = skeletonPointsDistance[key];
    });

    if (max / min > 2) return false;

    // let averageDistance = 0 ;
    // Object.keys(skeletonPointsDistance).forEach(function(key){
    //   averageDistance += skeletonPointsDistance[key];
    // });
    // averageDistance /= skeletonPointsDistance.length;

    // console.log(averageDistance);
    // // if a distance more than average distance 20%, then it is invalid
    // Object.keys(skeletonPointsDistance).forEach(function(key){
    //   if(Math.abs(skeletonPointsDistance[key] - averageDistance) > averageDistance * 0.4)
    //   {
    //     console.log("Abnormal skeleton detected!");
    //     return false;
    //   }
    // });

    return true;
}

function gameCompleteAction(game_condition) {
    if (gameCompleted) return;
    let finalScore = totalScore * maxScore;
    for (let i = timetaken_each_pose.length; i > 1; i--) {
        timetaken_each_pose[i - 1] =
            parseFloat(timetaken_each_pose[i - 1]) -
            parseFloat(timetaken_each_pose[i - 2]);
        //console.log("looping");
    }
    let performance_result = {
        Pose_attempt: pose_successful,
        Pose_accuracy: final_accuracy_array,
        Pose_time: timetaken_each_pose,
    };
    files.performance.push(performance_result);
    window.parent.wsCreateScore(Math.round(finalScore), files);
    gameStart = false;
    gameCompleted = true;
    webcam.stop();
    apromise.getTracks().forEach(function(track) {
        track.stop();
    });
    backgroundMusic.pause();
    var complete_bgm = document.getElementById("complete");
    complete_bgm.play();

    backgroundMusic.playbackRate = 1;
    //console.log(timetaken_each_pose.length);
    storeData(files, userName, startTime);
    openDataBase(userName, userid, startTime, finalScore);
    // localStorage.setItem("Score", Math.round(finalScore));

    /*
          document.getElementById('shareBtn').onclick = function() {
          FB.ui({
              display: 'popup',
              method: 'share',
              href: 'https://developers.facebook.com/docs/',
          }, function(response){});
          }
      */
    let campaign_name = document.getElementById("cmname").innerHTML;
    //document.getElementById("scoreboard-btnShare").setAttribute("data-href", "https://fuyoh-ads.com/campaign_landing_page?ogtitle=Highscore in"+campaign_name+"&amp;ogdescription=Fuyohhhhhh! I just scored"+finalScore.toFixed(0).toString()+"in Pose-Matching!&amp;ogimage=https://fuyoh-ads.com/game_web/image/05-posematching.jpg");
    //document.getElementById("fbshare-link").setAttribute("href", "https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Ffuyoh-ads.com%2Fcovid-19%3Fogtitle%3DHighscore%2Bin%2B"+campaign_name+"%26ogdescription%3DFuyohhhhhh%2521%2BI%2Bjust%2Bscored%2B"+finalScore.toFixed(0).toString()+"%2Bin%2BPose-Matching%2521%26ogimage%3Dhttps%253A%252F%252Ffuyoh-ads.com%252Fgame_web%252Fimage%252F05-posematching.jpg&amp;src=sdkpreparse");
    document
        .getElementById("fbshare-link")
        .addEventListener("click", function() {
            let fburl =
                "https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Ffuyoh-ads.com%2Fcovid-19%3Fogtitle%3DHighscore%2Bin%2B" +
                campaign_name +
                "%26ogdescription%3DFuyohhhhhh%2521%2BI%2Bjust%2Bscored%2B" +
                finalScore.toFixed(0).toString() +
                "%2Bin%2BPose%2BMatching%2521%26ogimage%3Dhttps%253A%252F%252Ffuyoh-ads.com%252Fgame_web%252Fimage%252F05-posematching.jpg&amp;src=sdkpreparse";
            window.open(
                fburl,
                "_blank",
                "height=300,width=400,left=550,top=10,resizable=yes,scrollbars=yes"
            );
        });
    document
        .getElementById("scoreboard-btnContinue")
        .addEventListener("click", function() {
            parent.location.href = "https://fuyoh-ads.com/covid-19/dashboard/";
        });
    document.getElementById("divInGameUI").style.display = "block";
    //document.getElementById("divGameCompleted").style.backgroundColor = "rgba(0, 0, 0, 0.8)";
    document.getElementById("divGameCompleted").style.display = "block";
    document.getElementById("scoreboard-score").innerHTML = finalScore.toFixed(0);
    if (game_condition == "timeup") {
        /*var timeup = document.getElementById("timesup");
            timeup.play();*/
        document.getElementById("img-game-timeup").style.display = "block";
        document.getElementById("img-game-completed").style.display = "none";
    } else {
        /*if(finalScore<1000){
                var complete = document.getElementById("complete");
                complete.play();
            }
            else
            {
                var perfect = document.getElementById("perfect");
                perfect.play();
            }*/
        document.getElementById("img-game-timeup").style.display = "none";
        document.getElementById("img-game-completed").style.display = "block";
    }
    setTimeout(function() {
        complete_bgm.pause();
        var gameending_bgm = document.getElementById("endingbgm");
        gameending_bgm.play();
        startConfetti();
        setInterval(function() {
            document.getElementById("confetti-canvas").width =
                $("#divScoreboard").width();
            document.getElementById("confetti-canvas").height =
                $("#divScoreboard").height();
        }, 100);
        document
            .getElementById("confetti-canvas")
            .setAttribute(
                "style",
                "display:block;z-index:999999;pointer-events:none;position:fixed"
            );
        document.getElementById("divScoreboard").style.display = "block";
        // $("#divGameCompleted").css({
        //     "-webkit-animation": "fadeout 2s ",
        //     "animation": "fadeout 2s",
        //     "opacity": 0
        // });
        // document.getElementById("img-scoreboard-fg").style.display = "block";
    }, 3000);
}