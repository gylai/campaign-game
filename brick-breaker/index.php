<!DOCTYPE html>
<html>
<head>
	<title>
	Brick Breaker
	</title>
	<link rel="stylesheet" href="rules.css">
	<link rel="stylesheet" href="./css/animation.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
  <style>
	@font-face {
      font-family: "MachineGunk";
      src: url("./font/MachineGunk-nyqg.ttf") format('truetype');
    }
    body{
      /* background-image: url("img/splashscreen.jpg"); */
      display: flexbox;
      background-size: cover;
      height: 100vh; /* viewport height = 100%, adjustment not recommended */
      width: 100vw; /* viewport width = 100%, adjustment not recommended */
      justify-content: center;
      align-items: center;
      margin: 0;
	  overflow: hidden;
    }
    main {
    position: relative;
    height: 100vmin; /* minimum viewport 100%, can be adjusted */
    width: 100vmin;
    }
    p {
    font-family: 'MachineGunk';
    color: #56362C;
    font-size: 3vw;
    text-align: center;
    position: absolute;
    top: 0%;
	left: 0;
	right: 0;
	margin-left: auto;
	margin-right: auto;
	display: none;
    }
    #splashscreen {
    height: 100%;
    width: 100%;
    z-index: 20;
    position: absolute;
    background-color: black
    }
    #splashscreen > video {
    background-color: black;
    object-fit: contain;
    width: 100%;
    height: 100%;
	outline: none;
	/* outline not working */
    }
    #loading {
    z-index: 21;
    height: auto;
    width: 30%;
    object-fit: contain;
    position: absolute;
    filter: sepia(100%) contrast(200%);
    }
    #tutorial{
      object-fit: contain;
      width: 100%;
      height: 100%;
      align-items: center;
    }
	#proceed {
		position: absolute;
		z-index: 32;
		display: none;
		background-image: linear-gradient(#FEC22E, #FE7235);
		border-radius: 0.45em 0px 0px 0.45em;
		width: 15%;
		height: 7%;
		top: 5%;
		right: 0;
		opacity: 0.4;
		transition: opacity 0.3s;
		-webkit-transition: opacity 0.3s;
		animation: slidein 0.7s;
		font-family: "MachineGunk";
		color: white;
	}
	#proceed:hover {
		opacity: 1;
		font-size: 1.5rem;
	}

	#exit{
		z-index: 22;
		background-image: linear-gradient(#FEC22E, #FE7235);
		border-radius: 0px 0.45em 0.45em 0px;
		position: absolute;
		width: 15%;
		height: 7%;
		top: 5%;
		left: 0;
		/*transform: translate(0, -50%);*/
		opacity: 0.4;
		transition: opacity 0.3s;
		-webkit-transition: opacity 0.3s;
		animation: slideinleft 1s;
		display: none;
		font-family: 'MachineGunk';
		color: #FFFFFF;
		display: none;
	}

	#exit:hover{
		opacity:1;
		font-size:1.5em;
	}
  </style>
    <style>
        /* Absolute Center Spinner */
        .loading {
            position: fixed;
            z-index: 999;
            height: 2em;
            width: 2em;
            overflow: show;
            margin: auto;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
        }

        /* Transparent Overlay */
        .loading:before {
            content: '';
            display: block;
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: radial-gradient(rgba(20, 20, 20,.8), rgba(0, 0, 0, .8));

            background: -webkit-radial-gradient(rgba(20, 20, 20,.8), rgba(0, 0, 0,.8));
        }

        /* :not(:required) hides these rules from IE9 and below */
        .loading:not(:required) {
            /* hide "loading..." text */
            font: 0/0 a;
            color: transparent;
            text-shadow: none;
            background-color: transparent;
            border: 0;
        }

        .loading:not(:required):after {
            content: '';
            display: block;
            font-size: 10px;
            width: 1em;
            height: 1em;
            margin-top: -0.5em;
            -webkit-animation: spinner 150ms infinite linear;
            -moz-animation: spinner 150ms infinite linear;
            -ms-animation: spinner 150ms infinite linear;
            -o-animation: spinner 150ms infinite linear;
            animation: spinner 150ms infinite linear;
            border-radius: 0.5em;
            -webkit-box-shadow: rgba(255,255,255, 0.75) 1.5em 0 0 0, rgba(255,255,255, 0.75) 1.1em 1.1em 0 0, rgba(255,255,255, 0.75) 0 1.5em 0 0, rgba(255,255,255, 0.75) -1.1em 1.1em 0 0, rgba(255,255,255, 0.75) -1.5em 0 0 0, rgba(255,255,255, 0.75) -1.1em -1.1em 0 0, rgba(255,255,255, 0.75) 0 -1.5em 0 0, rgba(255,255,255, 0.75) 1.1em -1.1em 0 0;
            box-shadow: rgba(255,255,255, 0.75) 1.5em 0 0 0, rgba(255,255,255, 0.75) 1.1em 1.1em 0 0, rgba(255,255,255, 0.75) 0 1.5em 0 0, rgba(255,255,255, 0.75) -1.1em 1.1em 0 0, rgba(255,255,255, 0.75) -1.5em 0 0 0, rgba(255,255,255, 0.75) -1.1em -1.1em 0 0, rgba(255,255,255, 0.75) 0 -1.5em 0 0, rgba(255,255,255, 0.75) 1.1em -1.1em 0 0;
        }

        /* Animation */

        @-webkit-keyframes spinner {
            0% {
                -webkit-transform: rotate(0deg);
                -moz-transform: rotate(0deg);
                -ms-transform: rotate(0deg);
                -o-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                -moz-transform: rotate(360deg);
                -ms-transform: rotate(360deg);
                -o-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }
        @-moz-keyframes spinner {
            0% {
                -webkit-transform: rotate(0deg);
                -moz-transform: rotate(0deg);
                -ms-transform: rotate(0deg);
                -o-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                -moz-transform: rotate(360deg);
                -ms-transform: rotate(360deg);
                -o-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }
        @-o-keyframes spinner {
            0% {
                -webkit-transform: rotate(0deg);
                -moz-transform: rotate(0deg);
                -ms-transform: rotate(0deg);
                -o-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                -moz-transform: rotate(360deg);
                -ms-transform: rotate(360deg);
                -o-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }
        @keyframes spinner {
            0% {
                -webkit-transform: rotate(0deg);
                -moz-transform: rotate(0deg);
                -ms-transform: rotate(0deg);
                -o-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                -moz-transform: rotate(360deg);
                -ms-transform: rotate(360deg);
                -o-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }
    </style>
</head>
<body>
<div class="loading">Loading&#8230;</div>

	<!-- <div class="hint">
	Click to play &#9836;
	</div>	 -->
	<!-- <audio autoplay id="BGM" src="audio/music1.mp3">
		<source type="audio/mpeg">
		Your browser does not support the audio tag.
	</audio> -->

	<div id="splashscreen">
		<video autoplay muted  poster="img/splashscreen.jpg">
			<source src="img/BrickBreaker.mp4" type="video/mp4">
			Your browser does not support the video tag
		</video>
	</div>

	<video id="tutorial" autoplay muted loop>
		<source src="tutorials/BrickBreaker(online).mp4" type="video/mp4">
		Your browser does not support the video tag.
	</video>

	<button id="proceed">Start Game</button>
	<button id="exit">Exit</button>
	<p>Click anywhere to continue</p>

	<main>
		<!--
		<div class="wrapper">

			<div class="title">
				Find the INVISIBLE DOGGGG <img id="logoimg">
			</div>
			<br><br>

			<div class="rules">
				<p>
					&#10049; First, choose ur preference background color!<br>
					<img src="img/color.gif" id="new"><br><br>
				</p>
				<p>
					&#10049; Second, move the ring(with ur hand) around the screen<br>
					&nbsp;&nbsp;&nbsp;&nbsp;LISTEN !<br>
					&nbsp;&nbsp;There will be<b> dog sound </b>when you near the DOG!<br>
					<img src="img/catchdog.gif" id="new"><br><br>
				</p>
				<p>
					&#10049; Please find three <b>DOGSSS</b> in seconds!<br>
					<img id="targetimg"><br><br>
				</p>
				<p>
					&#10049; Be Careful there will be a random <b>CAT</b>!<br>
					&nbsp;&nbsp;There will be<b> cat sound </b>when you near the CAT!<br>
					&nbsp;&nbsp;CAT will freeze your game<br>
					<img id="trapimg"><br><br>
				</p>

				<div class="ls">
					<input type="submit" name="gamebtn" value="Start Game" id="btn" onclick="window.location.href = 'game.html';">
				</div>
			</div>


			<div class="scroll-down" onclick="document.getElementById('btn').scrollIntoView();">
				<img src="img/scroll-down.png">
			</div>
		</div>
		-->
	</main>
	<span id="cmname" style="display:none"></span>
	<script src="loadItem.js"></script>
	<script>
        let loadPercentages = 0;
        let displayNavButton =true;

		window.onload = () => {
			// setTimeout(function () {
			// 	document.querySelector("#splashscreen").style.backgroundColor = "transparent";
			// 	document.querySelector("#splashscreen").style.display = "none";
			// }, 3000);
			// setTimeout(function () {
            //
			// 	document.querySelector("#proceed").style.display = "block";
			// 	document.querySelector("#exit").style.display = "block";
            //
			// 	proceed.onclick = () => {
            //
			// 		var campaign_name = url.searchParams.get("campaign_name");
            //
			// 		let campaign_name_value="campaign_name="
			// 		if(campaign_name)
			// 		{
			// 			document.getElementById("cmname").innerHTML = campaign_name;
            //
			// 			campaign_name_value+=campaign_name
			// 		}
            //
			// 		window.location.href = "game.php?"+campaign_name_value;
			// 	}
			// 	document.getElementById("exit").onclick = () => {
			// 	parent.location.href='https://fuyoh-ads.com/covid-19/dashboard';
			// 		document.getElementById("proceed").style.display = "none";
			// 		document.getElementById("exit").style.display = "none";
			// 	}
            //
            //
			// }, 7000);


		}


		var tutorial = document.querySelector("#tutorial");
        var loading = false;
        tutorial.addEventListener('timeupdate', function () {
            if(!loading) {
                loading = true;
                console.log("Timer Counting....")
                setTimeout(function () {
                    if(loadPercentages < 98) {
                        alert("We detect you are having a slow connection, you might experience longer loading time or abnormal gaming experience, please find an alternative reliable connectivity.")
                        allowToPlay = false;
                    }
                }, 30000);
            }
            if (this.duration && loadPercentages <98)
            {
                let range = 0;
                let bf = this.buffered;
                let time = this.currentTime;

                while (!(bf.start(range) <= time && time <= bf.end(range))) {
                    range += 1;
                }
                let loadStartPercentage = bf.start(range) / this.duration;
                let loadEndPercentage = bf.end(range) / this.duration;
                let loadPercentage = (loadEndPercentage - loadStartPercentage) * 100;
                console.log(loadPercentage)

                loadPercentages = loadPercentage;
            }
            else
            {
                $(".loading").hide();
                document.querySelector("#splashscreen").style.backgroundColor =
                    "transparent";
                document.querySelector("#splashscreen").style.display = "none";

                if(displayNavButton == true)
                {
                    displayNavButton=false;

                    //video duration;
                    setTimeout(function () {
                        document.querySelector("#proceed").style.display = "block";
                        document.querySelector("#exit").style.display = "block";
                    }, 3000);
                }
             
                proceed.onclick = () => {

                    var campaign_name = url.searchParams.get("campaign_name");

                    let campaign_name_value="campaign_name="
                    if(campaign_name)
                    {
                        document.getElementById("cmname").innerHTML = campaign_name;

                        campaign_name_value+=campaign_name
                    }

                    window.location.href = "game.php?"+campaign_name_value;
                }
                document.getElementById("exit").onclick = () => {
                    parent.location.href='https://fuyoh-ads.com/covid-19/dashboard';
                    document.getElementById("proceed").style.display = "none";
                    document.getElementById("exit").style.display = "none";
                }
            }
        });

		tutorial.onended = () => {
			//if (confirm("Press OK to continue\nPress Cancel to watch again")) {
			//	window.location.href = "game.html";
			//} else {
			//	return;
			//}
		}
	</script>
	<script async defer crossorigin="anonymous" src="https://connect.facebook.net/zh_CN/sdk.js#xfbml=1&version=v11.0" nonce="tCbD2tA6"></script>
	<script>(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
	fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));

	var url = new URL(window.location.href);
		var campaign_name = url.searchParams.get("campaign_name");
		if(campaign_name)
		{

			document.getElementById("cmname").innerHTML = campaign_name;
		}
		else{
			document.getElementById("cmname").innerHTML = "Brick Breaker";
		}
	</script>


</body>
</html>
