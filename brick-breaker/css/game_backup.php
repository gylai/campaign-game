
<!DOCTYPE html>

<html lang="en">
<head>
    <title>Brick Breaker</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Germania+One"> -->
    <script src="https://code.jquery.com/jquery-3.5.0.min.js"integrity="sha256-xNzN2a4ltkB44Mc/Jz3pT4iU1cmeR0FkXs4pru/JxaQ="crossorigin="anonymous"></script>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="stylesheet" href="./css/index.css">
	<style>
		#min,#sec{
			/* font-family: 'Share Tech Mono', monospace; */
			color: #daf6ff;
			text-shadow: 0 0 20px rgba(10, 175, 230, 1),  0 0 20px rgba(10, 175, 230, 0);
			letter-spacing: 0.05em;

		}
        
        ::-webkit-scrollbar {
        width: 0px; 
        background: transparent; 
        }
  
		
		@media only screen and (max-width: 1100px) {
			.breakout{
				width:80%;
			}
			.main_banner{
				width:100%;
			}
			 .game_details{
				background-size: 100% 100%;
				width:90%; 
			}
		
			
			.breakout,
			{
				display: none;
			}
			.main_banner{
				
			}
			#gameover{
				margin: 155px auto;
				width:80%; 
				height:100%;
			}
		}
		tr:nth-child(even) 
		{
			background-color: #f2f2f2;
		}
		tr:nth-child(odd) 
		{
			background-color: black;
			color:white
		}
		table,tr,th{
			/* font-family:Verdana; */
        }
        
        .flex{
            display:flex;
        }
        .flex-col {
            flex-direction: column ; 
        }
        .justify-center{
            justify-content : center;
        }
        .align-center{
            align-items: center; 
        }

		#gameover {
		/* background-image: url("img/gameover.png"); */
		z-index: 20;
		display: none;
		height: 100%;
		width: 100%;
		position: absolute;
		align-items: center;
		justify-content: center;
		background-color: black;
		-webkit-animation-name: gameoveranimate;
		-webkit-animation-duration: 0.6s;
		animation-name: gameoveranimate;
		animation-duration: 0.6s
	}

	#gameover > img {
		position: absolute;
		object-fit: cover;
		width: 100%;
		/* height: 100%; */
	}

	@-webkit-keyframes gameoveranimate {
		from {
			top: -300px;
			opacity: 0
		}

		to {
			top: 0;
			opacity: 1
		}
	}

	@keyframes gameoveranimate {
		from {
			top: -300px;
			opacity: 0
		}

		to {
			top: 0;
			opacity: 1
		}
	}
	#finishscreen {
		z-index: 1;
		transition-property: opacity;
		transition-duration: 1s;
  		transition-delay: 2s;
	}
	#finishscreen:hover {
		opacity: 0;
	}
	#gameovercontent {
		position: absolute;
		/* display: flex; */
		width: 100%;
		height: 80%;
		text-align: center;
		justify-content: center;
		align-items: center;
		display: grid;
		grid-template-columns: 50% 50%;
		grid-template-rows: 20% 40% 10% 30%;
		/* grid-column-gap: 1vw; */
		margin-top: 10vh;
	}
		#p1 {
			grid-column: span 2;
			margin: 0;
			width: 100%;
			height: 100%;
			font-family: "Machine Gunk";
			font-size: 2vw;
			color: #4F382E;
		}

		#score {
			grid-column: span 2;
			font-size: 8vw;
			text-align: center;
			font-family: "Machine Gunk";
			color: #4F382E;
		}

		#p2 {
			margin: 0;
			font-size: 1vw;
			color: #B2927E;
			text-align: right;
			font-family: "Machine Gunk";
		}
			
		#highscore {
			font-size: 3vw;
			text-align: left;
			font-family: "Machine Gunk";
			color: #4F382E;
		}

		#gameovercontent button {
			margin-top: 25vh!important;
			margin: auto;
			border-radius: 15px;
			width: 15%;
			height: 40%;
		}

		#gameovercontent button p {
			margin: 0;
			font-size: 2vw;
			color: white;
			font-family: "Machine Gunk";
		}

	#gameovercontent #share {
		margin-right: 0;
		background-image: linear-gradient(to bottom, #7E5B4A, #4F382E);
		/*
		shadow: #000000 0.1vw;
		border: #FFFFFF 0.1vw, #000000 0.1vw;
		*/
	}

	#gameovercontent #continue {
		margin-left: 0;
		background-image: linear-gradient(to bottom, #FEC22E, #FE7235);
	}
	#advert {
		padding-top: 30px;
		height: 180px;
		position: absolute;
		margin-top: auto;
		margin-bottom: auto;
		margin-left: auto;
		margin-right: auto;
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
		text-align: center;
	}
	.start-screen {
		z-index: 50;
		object-fit: contain;
		/* bottom: 10%; */
		background-color: rgba(0,0,0,.5);
		position:absolute;
		padding: 0;
		margin: 0;
		height: 100%;
		width: 100%;
	}
	#start-btn {
		z-index: 100;
	}
	</style>
</head>
<body >
<audio src="audio/music1.mp3" autoplay="true" display="visible" loop></audio>
<div  class="flex flex-col align-center">
	<!-- <div id="timePosition" class="flex flex-col justify-center align-center">
        <div id="timer" style="width:100%;">
		 <span style="font-weight:bold;color:red;font-family:Arial;font-size:20px;">Timer: </span>
            <span id="min"></span>: <span id="sec">&nbsp;</span>
        </div>
		<p style="color:white; font-size:25px;font-family:Arial;">Play until you are satisfied with the result ! Good Luck !</p>
		<span style="color:white;text-align:center;padding:20px;font-family:Arial;">Move paddle with arrow keys &#8592; = left, &#8594; = right</span>
    </div> -->
	
    <!-- <canvas id="breakout" class="breakout" onclick="start()" width="80%"></canvas> -->

	<!-- Game over screen (new scoreboard)-->
	<div id="gameover">
		<img id="finishscreen" src="./img/brickbreaker(Complete).jpg">
		<img id="gameoverui" src="./img/bg-11.jpg">
		<div id="gameovercontent">
			<p id="p1" style="padding-top: 40vh;">score</p>
			<span id="score" style="padding-top: 30vh;">0</span>
			<!-- <p id="p2" style="padding-top: 10vh;">high score<br />(all time)</p><span id="highscore" style="padding-top: 10vh;">0</span> -->
			<!-- <div id="win"><img src="./img/bg-11.jpg" style="width:100%;height:100%;opacity:80%;"></div>
			<div id="lose"><img src="./img/bg-11.jpg" style="width:100%;height:100%;opacity:80%;"></div> -->
			<div class="mt-3 justify-content-center" style="grid-column: span 2;">
				<button id="share"><p>share</p></button>
				<button id="continue"><p>continue</p></button>
			</div>
		</div>
	</div>

    <!-- <div id="youwon">
        <div id="win"><img src="./img/youwon.png" style="width:100%;height:100%;opacity:80%;"></div>
        <div id="lose"><img src="./img/gameover.png" style="width:100%;height:100%;opacity:80%;"></div>
        <button id="retry" onclick="resetGame()">TRY AGAIN</button> -->
        <!-- <button id="backToMenu" onclick="backToMenu()">Finish</button> -->
    <!-- </div> -->
    <img class="start-screen" src="./img/bg-08.png">
	<button id="start-btn" class="justify-content-center align-items-centers" onclick="game()"><img src="./img/img-09.png"></button>
    <div class="px-5">
		
		<div class="row">
			
			<div class="col-6 ml-5 d-flex justify-content-end align-items-right">
				<div class="game_details mt-5">
					<div class="details">
						<div class="about flex flex-col justify-center align-center">
						<!-- <img src="https://i.imgur.com/mjE70No.gif"/ width="100%;"> -->
						<button id="request" onclick="requestMotionPermission()">
						Allow permission to play
						</button>
						<!-- <button class="justify-content-center align-items-centers" onclick="game()">Start</button> -->
						<!--	<button onclick="about()">About game</button> 
						//-->
					<!--  <button onclick="how_to_play()">How To Play</button>
						<button onclick="display_score()">Scoreboard</button> -->
						<!-- <button> <a href="index.html" style="text-decoration:none;">Tutorial</a></button>
						<button> <a href="end.html" style="text-decoration:none;">Quit</a></button> -->
						</div>
					</div>
					<div class="game_about">
						<h1>About game</h1>
						<p>You are in control of a sliding platform that can bounce the wrecking balls into the bricks above. Use angles and rebounds to control the direction the balls move. If the balls fall into the abyss below, you will lose a life.</p>
						<button onclick="back()">Return</button>
					</div>

					<div class="how_to_play">
					<h1><b>How To Play</b></h1>
					<p style="color:white;padding:10px;font-size:16px;">Click or touch the game screen to begin. Tilt your phone to control the paddle. The ball will bounce in different directions based on where it hits the platform. Control the ball hit the brick to break it. You have only 3 lifes, every time the ball falls into abyss your will lost 1 life. Good luck and win the game!</p>
					<br>
					<img src="css/tilt.gif" width="170" height="100">
						
						<button onclick="back1()">Return</button>
					</div>
			
					<div class="scoreboard" style="margin:10px;">
						
						<span style="color:white;font-size:40px;font-family:'Arial;'">Leaderboard</span>
						<hr>
						<table border="1" width="100%" style="border-collapse: collapse;">
							<tr>
								<th style="background-color:black;color:white">Number</th>
								<th style="background-color: black;color:white">Username</th>
								<th style="background-color: black;color:white">Score</th>
							</tr> 
							
							<?php 
							$num=10;
							$sql = "Select * from score ORDER BY score DESC LIMIT $num";
							$result = mysqli_query($connect,$sql);
							$x=1;
							
								while($row=mysqli_fetch_assoc($result))
								{
							?>
							<tr>
							<th><?php echo $x; $x++;?></th>
							<th><?php echo $row["user"] ?></th>
							<th><?php echo $row["score"] ?></th>
							</tr><?php } ?>
					</table>
					<hr>
						<button onclick="back2()">Return</button>
					</div>
			
				</div>
				<div class="canvas-background-img">
					<canvas id="breakout" class="breakout" style="transform: scale(.9);" onclick="start()" width="80%" ></canvas>
				</div>
			</div>
			<div class="col-6 text-center">
				<div class="advertising text-center pt-5" style="position: relative;">
					<img id="advert" src="">
					<img class="w-50" src="./img/bg-02.png">
				</div>
				<div>
					<span style="background:#D74C36;color:#FFB3A5;text-align:center;padding:20px;font-family:Montserrat;">Move paddle with arrow keys left & right</span>
				</div>
			</div>

		</div>	
	</div>

	<!-- <span style="color:white;text-align:center;padding:20px;font-family:Arial;">This game is supported<br> by TM</span>
	<img src="./img/tips2.png" style="width:300px;"> -->
</div>
    <script src="./js/index.js"></script>
    <script src="./js/component.js"></script>
	
	
</body>
</html>

<script>

window.onload = function() {
	document.getElementById("advert").src = "./img/coin.png";
};

requestMotionPermission()
</script>