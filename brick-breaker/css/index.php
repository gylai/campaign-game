<!DOCTYPE html>
<html>
<head>
	<title>
	Brick Breaker
	</title>
	<link rel="stylesheet" href="rules.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
  <style>
    body{
      /* background-image: url("img/splashscreen.jpg"); */
      display: flexbox;
      background-size: cover;
      height: 100vh; /* viewport height = 100%, adjustment not recommended */
      width: 100vw; /* viewport width = 100%, adjustment not recommended */
      justify-content: center;
      align-items: center;
      margin: 0;
	  overflow: hidden;
    }
    main {
    position: relative;
    height: 100vmin; /* minimum viewport 100%, can be adjusted */
    width: 100vmin;
    }
    p {
    font-family: 'Machine Gunk';
    color: #56362C;
    font-size: 3vw;
    text-align: center;
    position: absolute;
    top: 0%;
	left: 0;
	right: 0;
	margin-left: auto;
	margin-right: auto;
	display: none;
    }
    #splashscreen {
    height: 100%;
    width: 100%;
    z-index: 20;
    position: absolute;
    background-color: black
    }
    #splashscreen > video {
    background-color: black;
    object-fit: contain;
    width: 100%;
    height: 100%;
	outline: none; 
	/* outline not working */
    }
    #loading {
    z-index: 21;
    height: auto;
    width: 30%;
    object-fit: contain;
    position: absolute;
    filter: sepia(100%) contrast(200%);
    }
    #tutorial{
      object-fit: contain;
      width: 100%;
      height: 100%;
      align-items: center;
    }
	#proceed {
		position: absolute;
		z-index: 32;
		display: none;
		background-image: linear-gradient(#FEC22E, #FE7235);
		border-radius: 10px 0px 0px 10px;
		width: 15%;
		height: 7%;
		top: 5%;
		right: 0;
		opacity: 0.4;
		transition: opacity 0.3s;
		-webkit-transition: opacity 0.3s;
		animation: slidein 1s;
		font-family: "Machine Gunk";
		color: white;
		font-size: 1.5rem;
	}
	#proceed:hover {
		opacity: 1;
	}
  </style>
</head>
<body>
	<!-- <div class="hint">
	Click to play &#9836;
	</div>	 -->
	<!-- <audio autoplay id="BGM" src="audio/music1.mp3">
		<source type="audio/mpeg">
		Your browser does not support the audio tag.
	</audio> -->

	<div id="splashscreen">
		<video autoplay muted poster="img/splashscreen.jpg">
			<source src="img/BrickBreaker.mp4" type="video/mp4">
			Your browser does not support the video tag
		</video>
	</div>

	<video id="tutorial" autoplay muted>
		<source src="tutorials/BrickBreaker(online).mp4" type="video/mp4">
		Your browser does not support the video tag.
	</video>

	<button id="proceed">Start Game</button>
	<p>Click anywhere to continue</p>

	<main>
		<!--
		<div class="wrapper">

			<div class="title">
				Find the INVISIBLE DOGGGG <img id="logoimg">
			</div>
			<br><br>

			<div class="rules">
				<p>
					&#10049; First, choose ur preference background color!<br>
					<img src="img/color.gif" id="new"><br><br>
				</p>
				<p>
					&#10049; Second, move the ring(with ur hand) around the screen<br>
					&nbsp;&nbsp;&nbsp;&nbsp;LISTEN !<br>
					&nbsp;&nbsp;There will be<b> dog sound </b>when you near the DOG!<br>
					<img src="img/catchdog.gif" id="new"><br><br>
				</p>
				<p>
					&#10049; Please find three <b>DOGSSS</b> in seconds!<br>
					<img id="targetimg"><br><br>
				</p>
				<p>
					&#10049; Be Careful there will be a random <b>CAT</b>!<br>
					&nbsp;&nbsp;There will be<b> cat sound </b>when you near the CAT!<br>
					&nbsp;&nbsp;CAT will freeze your game<br>
					<img id="trapimg"><br><br>
				</p>

				<div class="ls">
					<input type="submit" name="gamebtn" value="Start Game" id="btn" onclick="window.location.href = 'game.html';">
				</div>
			</div>


			<div class="scroll-down" onclick="document.getElementById('btn').scrollIntoView();">
				<img src="img/scroll-down.png">
			</div>
		</div>
		-->
	</main>

	<script src="loadItem.js"></script>
	<script>

		window.onload = () => {
			setTimeout(function () {
				document.querySelector("#splashscreen").style.backgroundColor = "transparent";
				document.querySelector("#splashscreen").style.display = "none";
			}, 3000);
		}

		var tutorial = document.querySelector("#tutorial");

		tutorial.onended = () => {

			document.querySelector("#proceed").style.display = "block";

            proceed.onclick = () => {
                window.location.href = "game.php";
            }
			//if (confirm("Press OK to continue\nPress Cancel to watch again")) {
			//	window.location.href = "game.html";
			//} else {
			//	return;
			//}
		}
	</script>
</body>
</html>
