<?php
  if($_GET['hardware']=='camera')
  {
?>
      <script>
        var hardware_name = "camera";
      </script>
<?php
  }
  else
  {
?>
    <script>
      var hardware_name = "mic";
    </script>
<?php
  }
?>

<html>
<head>
<title>Grant Permission</title>
<script src="https://kit.fontawesome.com/ce491e2369.js" crossorigin="anonymous"></script>
<style>
    @font-face {
    font-family: "Montserrat-Regular";
    src: url("font/Montserrat-Regular.ttf") format('truetype');
    }

    @font-face {
        font-family: "Montserrat-SemiBold";
        src: url("font/Montserrat-SemiBold.ttf") format('truetype');
    }

    :root {
    --target-width: 100vw;
    }

    body {
        background-image: linear-gradient(black, grey);
        background-repeat: no-repeat;
        width: 100vw;
        height: calc(100vw/16*9);
    }

    .content{
        display: block;
        text-align:left;
        position:relative;
        width: 58%;
        left: 21%;
        font-family: "Montserrat-Regular";
        font-weight: 500;
        color: #3C3C3C;
        font-size: calc(var(--target-width)*0.01*1.0);
    }

    .content b{
        color: #287194;
        font-size: calc(var(--target-width)*0.01*1.0);
    }

    #cameracontent, #miccontent{
        background-color: white;
        margin: 5% 10% 5% 10%;
        padding-bottom: 2%;
        border-radius: calc(var(--target-width)*0.01*0.6);
        text-align: center;
    }

    h1 {
        font-size: calc(var(--target-width)*0.01*1.4);
        font-family: 'Montserrat-Regular';
        color: #3C3C3C;
    }

    h2 {
        font-size: calc(var(--target-width)*0.01*1.2);
        font-family: "Montserrat-SemiBold";
        font-weight: bold;
        color: #3C3C3C;
    }

    hr {
        margin: 2% 21% 2% 21%;
        color: #BABABA;
    }

    button {
        box-shadow: #000000 15%;
        /* border-color: #FFFFFF 100% #000000 50%; */
        border-width: calc(var(--target-width)*0.01*0.2);
        color: #FFFFFF;
        border-radius: calc(var(--target-width)*0.01*0.6);
        width: 20%;
        height: 8%;
        font-size: calc(var(--target-width)*0.01*1.3);
    }

    button:hover {
        border-width: calc(var(--target-width)*0.01*0.2);
        font-size: calc(var(--target-width)*0.01*1.5);
    }

    #exit_btn {
        background-color: #287194;
        color: white;
        font-family: 'Montserrat-Regular';
        font-weight: bold;
    }
</style>
</head>
<body onload="checkbrowser()">
    <div id="cameracontent" style="display:none;">
        <br><br>
        <img src="images/no_camera.png" style="width:auto; height:20%;">
        <h1>You have reached this page because we cannot access your camera</h1>
        <hr>
        <span class="content">
            <h2>Instruction for the website to access webcam :</h2>
            <div id="firefox-camera" style="display:none">
                <p>- Click the <b>menu button <i class="fas fa-bars"></i></b> and select <b>"Settings"</b>.</p>
                <p>- Click <b>"Privacy & Security"</b> from the left menu.</p>
                <p>- Scroll down to the <b>Permissions</b> section.</p>
                <p>- Click the <b>"Settings..."</b> button for the <b>Camera <i class="fas fa-video"></i></b> option.</p>
                <p>- Use the <b>Allow/Block selector</b> to change permission status from <b>"Block"</b> to <b>"Allow"</b> for the <b>https://fuyoh-ads.com</b>.</p>
                <p>- Click the <b>"Save Changes"</b> button.</p>
            </div>
            <div id="chrome-camera" style="display:none">
                <p>- At the top right of your browser, click <b>"More <i class="fas fa-ellipsis-v fa-xs"></i> " > "Settings"</b>.</p>
                <p>- Under <b>"Privacy and security"</b>, click <b>Site settings</b>.</p>
                <p>- Scroll down to the <b>Permissions</b> section.</p>
                <p>- Click <b>Camera</b>.</p>
                <p>- Find <b>https://fuyoh-ads.com:443</b> and click the <b>arrow icon <i class="fas fa-caret-right"></i></b> beside it.</p>
                <p>- Change the camera permission to <b>"Allow"</b>.</p>
            </div>
            <div id="safari-camera" style="display:none">
                <p>- On your Mac, choose <b>Apple menu > System Preferences</b>, click <b>Security & Privacy</b>, then click <b>Privacy</b>.</p>
                <p>- Select <b>Camera</b>.</p>
                <p>- Unlock (click) the lock icon in the lower-left to allow you to make changes to your preferences.</p>
                <p>- Select the checkbox next to an app to allow it to access your camera.</p>
                <p>- Deselect the checkbox to turn off access for that app.</p>
                <p>- If you turn off access for an app, you’re asked to turn it on again the next time that app tries to use your camera.</p>
            </div>
            <div id="edge-camera" style="display:none">
                <p>- Click the <b>Lock or Information icon <i class="fas fa-lock"></i></b> next to the website link in the address bar.</p>
                <p>- Find <b>Camera</b> under <b>Permissions for this site</b>.</p>
                <p>- Use the drop-down menus beside the <b>Camera</b> to change the permissions to <b>"Allow"</b>.</p>
                <p>- Click the <b>Refresh button <i class="fas fa-redo"></i></b> on the site to apply the changes.</p>
            </div>
        </span>
        <button id="exit_btn" onclick="exit_game()">&nbsp&nbspExit&nbsp&nbsp</button>
    </div>
    <div id="miccontent" style="display:none">
        <br><br>
        <img src="images/no_mic.png" style="width:auto; height:20%;">
        <h1>You have reached this page because we cannot access your microphone</h1>
        <hr>
        <span class="content">
            <h2>INSTRUCTION FOR THE WEBSITE TO ACCESS MICROPHONE :</h2>
            <br>
            <div id="firefox-mic" style="display:none">
                <p>- Click the <b>menu button <i class="fas fa-bars"></i></b> and select <b>"Settings"</b>.</p>
                <p>- Click <b>"Privacy & Security"</b> from the left menu.</p>
                <p>- Scroll down to the <b>Permissions</b> section.</p>
                <p>- Click the <b>"Settings..."</b> button for the <b>Microphone <i class="fas fa-microphone"></i></b> option.</p>
                <p>- Use the <b>Allow/Block selector</b> to change permission status from <b>"Block"</b> to <b>"Allow"</b> for the <b>https://fuyoh-ads.com</b>.</p>
                <p>- Click the <b>"Save Changes"</b> button.</p>
            </div>
            <div id="chrome-mic" style="display:none">
                <p>- At the top right, click <b>"More <i class="fas fa-ellipsis-v fa-xs"></i> " > "Settings"</b>.</p>
                <p>- Under <b>"Privacy and security"</b>, click <b>Site settings</b>.</p>
                <p>- Scroll down to the <b>Permissions</b> section.</p>
                <p>- Click <b>Microphone</b>.</p>
                <p>- Find <b>https://fuyoh-ads.com:443</b> and click the <b>arrow icon <i class="fas fa-caret-right"></i></b> beside it.</p>
                <p>- Change the microphone permission to <b>"Allow"</b>.</p>
            </div>
            <div id="safari-mic" style="display:none">
                <p>- On your Mac, choose <b>Apple menu > System Preferences</b>, click <b>Security & Privacy</b>, then click <b>Privacy</b>.</p>
                <p>- Select <b>Microphone</b>.</p>
                <p>- Unlock (click) the lock icon in the lower-left to allow you to make changes to your preferences.</p>
                <p>- Select the checkbox next to an app to allow it to access the microphone.</p>
                <p>- Deselect the checkbox to turn off access for that app.</p>
                <p>- If you turn off access for an app, you’re asked to turn it on again the next time that app tries to use your microphone.</p>
            </div>
            <div id="edge-mic" style="display:none">
                <p>- Click the <b>Lock or Information icon <i class="fas fa-lock"></i></b> next to the website link in the address bar.</p>
                <p>- Find <b>Microphone</b> under <b>Permissions for this site</b>.</p>
                <p>- Use the drop-down menus beside the <b>Microphone</b> to change the permissions to <b>"Allow"</b>.</p>
                <p>- Click the <b>Refresh button <i class="fas fa-redo"></i></b> on the site to apply the changes.</p>
            </div>
        </span> 
        <button id="exit_btn" onclick="exit_game()">&nbsp&nbspExit&nbsp&nbsp</button>
    </div>
</body>
</html>

<script>
    function checkbrowser()
    {
        // Firefox 1.0+
        var isFirefox = typeof InstallTrigger !== 'undefined';

        // Safari 3.0+ "[object HTMLElementConstructor]" 
        var isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof safari !== 'undefined' && window['safari'].pushNotification));

        // Internet Explorer 6-11
        var isIE = /*@cc_on!@*/false || !!document.documentMode;

        // Edge 20+
        var isEdge = !isIE && !!window.StyleMedia;

        // Chrome 1 - 79
        var isChrome = !!window.chrome && (!!window.chrome.webstore || !!window.chrome.runtime);

        // Edge (based on chromium) detection
        var isEdgeChromium = isChrome && (navigator.userAgent.indexOf("Edg") != -1);

        filtercontent(isFirefox, isSafari, isEdge, isChrome, isEdgeChromium);
    }

    function filtercontent(isFirefox, isSafari, isEdge, isChrome, isEdgeChromium)
    {
        if(hardware_name == "camera")
        {
            document.getElementById("cameracontent").style.display = "block";
            if(isFirefox)
            {
                document.getElementById("firefox-camera").style.display = "block";
            }
            else if(isSafari)
            {
                document.getElementById("safari-camera").style.display = "block";
            }
            else if(isEdge || isEdgeChromium)
            {
                document.getElementById("edge-camera").style.display = "block";
            }
            else
            {
                document.getElementById("chrome-camera").style.display = "block";
            }
        }
        else
        {
            document.getElementById("miccontent").style.display = "block";
            if(isFirefox)
            {
                document.getElementById("firefox-mic").style.display = "block";
            }
            else if(isSafari)
            {
                document.getElementById("safari-mic").style.display = "block";
            }
            else if(isEdge || isEdgeChromium)
            {
                document.getElementById("edge-mic").style.display = "block";
            }
            else
            {
                document.getElementById("chrome-mic").style.display = "block";
            }
        }
    }

    window.onresize = screenResize;

    function screenResize() {
        // w: width, h: height
        let w = window.innerWidth;
        let h = window.innerHeight;
        let targetWidth = w;
        if (h >= w / 16 * 9) {
            /*$("body").css({
                "width": "100vw",
                "height": "calc(100vw/16*9)"
            });*/

        } else {
            targetWidth = h / 9 * 16;
                /*$("body").css({
                    "width": "calc(100vh/9*16)",
                    "height": "100vh"
                });*/
        }
        document.documentElement.style.setProperty('--target-width', targetWidth + "px");
    }

    function exit_game() {
        parent.location.href = "https://fuyoh-ads.com/covid-19/dashboard/";
    }
</script>