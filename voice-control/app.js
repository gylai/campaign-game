//check browser
var browserUsed = (function () {
  if (
    (navigator.userAgent.indexOf("Opera") ||
      navigator.userAgent.indexOf("OPR")) != -1
  ) {
    return "Opera";
  } else if (navigator.userAgent.indexOf("Chrome") != -1) {
    return "Chrome";
  } else if (navigator.userAgent.indexOf("Safari") != -1) {
    return "Safari";
  } else if (navigator.userAgent.indexOf("Firefox") != -1) {
    alert("Mozilla Firefox does not support this game");
    return "Firefox";
  } else if (
    navigator.userAgent.indexOf("MSIE") != -1 ||
    !!document.documentMode == true
  ) {
    return "IE"; //crap
  } else {
    return "Unknown";
  }
})();

var SpeechRecognition = SpeechRecognition || webkitSpeechRecognition;
var SpeechGrammarList = SpeechGrammarList || webkitSpeechGrammarList;
var SpeechRecognitionEvent =
  SpeechRecognitionEvent || webkitSpeechRecognitionEvent;

//================================================================//
//Set Command
var commands = getCommands();
var directions = commands.directions;

function getCommands() {
  let result = {};
  $.ajax({
    url: "commands.json",
    async: false,
    success: function (data) {
      result = data;
    },
    error: function (xhr, textStatus, errorMessage) {
      //If error, everything is default
      console.log("Error: " + xhr.status + " " + errorMessage);
      result = {
        seconds: 60,
        directions: {
          up: ["up", "banana", "car"],
          down: ["down", "apple"],
          right: ["right", "watermelon"],
          left: ["left", "mango"],
        },
        restart: "restart",
        reset: "reset",
      };
    },
  });
  return result;
}

//================================================================//
//SFX
var countdown321 = document.createElement("audio");
countdown321.src = "SFX/Countdown(321).mp3";
countdown321.volume = 0.8;
var countdownStart = document.createElement("audio");
countdownStart.src = "SFX/Countdown(Start).mp3";
countdownStart.volume = 0.8;
var backgroundMusic = document.createElement("audio");
backgroundMusic.src = "SFX/BackgroundMusic.mp3";
backgroundMusic.volume = 0.8;
var levelSFX = document.createElement("audio");
levelSFX.src = "SFX/ChangeLevel.mp3";
levelSFX.volume = 1;

var movementSFX = [
  (C6 = new Audio("SFX/C6.mp3")),
  (D6 = new Audio("SFX/D6.mp3")),
  (E6 = new Audio("SFX/E6.mp3")),
  (F6 = new Audio("SFX/F6.mp3")),
  (G6 = new Audio("SFX/G6.mp3")),
  (A6 = new Audio("SFX/A6.mp3")),
  (B6 = new Audio("SFX/B6.mp3")),
  (C7 = new Audio("SFX/C7.mp3")),
];

var wrongSFX = new Audio("SFX/Wall slam.mp3");
wrongSFX.volume = 0.8;
//================================================================//

var recognition = new SpeechRecognition();
recognition.continuous = true;
recognition.lang = "en-US";
recognition.interimResults = false;
recognition.maxAlternatives = 1;

navigator.getUserMedia =
  navigator.getUserMedia ||
  navigator.webkitGetUserMedia ||
  navigator.mozGetUserMedia ||
  navigator.msGetUserMedia;

var countdownToLobby;
// navigator.permissions.query({name: "microphone"})
// .then(({state}) => {
//   console.log(state)
//   clearInterval(countdownToLobby);

//   if(state!="granted")
//   {
//     window.parent.wsRequireHardwareMessage("microphone");

//     countdownToLobby= setInterval(runRedirectToLobby, 100000);
//   }

// },
//   e => {
//     console.log(e.name +": "+ e.message)
//   });

//Invoke mic access
var micStart = false;
var diagnostic = document.querySelector(".output");
var bg = document.querySelector("html");
var commandText = ""; // used for display to the users what speech is recognized
var commandsFromWords = []; // Check if there are commands exist within a speech
var mic = document.querySelector("#btn-mic");
var micchecking;

//================================================================//
//Microphone Access
function startMic() {
  navigator.getUserMedia(
    { audio: true },
    (stream) => {
      micStart = true;
      micchecking = setInterval(checkCamera, 1000);
    },
    (err) => {
      console.log(err);
      // alert(
      //     "Please turn on your microphone and refresh\n" +
      //     "Tips: Click on the mic / video button by your URL"
      // );
      parent.location.href = "../error/hardware_error_page.php?hardware=mic";
      //window.parent.wsRequireHardwareMessage("microphone");

      // console.log("start count down to lobby")
      // countdownToLobby= setInterval(runRedirectToLobby, 100000);
    }
  );
}

function checkCamera() {
  navigator.getUserMedia(
    { audio: true },
    function () {
      // mic is available
    },
    function () {
      // mic is not available
      if (!isEnd)
        parent.location.href = "../error/hardware_error_page.php?hardware=mic";
    }
  );
}

//================================================================//

function runRedirectToLobby() {
  parent.location.href = "https://fuyoh-ads.com/covid-19/dashboard";
}

var checkCommandChange = {
  commandChange: false,
  set testVar(value) {
    this.commandChange = true;
  },
};

mic.onmousedown = () => {
  recognition.start();
  console.log("Ready to receive a command.");
  mic.style.filter = "brightness(2)";
  document.querySelector("#speech-bubble").src = "Assets/speech-bubble2.png";
  diagnostic.innerHTML =
    "<span class='highlight-gray'>Release to show text</span>";
  backgroundMusic.volume = 0.3;
  //document.querySelector("#mic-container").style.animation = "changecolor 0.5s normal forwards";
};
mic.onmouseup = () => {
  recognition.stop();
  console.log("Recognition stopped.");
  mic.style.filter = "brightness(1)";
  document.querySelector("#speech-bubble").style.display = "none";
  //document.querySelector("#mic-container").style.animation = "none";
};
if (mic.onmousedown) {
  mic.onmouseleave = () => {
    recognition.stop();
    console.log("Recognition stopped.");
    mic.style.filter = "brightness(1)";
    document.querySelector("#speech-bubble").style.display = "none";
    //document.querySelector("#mic-container").style.animation = "none";
  };
}

//Not in use yet
recognition.onaudiostart = function () {
  //document.querySelector("#guide").innerHTML = "we are listening";
};

recognition.onend = function () {
  if (checkCommandChange.commandChange) {
    diagnostic.innerHTML = commandText;
    doCommand(commandsFromWords);
    checkCommandChange.commandChange = false;
  } else {
    diagnostic.innerHTML =
      "no speech detected </br ></br > <span class='highlight-gray'>Click and hold the mic</span>";
    backgroundMusic.volume = 0.8;
  }
};

recognition.onerror = function (event) {
  diagnostic.innerHTML = "Error occurred in recognition: " + event.error;
};

//Invoke first maze creation
(function () {
  maze = createMaze("small");
})();

recognition.onresult = function (event) {
  diagnostic.innerHTML = "Loading text...";
  // The SpeechRecognitionEvent results property returns a SpeechRecognitionResultList object
  // The SpeechRecognitionResultList object contains SpeechRecognitionResult objects.
  // It has a getter so it can be accessed like an array
  // The [last] returns the SpeechRecognitionResult at the last position.
  // Each SpeechRecognitionResult object contains SpeechRecognitionAlternative objects that contain individual results.
  // These also have getters so they can be accessed like arrays.
  // The [0] returns the SpeechRecognitionAlternative at position 0.
  // We then return the transcript property of the SpeechRecognitionAlternative object

  //Confidence is the accuracy of received speech
  var last = event.results.length - 1;
  var command = event.results[last][0].transcript;
  var confidence = event.results[0][0].confidence;
  //To remove all possible errors like "." and "," and upper cases
  command = command.toLowerCase();
  command = command.split(".").join("");
  command = command.split(",").join("");
  command = command.split("?").join("");
  command = command.split("$").join("");

  date = Math.floor(Date.now() / 1000);
  commandsReceived.push({
    command: command,
    "unix-timestamp": date,
    confidence: confidence,
  });

  // bg.style.backgroundColor = command;
  console.log("command: " + command + " ,confidence: " + confidence);

  // because a command can contain multiple words
  // we need to split it.
  let words = command.split(" ");
  commandText = ""; // used for display to the users what speech is recognized
  commandsFromWords = []; // Check if there are commands exist within a speech
  let isAdded = false; // to check if the word is to be highlighted. If true the highlighted word does not add multiple times
  var index = 0; // In case there are multiple commands. We are to differentiate them with different id index
  let textLength = 0;

  (function () {
    for (let i = 0; i < words.length; i++) {
      textLength += words[i].length;
      if (textLength >= 50) {
        words.splice(i, words.length - i);
        words.push("...");
        return;
      }
    }
  })();

  for (let j = 0; j < words.length; j++) {
    isAdded = false;
    if (commandText != "") commandText += " ";
    for (let i in directions) {
      if (directions[i].includes(words[j])) {
        commandsFromWords.push(words[j]);
        commandText +=
          "<span id='highlight-" + index + "'>" + words[j] + "</span>";
        isAdded = true;
        index++;
      }
      if (words[j] == commands.restart && !isAdded) {
        commandsFromWords.push(words[j]);
        commandText +=
          "<span id='highlight-" + index + "'>" + words[j] + "</span>";
        isAdded = true;
        index++;
      }
      if (words[j] == commands.reset && !isAdded) {
        commandsFromWords.push(words[j]);
        commandText +=
          "<span id='highlight-" + index + "'>" + words[j] + "</span>";
        isAdded = true;
        index++;
      }
    }
    if (isAdded === false) commandText += words[j];
  }
  //console.log(commandsFromWords);
  checkCommandChange.testVar = command;

  // This is disabled since Edge cannot detect confidence
  // if (confidence > 0.2) {
  //     doCommand(commandsFromWords);
  //     //executeVoiceCommands(commandsFromWords, confidence);
  // } else {
  //     diagnostic.textContent = "Not sure that I understand your command.";
  //     //executeVoiceCommands(commandsFromWords, confidence);
  // }
};

//checkCommand() decides where counter should stop at
//Eg: If it stops at 3, it returns true, then it will execute the "left" obj (4th obj) afterwards
var counter = 0;

function checkCommand(command) {
  for (let i in directions) {
    //loop through objs
    if (directions[i].includes(command)) {
      //console.log("true");
      return true;
    }
    counter++;
  }
  return false;
}

// Execute commands
function doCommand(command) {
  let executed = false;
  let movementCounter = 0;
  let SFXCounter = 0;
  let SFXReverse = false; //When movement sound 'Do Re Mi..' reach higher pitch 'Do', it reverses

  try {
    // "try" attempt to catch when wrong command detected, not in used yet
    if (command.length != 0) {
      document.querySelector("#mic-toggle").style.display = "block";

      for (let i = 0; i < command.length; i++) {
        if (command[i].indexOf(commands.restart) >= 0) {
          maze.restart();
          event += "returnspawnpoint";
          document.getElementById("highlight-" + i).className =
            "highlight-yellow";
        } else if (command[i].indexOf(commands.reset) >= 0) {
          maze.newGame();
          event += "resetmaze";
          document.getElementById("highlight-" + i).className =
            "highlight-yellow";
        }
      }

      if (event != "returnspawnpoint" && event != "resetmaze") {
        let movementDelay = setInterval(() => {
          if (event != "") event += ", ";
          if (checkCommand(command[movementCounter])) {
            if (Object.keys(directions)[counter] == "up") {
              executed = maze.moveUp();
              //console.log("up");
              if (executed) {
                event += "move up";
                totalmovement++;
              } else {
                event += "move up rejected";
              }
            } else if (Object.keys(directions)[counter] == "down") {
              executed = maze.moveDown();
              //console.log("down");
              if (executed) {
                event += "move down";
                totalmovement++;
              } else {
                event += "move down rejected";
              }
            } else if (Object.keys(directions)[counter] == "right") {
              executed = maze.moveRight();
              //console.log("right");
              if (executed) {
                event += "move right";
                totalmovement++;
              } else {
                event += "move right rejected";
              }
            } else if (Object.keys(directions)[counter] == "left") {
              executed = maze.moveLeft();
              //console.log("left");
              if (executed) {
                event += "move left";
                totalmovement++;
              } else {
                event += "move left rejected";
              }
            }
          } else {
            event += "";
          }
          counter = 0;
          if (executed) {
            document.getElementById("highlight-" + movementCounter).className =
              "highlight-green";
            movementSFX[SFXCounter].play();
          } else {
            document.getElementById("highlight-" + movementCounter).className =
              "highlight-red";
            wrongSFX.play();
            document.querySelector(".game-screen").style.animation =
              "shake 0.4s linear";
            setTimeout(() => {
              document.querySelector(".game-screen").style.animation = "none";
            }, 400);
            clearInterval(movementDelay);
            //throw new Error("exit");
          }

          if (SFXReverse == false) {
            SFXCounter++;
            if (SFXCounter == 7) {
              SFXReverse = true;
            }
          } else {
            SFXCounter--;
            if (SFXCounter == 0) {
              SFXReverse = false;
            }
          }
          movementCounter++;
          if (movementCounter >= command.length) {
            clearInterval(movementDelay);
          }
        }, 500);
      }
      setTimeout(() => {
        document.querySelector("#mic-toggle").style.display = "none";
        diagnostic.innerHTML =
          "<span class='highlight-gray'>Press and hold the mic</span>";
        commandsReceived[commandsReceived.length - 1].event = event;
        event = "";
        backgroundMusic.volume = 0.8;
      }, (command.length + 1) * 500); // +1 to ensure all the commands are complete processing
    } else {
      diagnostic.innerHTML +=
        "</br ></br > <span class='highlight-gray'>Press and hold the mic</span>";
      backgroundMusic.volume = 0.8;
    }
  } catch (e) {
    //console.log("enter error")
    if (e === "exit") {
      clearInterval(movementDelay);
      return;
    }
  }
}

//Not in use
recognition.onnomatch = function (event) {
  diagnostic.textContent = "I didn't recognise that color.";
};
