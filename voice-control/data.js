var playWidth = document.querySelector(".game-screen").clientWidth;
var playHeight = document.querySelector(".game-screen").clientHeight;
var commandsReceived = [];
var date;
var gameStartTime;
var endTime;
var totalsec = sec;
var event = ""; // event is used to collect whether user moved in direction or reset the game
var totalmovement;
var mazesize = [];
var finishedmaze = 0;
var remainingtime;
var numberOfMaze = 4;

var mazeFinishedTime = (function () {
  let setMazeAmount = {};
  for (let i = 0; i < numberOfMaze; i++) {
    setMazeAmount["maze_" + (i + 1)] = null;
  }
  return setMazeAmount;
})();

function dlData() {
  var data = new Object();

  remainingtime = sec;

  //=============================
  //Game setting
  data.gameSetting = {
    totalsec: totalsec,
    commands: commands,
    mazesize: mazesize,
    browser: browserUsed,
    numberOfMaze: numberOfMaze,
  };
  //=============================
  //Events
  data.gameEvent = {
    score: score,
    commandsReceived: commandsReceived,
    finishedmaze: finishedmaze,
    timestamps: {
      starttime: gameStartTime,
      endtime: endTime,
      remainingtime: remainingtime,
      mazeFinishedTime: mazeFinishedTime,
    },
  };
  //=============================
  //GUIs data

  data.GUIs = {
    playregionsize: {
      playregionwidth: playWidth,
      playregionheight: playHeight,
    },
    windowsize: {
      windowwidth: window.innerWidth,
      windowheight: window.innerHeight,
    },
  };
  //=============================

  // $.ajax({
  //     type: "POST",
  //     url: "gamedata.php",
  //     data: {
  //         startTime: data.starttime,
  //         performance: data.performance,
  //         handDetection: data.handDetection,
  //         loadingTimeTaken: data.loadingTimeTaken,
  //     },
  //     // on success do nothing
  // });

  // const text = JSON.stringify(data);
  // const name = "data.json";
  // const type = "text/plain";

  // const a = document.createElement("a");
  // const file = new Blob([text], { type: type });
  // a.href = URL.createObjectURL(file);
  // a.download = name;
  // document.body.appendChild(a);
  // a.click();
  // a.remove();

  return data;
}
