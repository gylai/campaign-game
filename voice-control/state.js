var sec = 120;
var countDiv = document.getElementById("timer");
var isEnd = false;
var isStart = false;
var score = 0;

var scoreboard = new Audio();
scoreboard.src = "../audio/scoreboard(edited).mp3";
var soundVol = 0;

function secpass() {
    "use strict";

    var min = Math.floor(sec / 60), //remaining minutes
        remSec = sec % 60; //remaining seconds

    if (remSec < 10) {
        remSec = "0" + remSec;
    }
    if (min < 10) {
        min = "0" + min;
    }
    countDiv.innerHTML = min + ":" + remSec;

    sec = sec - 1;
}

var countDown = setInterval(() => {
    if (isStart === true && sec > 0) {
        ("use strict");
        secpass();
    } else if (sec <= 0) {
        timeout();
    }
}, 1000);

function timeout() {
    if (sec > 0 && finishedmaze != 4) {
        document.querySelector(".game-screen").style.animation = "fadeout 1s";
        levelSFX.play();
        backgroundMusic.volume = 0.3;
        setTimeout(() => {
            document.querySelector("#level").innerHTML = finishedmaze + 1 + " / 4";
            backgroundMusic.volume = 0.8;
            document.querySelector(".game-screen").style.animation = "none";
            if (finishedmaze < 2) {
                maze = createMaze("small");
            } else {
                maze = createMaze("medium");
            }
        }, 900);
    } else {
        if (isEnd) return;
        isEnd = true;
        isStart = false;

        endTime = Math.floor(Date.now() / 1000);
        let jsonData = dlData();
        window.parent.wsCreateScore(score, jsonData);

        clearInterval(micchecking);
        clearInterval(countDown);

        backgroundMusic.pause();
        document.querySelector("#score").innerHTML = score;
        //document.querySelector("#highscore").innerHTML = score;

        if (finishedmaze == 4)
            document.querySelector("#img-scoreboard-bg").src =
            "Assets/Assets/voicecontrol(Complete).jpg";
        else
            document.querySelector("#img-scoreboard-bg").src =
            "Assets/Assets/voicecontrol(Timesup).jpg";

        //Scoreboard button links
        var campaign_name = document.getElementById("cmname").innerHTML;

        document.getElementById("share").addEventListener("click", function() {
            let fburl =
                "https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Ffuyoh-ads.com%2Fcovid-19%3Fogtitle%3DHighscore%2Bin%2B" +
                campaign_name +
                "%26ogdescription%3DFuyohhhhhh%2521%2BI%2Bjust%2Bscored%2B" +
                score +
                "%2Bin%2B" +
                "Voice-Control" +
                "%2521%26ogimage%3Dhttps%253A%252F%252Ffuyoh-ads.com%252Fgame_web%252Fimage%252F02-voicecontrol.jpg&amp;src=sdkpreparse";
            window.open(
                fburl,
                "_blank",
                "height=300,width=400,left=550,top=10,resizable=yes,scrollbars=yes"
            );
        });

        //End game transitions
        setTimeout(() => {
            let rgba = 0;
            let dim = setInterval(() => {
                document.querySelector("#gameovercontainer").style.backgroundColor =
                    "rgba(0,0,0," + rgba + ")";
                rgba += 0.02;

                if (rgba >= 0.7) clearInterval(dim);
            }, 10);

            document.querySelector(".game-screen").style.animation = "fadeout 1.5s";
            setTimeout(() => {
                document.querySelector(".game-screen").style.display = "none";
                document.querySelector(".container-fullScreen").style.display = "flex";
                var complete_bgm = document.getElementById("complete");
                complete_bgm.play();
                document.getElementById("divGameCompleted").style.display = "block";
                setTimeout(function() {
                    complete_bgm.pause();
                    scoreboard.play();
                    startConfetti();
                    document.getElementById("gameover").style.display = "flex";
                }, 3000);
            }, 1000);
        }, 1000);
    }
}

//=============================
// Splash Screen and Tutorial

window.onload = () => {
    tutorial.pause();
    pregameResize();
    startMic();
    // proceed.style.display = "block"; //  debug use

    let playTutorialVideo = setInterval(() => {
        if (tutorial.played.length > 0) {
            document.querySelector("#splashscreen").style.backgroundColor =
                "transparent";
            document.querySelector("#splashscreen").style.display = "none";
            $(".loading").hide();
            clearInterval(playTutorialVideo);
        } else {
            tutorial.play();
            console.log("Trying to play tutorial.");
        }
    }, 2000);
};

window.onresize = pregameResize;

function pregameResize() {
    // w: width, h: height
    let w = window.innerWidth;
    let h = window.innerHeight;
    let targetWidth = w;
    if (h >= (w / 16) * 9) {
        $("body").css({
            width: "100vw",
            height: "calc(100vw/16*9)",
        });
    } else {
        (targetWidth = (h / 9) * 16),
        $("body").css({
            width: "calc(100vh/9*16)",
            height: "100vh",
        });
    }
    document.documentElement.style.setProperty(
        "--target-width",
        targetWidth + "px"
    );
}

var proceed = document.querySelector("#proceed");
var loadingtext = document.querySelector("#loadingtext");
var loadingscreen = document.querySelector("#loadingscreen");
var tutorial = document.querySelector("#tutorial");
var exitBtn = document.querySelector("#exit");

let loadPercentages = 0;

setTimeout(function() {
    if (loadPercentages < 99) {
        alert("We detect you are having a slow connection, you might experience longer loading time or abnormal gaming experience, please find an alternative reliable connectivity.");
    }
}, 30000);
let checkbuffer = function() {
    if (loadPercentages < 99) {
        let range = 0;
        let bf = this.buffered;
        let time = this.currentTime;
        let loadPercentage = bf.end(range) / this.duration * 100;
        loadPercentages = loadPercentage;
    } else {
        tutorial.removeEventListener('timeupdate', checkbuffer);
    }
};
tutorial.addEventListener('timeupdate', checkbuffer);
tutorial.onended = () => {
    tutorial.play();
    tutorial.loop = true;
    proceed.style.display = "block";
    exitBtn.style.display = "block";
};

proceed.onclick = () => {
    if (!micStart) {
        alert(
            "Please allow microphone access and retry.\n" +
            "Tips: Click on the mic / video button by your URL."
        );
    } else {
        isProceed = true;
        //clearInterval(proceedAnim);
        exitBtn.style.display = "none";
        proceed.style.display = "none";
        document.querySelector("#tutorial").style.display = "none";
        countdown321.play();
        loadingscreen.style.display = "flex";
        loadingtext.innerHTML = "Game Loading..<br />3..";
        setTimeout(() => {
            loadingtext.innerHTML = "Ready..<br />2..";
            loadingscreen.style.backgroundColor = "#550046";
            countdown321.play();
            setTimeout(() => {
                loadingtext.innerHTML = "Set..<br />1..";
                loadingscreen.style.backgroundColor = "#000790";
                countdown321.play();
                setTimeout(() => {
                    loadingtext.innerHTML = "Go!!!";
                    loadingscreen.style.backgroundColor = "#38a838";
                    countdownStart.play();
                    setTimeout(() => {
                        gameStartTime = Math.floor(Date.now() / 1000);
                        document.querySelector("#index").style.display = "none";
                        document.querySelector("#speech-bubble").style.display = "block";
                        backgroundMusic.play();
                        backgroundMusic.loop = true;
                        isStart = true;
                        start();
                    }, 1000);
                }, 1000);
            }, 1000);
        }, 1000);
    }
};

//To ISO8601 Time Format
function ISODateString(d) {
    function pad(n) {
        return n < 10 ? "0" + n : n;
    }
    return (
        d.getUTCFullYear() +
        "-" +
        pad(d.getUTCMonth() + 1) +
        "-" +
        pad(d.getUTCDate()) +
        "T" +
        pad(d.getUTCHours()) +
        ":" +
        pad(d.getUTCMinutes()) +
        ":" +
        pad(d.getUTCSeconds()) +
        "Z"
    );
}