<?php include("connect.php"); ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <title>Voice controlled maze</title>

    <link rel="stylesheet" href="css/animation.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="../css/loading.css">
    <script src="confetti.js"></script>

    <audio id="complete" src="../audio/complete.mp3"></audio>
</head>

<body>

    <!-- Intro -->
    <div class="loading">Loading...</div>

    <div id="index">
        <div id="splashscreen">
            <video autoplay muted loop poster="Assets/Assets/bg-04.jpg">
                <source type="video/mp4" src="Assets/Splash Screen/VoiceControl.mp4" />
                Your browser does not support the video tag
            </video>
        </div>

        <button id="proceed">
            start game
        </button>

        <button id="exit" onclick="parent.location.href='https://fuyoh-ads.com/covid-19/dashboard/';">
            exit
        </button>

        <video id="tutorial" autoplay>
            <source type="video/mp4" src="Assets/Game Instruction/VoiceControl.mp4">
            Your browser does not support the video tag.
        </video>

        <div id="loadingscreen">
            <p id="loadingtext"></p>
            <img id="loadingbar" src="img/loadingbar23.gif" />
        </div>
    </div>

    <!-- Game over screen-->

    <div id="gameovercontainer">
        <div class="divGameCompleted container-fullScreen" id="divGameCompleted">
            <img id="img-scoreboard-bg" class="img-scoreboard" src="Assets/Assets/bg-04.jpg" />
        </div>

        <div class="container-fullScreen" id="gameover">
            <img id="gameoverui" src="Assets/Assets/bg-10.jpg" />
            <div id="gameovercontent">
                <p id="score-txt">score</p>
                <span id="score">0</span>
                <button id="share" data-layout="button" data-size="small">
                    <a target="_blank" class="fb-xfbml-parse-ignore" id="fbshare-link" style="color:#FFF; text-decoration: none;">
                        share
                    </a>
                </button>

                <button id="continue" onclick="parent.location.href='https://fuyoh-ads.com/covid-19/dashboard/';">
                    continue
                </button>
            </div>
        </div>
    </div>

    <!-- Game Screen -->

    <div class="game-screen">
        <div class="game-section">

            <div id="maze-finished-container">
                <p>Level</p>
                <span id="level">0 / 4</span>
            </div>

            <div id="timer-container">
                <p>Time left</p>
                <span id="timer"></span>
            </div>

            <div id="maze-wrapper">
                <div id='maze'>
                </div>
            </div>
        </div>
        <div class="game-instruction">
            <div id="instruction-container">

                <div id="instruction-text">
                    <p>Use your mic and try to speak the below words...</p>
                </div>

                <div id="up-text">
                    <img src="Assets/Assets/img-05.png"></img>
                    <p>wash hand</p>
                </div>

                <div id="right-text">
                    <img src="Assets/Assets/img-07.png"></img>
                    <p>kill virus</p>
                </div>

                <div id="left-text">
                    <img src="Assets/Assets/img-06.png"></img>
                    <p>stay home</p>
                </div>

                <div id="down-text">
                    <img src="Assets/Assets/img-08.png"></img>
                    <p>vaccination</p>
                </div>
            </div>
            <div id="output-container">
                <p class="output"></p>
            </div>
            <div id="mic-container">

                <img id="speech-bubble" src="Assets/speech-bubble1.png"></img>
                <img id="mic-toggle" src="Assets/Assets/img-04.png"></img>
                <button class="" id="btn-mic" title="Click to speak">
                </button>

                <!--
                <p id="mic-text">    
                    Press & hold the button to start speaking.
                </p>
                -->
            </div>
        </div>
    </div>

    <p id="cmname" style="display:none;"></p>
    <script src="state.js"></script>
    <script src="data.js"></script>
    <script src="maze.js"></script>
    <script src="app.js"></script>
</body>

</html>

<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v11.0" nonce="JKRxb3HX">
</script>

<script>
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    var url = new URL(window.location.href);
    var campaign_name = url.searchParams.get("campaign_name");

    if (campaign_name) {
        document.getElementById("cmname").innerHTML = campaign_name;
    } else {
        document.getElementById("cmname").innerHTML = "Voice-Control";
    }
</script>