//to record time taken
var startTime, endTime;

function start() {
  startTime = performance.now();
}

function end() {
  endTime = performance.now();
  var timeDiff = endTime - startTime; //in ms
  // strip the ms
  timeDiff /= 1000;

  // get seconds
  var seconds = Math.round(timeDiff);
  return seconds;
}

//Implemented Prim's Algorithm on maze's logic

//making the maze
class Maze {
  constructor(height, width, maze, walls, currentPosition) {
    this.height = height % 2 == 0 ? height + 1 : height;
    this.width = width % 2 == 0 ? width + 1 : width;
    this.maze = maze;
    this.walls = walls;
    this.currentPosition = currentPosition;

    // this is also set in the CSS under .block
    // this.blockHeight = 40;
    // this.blockWidth = 40;
  }

  createMaze() {
    let self = this;

    const mazeElement = document.getElementById("maze");

    mazeElement.innerHTML = "";
    // mazeElement.setAttribute('style', 'height:' + this.height * this.blockHeight + 'px; width:' + this.width * this.blockWidth + 'px');

    // fill the whole maze with walls
    for (var y = 0; y < this.height; y++) {
      this.maze[y] = [];
      for (var x = 0; x < this.width; this.maze[y][x++] = "wall") {
        var el = mazeElement.appendChild(document.createElement("div"));
        el.className = "block wall";
        el.setAttribute("id", y + "-" + x);
      }
    }

    this.amaze(this.currentPosition.y, this.currentPosition.x, true);

    while (this.walls.length != 0) {
      //get random coords of wall, choose which wall to remove
            var randomWall = this.walls[
                Math.floor(Math.random() * this.walls.length)
            ],
                host = randomWall[2], //gets (y,x) from this.walls
                opposite = [
                    host[0] + (randomWall[0] - host[0]) * 2, //y
                    host[1] + (randomWall[1] - host[1]) * 2, //x
                ];
            if (this.valid(opposite[0], opposite[1])) {
                if (this.maze[opposite[0]][opposite[1]] == "maze")
                    this.walls.splice(this.walls.indexOf(randomWall), 1);
                else
                    this.amaze(randomWall[0], randomWall[1], false),
                        this.amaze(opposite[0], opposite[1], true);
            } else this.walls.splice(this.walls.indexOf(randomWall), 1);
    }

    document.getElementById("0-0").className = "block me";

    document.getElementById(
      parseInt(this.height) - 1 + "-" + (parseInt(this.width) - 1)
    ).className = "block finish";

    // Removes key control
    // document.body.onkeydown = function (e) {
    //   switch (e.keyCode) {
    //     case 38:
    //       self.moveUp();
    //       break;
    //     case 40:
    //       self.moveDown();
    //       break;
    //     case 39:
    //       self.moveRight();
    //       break;
    //     case 37:
    //       self.moveLeft();
    //       break;
    //   }
    //   // var newPosition = self.createNewPosition(e.keyCode);
    //   //self.movePlayer(self, self.currentPosition, newPosition);
    // };
  }
  /**
   * Moves the player to a new position
   * Returns "True" if player was moved and "False" if failed (because of a wall)
   */
  movePlayer(maze, oldPosition, newPosition) {
    if (
      maze.valid(newPosition.y, newPosition.x) &&
      maze.maze[newPosition.y][newPosition.x] != "wall"
    ) {
      document
        .getElementById(oldPosition.y + "-" + oldPosition.x)
        .classList.remove("me");
      // update current position
      maze.currentPosition = newPosition;
      document
        .getElementById(newPosition.y + "-" + newPosition.x)
        .classList.add("me");
      if (maze.isFinished()) {

          var totalSeconds = end();
          finishedmaze++;

          mazeFinishedTime["maze_" + (finishedmaze)] = Math.floor(Date.now() / 1000);

          if (finishedmaze <= 2) {
              score += 200
          } else {
              score += 300;
          }
          timeout();
      }
      return true;
    } else {
      return false;
    }
  }
  //set player position to starting and make a new maze
  newGame() {
    document.getElementById("maze").innerHTML = "";
    this.walls = [];
    this.currentPosition = { x: 0, y: 0 };
    this.createMaze();
  }
  //set player position to starting using same maze
  restart() {
    if (this.isFinished()) {
      document.getElementById(
        parseInt(this.height) - 1 + "-" + (parseInt(this.width) - 1)
      ).className = "block finish";
    }
    this.movePlayer(this, this.currentPosition, { x: 0, y: 0 });
  }

  amaze(y, x, addBlockWalls) {
    let maze = this.maze;

    maze[y][x] = "maze";
    document.getElementById(y + "-" + x).className = "block";
    //surround an area with wall
    if (addBlockWalls && this.valid(y + 1, x) && this.maze[y + 1][x] == "wall")
      this.walls.push([y + 1, x, [y, x]]);
    if (addBlockWalls && this.valid(y - 1, x) && this.maze[y - 1][x] == "wall")
      this.walls.push([y - 1, x, [y, x]]);
    if (addBlockWalls && this.valid(y, x + 1) && this.maze[y][x + 1] == "wall")
      this.walls.push([y, x + 1, [y, x]]);
    if (addBlockWalls && this.valid(y, x - 1) && this.maze[y][x - 1] == "wall")
      this.walls.push([y, x - 1, [y, x]]);
  }

  valid(a, b) {
    return a < this.height && a >= 0 && b < this.width && +b >= 0 ? true : false;
  }

  isFinished() {
    return (
      this.currentPosition.y == this.height - 1 &&
      this.currentPosition.x == this.width - 1
    );
  }
  /**
   *  control functions
   *  Returns "True" if player was moved and "False" if failed (because of a wall)
   */
  //to move the icon/circle
  moveDown() {
    const newPosition = {
      x: this.currentPosition.x,
      y: this.currentPosition.y + 1,
    };
    // const newPosition = this.createNewPosition('down');
    return this.movePlayer(this, this.currentPosition, newPosition);
  }
  moveUp() {
    const newPosition = {
      x: this.currentPosition.x,
      y: this.currentPosition.y - 1,
    };
    // const newPosition = this.createNewPosition('down');
    return this.movePlayer(this, this.currentPosition, newPosition);
  }
  moveRight() {
    const newPosition = {
      x: this.currentPosition.x + 1,
      y: this.currentPosition.y,
    };
    // const newPosition = this.createNewPosition('down');
    return this.movePlayer(this, this.currentPosition, newPosition);
  }
  moveLeft() {
    const newPosition = {
      x: this.currentPosition.x - 1,
      y: this.currentPosition.y,
    };
    // const newPosition = this.createNewPosition('down');
    return this.movePlayer(this, this.currentPosition, newPosition);
  }
}

//creating maze base on difficulty and then set the maze size

let maze;

function createMaze(size) {
  //size = "medium" //fix to small maze
  mazesize.push(size);
  let mazeWidth = 5;
  let mazeHeight = 5;

  const mazeElement = document.getElementById("maze");

  switch (size) {
    case "small":
      mazeWidth = 5;
      mazeHeight = 5;
      mazeElement.className = "small-maze";
      break;
    case "medium":
      mazeWidth = 7;
      mazeHeight = 7;
      mazeElement.className = "medium-maze";
      break;
    case "large":
      mazeWidth = 9;
      mazeHeight = 9;
      mazeElement.className = "large-maze";
      break;
    case "verylarge":
      mazeWidth = 11;
      mazeHeight = 11;
      mazeElement.className = "very-large-maze";
      break;
    default:
      mazeWidth = 5;
      mazeHeight = 5;
      mazeElement.className = "small-maze";
      break;
  }

  let startPosition = { x: 0, y: 0 };

  let maze = new Maze(mazeHeight, mazeWidth, [], [], startPosition);
  maze.createMaze();

  return maze;
}
