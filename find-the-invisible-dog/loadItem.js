var itemObj = getItem();

function getItem() {
    var result = {};
    $.ajax({
        url: "items.json",
        async: false,
        success: function(data) {
            result = data;
        },
        error: function(xhr, textStatus, errorMessage) { //If error, everything is default
            console.log("Error: " + xhr.status + " " + errorMessage);
            result = {
                "BGM": "Items/bgm/Sneaky Snitch.mp3",
                "fastBGM": "Items/bgm/Sabre Dance.mp3",
                "winAudio": "Items/bgm/default/win.mp3",
                "loseAudio": "Items/bgm/default/lose.mp3",
                "targetAudio": "Items/Audio/default/catch dog.mp3",
                "hintAudio": "Items/doorbell.mp3",
                "targetNearby": "Items/Audio/default/dog.mp3",
                "trapAudio": "Items/Audio/default/catch cat.mp3",
                "trapNearby": "Items/Audio/default/cat.mp3",
                "handImg": "Assets/Props/img-08.png",
                "timerGUI": "Assets/Props/img-01.png",
                "basketGUI": "Assets/Props/img-02.png",
                "vcanvasGUI": "Assets/Props/img-03.png",
                "fenceGUI": "Assets/Props/bg-041.png",
                "infoBoard": "Assets/Props/img-04.png",
                "hintImg": "Assets/GIF/present.gif",
                "openingHint": "Assets/GIF/presentPopUp.gif",
                "openedHint": "Assets/GIF/presentPopUp2.gif",
                "canvasBG": "Assets/Props/bg-051.png",
                "backgroundImg": "AssetsProps//bg-02.png",
                "targetImg": "Assets/Props/img-05.png",
                "trapImg": "Assets/Props/img-06.png",
                "basketDog": "Assets/Props/img-09.png",
                "ads": ["Assets/Ads/pm2.png", "Assets/Ads/pm3.png", "Assets/Ads/pm copy.png"],
                "hintAds": "img/tmlogo.png",
                "gameOverUI": "Assets/Game Over/bg-07.jpg",
                "brocolli1": "Assets/Props/img-10.png",
                "brocolli2": "Assets/Props/img-11.png",
                "freezeUI": "Assets/Props/img-12.png",
                "loadingScreen": "Assets/Game Over/bg-06.jpg",
                "splashScreen": "Assets/SplashScreen/FindTheInvisibleDog.mp4",
                "tutorial": "Assets/tutorial.mp4",
                "timesUp": "Assets/Game Over/findtheinvisibledog(Timesup).jpg",
                "complete": "Assets/Game Over/findtheinvisibledog(Complete).jpg"
            };
        }
    });
    return result;
}

// console.log(itemObj);

//Audios
var BGM = new Audio(itemObj.BGM);
var fastBGM = new Audio(itemObj.fastBGM);
fastBGM.volume = 1;
BGM.volume = 1;
var targetAudio = new Audio(itemObj.targetAudio); //aka dog audio
var targetNearby = new Audio(itemObj.targetNearby); //aka dog nearby audio
var trapAudio = new Audio(itemObj.trapAudio);
var trapNearby = new Audio(itemObj.trapNearby);
var winAudio = new Audio(itemObj.winAudio);
var loseAudio = new Audio(itemObj.loseAudio);
var hintAudio = new Audio(itemObj.hintAudio);
hintAudio.volume = 1;
winAudio.volume = 1;
loseAudio.volume = 1;

//Screens
var loadingScreen = document.querySelector("#loadingscreen img");
loadingScreen.src = itemObj.loadingScreen;
var splashScreen = document.querySelector("#splashscreen video");
splashScreen.src = itemObj.splashScreen;
var tutorial = document.querySelector("#tutorial");
tutorial.src = itemObj.tutorial;

//GUIs
var handImg = document.querySelector("#handimg"); //1:1 ratio recommended
if (handImg) handImg.src = itemObj.handImg;
var timerGUI = document.querySelector("#timergui");
if (timerGUI) timerGUI.src = itemObj.timerGUI;
var basketGUI = document.querySelector("#basketgui");
if (basketGUI) basketGUI.src = itemObj.basketGUI;
var vcanvasGUI = document.querySelector("#vcanvasgui");
if (vcanvasGUI) vcanvasGUI.src = itemObj.vcanvasGUI;
var fenceGUI = document.querySelector(".container-item4");
if (fenceGUI) fenceGUI.style.backgroundImage = "url('" + itemObj.fenceGUI + "')";
var infoBoard = document.querySelector("#infoboard");
if (infoBoard) infoBoard.src = itemObj.infoBoard;
var hintImg = document.querySelector("#hintimg");
if (hintImg) hintImg.src = itemObj.hintImg;
var openingHint = document.querySelector("#openinghint");
if (openingHint) openingHint.src = itemObj.openingHint;
var openedHint = document.querySelector("#openedhint");
if (openedHint) openedHint.src = itemObj.openedHint;
var canvasBG = document.querySelector("#canvasbg");
if (canvasBG) canvasBG.src = itemObj.canvasBG;
var backgroundImg = document.getElementsByTagName("html")[0];
if (backgroundImg) backgroundImg.style.backgroundImage = "url('" + itemObj.backgroundImg + "')";
var targetImg = document.querySelector("#targetimg");
if (targetImg) targetImg.src = itemObj.targetImg;
var trapImg = document.querySelector("#trapimg");
if (trapImg) trapImg.src = itemObj.trapImg;
var gameOverUI = document.querySelector("#gameoverui");
if (gameOverUI) gameOverUI.src = itemObj.gameOverUI;

var whiteDog = itemObj.whiteDog;

var brocolli = document.querySelector(".brocolli");
var brocolli1 = document.querySelector(".brocolli-1");
if (brocolli && brocolli1) {
    for (let i = 0; i < 4; i++) {
        document.getElementsByClassName("brocolli")[i].src = itemObj.brocolli2;
    }
    for (let j = 0; j < 3; j++) {
        document.getElementsByClassName("brocolli-1")[j].src = itemObj.brocolli1;
    }
}
var freezeUI = document.querySelector("#freezemessage");
if (freezeUI) freezeUI.src = itemObj.freezeUI;

var timesUpSrc = itemObj.timesUp;
var completeSrc = itemObj.complete;
//================================//
//Ads
//================================//
let adsCounter = 0
var ads = document.querySelector("#ads");
if (ads) {
    ads.src = itemObj.ads[adsCounter];
    setInterval(() => {
        ads.src = itemObj.ads[adsCounter];
        adsCounter++;
        if (adsCounter > itemObj.ads.length - 1) adsCounter = 0;
    }, 5000);
}

var hintAds = document.querySelector("#hintad");
if (hintAds) hintAds.src = itemObj.hintAds;