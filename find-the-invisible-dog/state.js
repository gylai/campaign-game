var loadingStartTime = performance.now();
var isSplash = false;
let loadingEndTime, loadingTimeEllapse, loadingMin, loadingSec;

//Record loading time
function getLoadingTimeEllapse() {

    loadingEndTime = performance.now();
    loadingTimeEllapse = Math.round((loadingEndTime - loadingStartTime) / 1000);

    loadingMin = Math.floor(loadingTimeEllapse / 60);
    loadingSec = Math.floor(loadingTimeEllapse - (loadingMin * 60));

    if (loadingMin < 10) {
        loadingMin = "0" + loadingMin;
    }
    if (loadingSec < 10) {
        loadingSec = "0" + loadingSec;
    }
    loadingTimeEllapse = loadingMin + ":" + loadingSec;
    console.log("Loading Time Ellapsed : " + loadingTimeEllapse);
}

window.onload = () => {
    pregameResize();
    startVideo();
    // proceed.style.display = "block"; // debug use
    BGM.loop = true;
    isSplash = true;

    handTrack.load(modelParams).then((lmodel) => {
        model = lmodel;
    });

    let playTutorialVideo = setInterval(() => {
        if (tutorial.played.length > 0) {
            document.querySelector("#splashscreen").style.backgroundColor = "transparent";
            document.querySelector("#splashscreen").style.display = "none";
            $(".loading").hide();
            clearInterval(playTutorialVideo);
        } else {
            tutorial.play();
            BGM.play();
            console.log("Trying to play tutorial and BGM");
        }
    }, 2000);
}

function pregameResize() {
    // w: width, h: height
    let w = window.innerWidth;
    let h = window.innerHeight;
    let targetWidth = w;
    if (h >= w / 16 * 9) {
        $("#pregamecontainer").css({
            "width": "100vw",
            "height": "calc(100vw/16*9)"
        });

    } else {
        targetWidth = h / 9 * 16,
            $("#pregamecontainer").css({
                "width": "calc(100vh/9*16)",
                "height": "100vh"
            });
    }
    document.documentElement.style.setProperty('--target-width', targetWidth + "px");
}

var proceed = document.querySelector("#proceed");
var exitBtn = document.querySelector("#exit");
var tutorial = document.querySelector("#tutorial");
let loadPercentages = 0;

tutorial.onended = () => {
    tutorial.play();
    proceed.style.display = "block";
    exitBtn.style.display = "block";
}

proceed.onclick = () => {
    if (!isStart) {
        alert("The game is not ready yet, please retry after a few seconds.\n" +
            "*Please allow the camera access if you haven't done so yet.");
        // location.href = "../error/hardware_error_page.php?hardware=camera";
        return;
    }
    document.querySelector("#index").style.display = "none";
    tutorial.pause();
    document.querySelector("#loadingscreen").style.display = "block";
    document.querySelector("#loadingbar").style.display = "block";
    document.querySelector("#loadingscreen").style.display = "none";
    document.querySelector("#loadingbar").style.display = "none";
    document.querySelector("#hardware-container").style.display = "grid";
    document.querySelector("#blackscreen").style.display = "flex";
}

document.querySelector("#btnSkipInstruction").onclick = () => {
    document.querySelector("#hardware-container").style.display = "none";
    document.querySelector("#blackscreen > p").innerHTML = "Ready.. </br >3";
    setTimeout(() => {
        document.querySelector("#blackscreen > p").innerHTML = "Set.. </br >2";
        setTimeout(() => {
            document.querySelector("#blackscreen > p").innerHTML = "Go!! </br >1";
            setTimeout(() => {
                document.querySelector("#blackscreen").style.display = "none";
                isLoaded = true; //ensures game starts after loading page ends
            }, 500);
        }, 1000);
    }, 1000);
}

setTimeout(function() {
    if (loadPercentages < 99) {
        alert("We detect you are having a slow connection, you might experience longer loading time or abnormal gaming experience, please find an alternative reliable connectivity.");
    }
}, 30000);
let checkbuffer = function() {
    if (loadPercentages < 99) {
        let range = 0;
        let bf = this.buffered;
        let time = this.currentTime;
        let loadPercentage = bf.end(range) / this.duration * 100;
        loadPercentages = loadPercentage;
    } else {
        tutorial.removeEventListener('timeupdate', checkbuffer);
    }
};
tutorial.addEventListener('timeupdate', checkbuffer);