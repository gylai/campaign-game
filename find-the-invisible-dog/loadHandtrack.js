// String utility function to use with toFormattedString()
String.prototype.padLeft = function (length, character) {
  return new Array(length - this.length + 1).join(character || " ") + this;
};

// utility function to format Date() object to dd/mm/YYYY hh:mm
Date.prototype.toFormattedString = function () {
  return (
    [
      String(this.getDate()).padLeft(2, "0"),
      String(this.getMonth() + 1).padLeft(2, "0"),
      String(this.getFullYear()),
    ].join("/") +
    " " +
    [
      String(this.getHours()).padLeft(2, "0"),
      String(this.getMinutes()).padLeft(2, "0"),
      String(this.getSeconds()).padLeft(2, "0"),
    ].join(":")
  );
};

// give admin control over how many dogs and cat
var cwidth = document.querySelector("#canvascontainer").clientWidth;
var cheight = document.querySelector("#canvascontainer").clientHeight;
var spawn = [];
var extraScore = 0;
var highestScore = 0;
var score = 0;
var accumulate_bonus_time = 0;
//var catTouchNum = 0;
var catsCaptured = 0;
var dogsCaptured = 0;
var hintsCaptured = 0;
var scoreboard = new Audio();
scoreboard.src = "../audio/scoreboard(edited).mp3";
var soundVol = 0;
var camerachecking;
var usermedia;
var cameramedia;
var detection_loop;
var draw_loop;
var game_end = false;
var finishAnimateGift = false;

// variables for gift spawning
const hintRandomTime = (min, max) => {
  //gift will randomly spawn between 8 and 20
  return Math.floor(Math.random() * (max - min) + min);
};

var hintSpawn = false; //gift can be start being touched
var hintEnd = false; //a cycle of a gift spawn ends
var hintStart = false; //starts only when detect hand, isLoaded is not used because it will effect the the time gap between each appearance

var begin = 0;
var sec = 60;
var totalSec = sec; //used for gift spawning calculation;
var hintAvailableSec =
  totalSec - (totalSec * 0.1 + (totalSec - totalSec * 0.9) + numOfGift * 3); // 3 is included because the animation of every gift is 3 seconds

var stDate = Math.floor(Date.now() / 1000);
var total = 0;
var perfTime = [];
var handLocations = [];

var recordTimeTouch = [];

function checkNoOverlap(arr, obj) {
  for (var i = 0; i < arr.length; i++) {
    if (calcEuclDistance(obj, arr[i]) <= 3 * (2 * objRadius)) {
      return false;
    }
  }
  return true;
}
// target creation
for (var i = 1; i <= numOfTarget; i++) {
  var temp = new spawnableItem("dog", cwidth, cheight, objRadius);

  while (!checkNoOverlap(spawn, temp)) {
    temp.regenerateXY(cwidth, cheight);
  }
  spawn.push(temp);

  // spawn.push(new spawnableItem("dog", cwidth, cheight, objRadius));

  let emptyDogContainer = document.querySelector("#emptydogcontainer");
  let img = document.createElement("img");
  let dogSize;

  if (numOfTarget <= 6) dogSize = 0.5;
  else if (numOfTarget <= 8) dogSize = 0.4;
  else dogSize = 0.3;

  img.className = "basketdoggy";
  img.src = whiteDog;
  img.style.height = emptyDogContainer.clientHeight * dogSize + "px";
  img.style.width = emptyDogContainer.clientHeight * dogSize + "px";
  emptyDogContainer.appendChild(img);
}

// trap creation
for (var i = 1; i <= numOfTrap; i++) {
  var tempTrap = new spawnableItem("trap", cwidth, cheight, objRadius);

  while (!checkNoOverlap(spawn, tempTrap)) {
    tempTrap.regenerateXY(cwidth, cheight);
  }
  spawn.push(tempTrap);
  // spawn.push(new spawnableItem("trap", cwidth, cheight, objRadius));
}

//gift creation
for (var i = 1; i <= numOfGift; i++) {
  spawn.push(new spawnableItem("hint", cwidth, cheight, objRadius));
}

// RNG for target and trap selection
const randomIndex = Math.floor(Math.random() * numOfTarget);
// 3 target + 1 trap
// random * 3 = 0<= x <= 3
var trapIndex;
if (numOfTrap == 1) {
  trapIndex = numOfTarget;
} else {
  trapIndex = Math.floor(Math.random() * numOfTrap) + numOfTarget;
}

// put in json file from server
const modelParams = {
  flipHorizontal: true,
  imageScaleFactor: 0.7, //changed here
  maxNumBoxes: 1, // maximum number of boxes to detect
  iouThreshold: 0.6, // ioU threshold for non-max suppression
  scoreThreshold: 0.65, // confidence threshold for predictions.
};

function renderVideo() {
  vctx.drawImage(video, 0, 0, vcanvas.width, vcanvas.height);
  console.log(renderVideo);
  // setInterval(renderVideo, 1000 / 60);
}

// port over renderPrediction from handtrackjs
function renderPred(_prediction, _canvas, _context, _mediasource) {
  _context.clearRect(0, 0, _canvas.width, _canvas.height);
  _canvas.width = _mediasource.width;
  _canvas.height = _mediasource.height;
  _canvas.style.height =
    parseInt(_canvas.style.width) *
      (_mediasource.height / _mediasource.width).toFixed(2) +
    "px";
  _context.save();
  _context.scale(-1, 1);
  _context.translate(-_mediasource.width, 0);
  _context.drawImage(
    _mediasource,
    0,
    0,
    _mediasource.width,
    _mediasource.height
  );
  _context.restore();

  for (let i = 0; i < _prediction.length; i++) {
    const pred = _prediction[i];
    // _context.beginPath();
    // _context.fillStyle = "rgba(255,255,255,0.6)";

    // _context.fillRect(pred.bbox[0] + 1, pred.bbox[1]+1, pred.bbox[2]-1, 17*1.5);
    // _context.lineWidth = 2;

    roundRectangle(
      _context,
      pred.bbox[0],
      pred.bbox[1],
      pred.bbox[2],
      pred.bbox[3],
      3,
      false,
      true
    );
  }
}

navigator.getUserMedia =
  navigator.getUserMedia ||
  navigator.webkitGetUserMedia ||
  navigator.mozGetUserMedia ||
  navigator.msGetUserMedia;

// handTrack built in method to start video stream
function startVideo() {
  navigator.getUserMedia(
    { video: true },
    (stream) => {
      video.srcObject = stream;
      cameramedia = stream;
      handTrack.startVideo(video).then((status) => {
        if (status) {
          getLoadingTimeEllapse();
          // clearInterval(countdownToLobby);
          // renderVideo();
          isStart = true;
          camerachecking = setInterval(checkCamera, 1000);
          detection_loop = setInterval(runDetection, 100);
          draw_loop = setInterval(draw, 1000 / 60);
          console.log("Camera loaded");
        } else console.log("HandtrackJs fail to start video");
        // else if (!status) {
        //     window.parent.wsRequireHardwareMessage("camera");
        //     countdownToLobby = setInterval(runRedirectToLobby, 60000);
        // }
      });
    },
    (err) => {
      //alert(
      //    "Please turn on your camera and refresh\n" +
      //    "Tips: Click on the mic / video button by your URL"
      //);
      if (!game_end)
        parent.location.href =
          "../error/hardware_error_page.php?hardware=camera";
      console.log(err);
    }
  );
}

function checkCamera() {
  if (game_end) return;

  if (cameramedia.active == true) {
    //do nothing
  } else {
    parent.location.href = "../error/hardware_error_page.php?hardware=camera";
  }
}

// function runRedirectToLobby() {
//     parent.location.href = "https://fuyoh-ads.com/campaign_web/#/game";
// }

function runDetection() {
  if (model === undefined) return;
  if (game_end) return;

  var timeStart = performance.now();
  model.detect(video).then((predictions) => {
    // model.renderPredictions(predictions, vcanvas, vctx, video);
    renderPred(predictions, vcanvas, vctx, video);
    // console.log("entered model detect");
    var tid = setInterval(perfTime.push(model.getFPS()), 2000);
    var tid2 = setInterval(handLocations.push([handX, handY]), 2000);

    if (predictions.length !== 0 && isLoaded == true) {
      //isLoaded ensures the game won't start at loading page and can be set as a condition to disable the tracking

      if (begin == 0) touchTimeStart = performance.now();
      hintStart = true;

      midX = predictions[0].bbox[0] + predictions[0].bbox[2] / 2;
      midY = predictions[0].bbox[1] + predictions[0].bbox[3] / 2;

      handX = cwidth * (midX / video.width);
      handY = cheight * (midY / video.height);

      begin = 1;

      for (var i = 0; i < spawn.length; i++) {
        spawn[i].calculateDistanceHandToObject(handX, handY);
      }

      for (var i = 0; i < spawn.length; i++) {
        if (spawn[i].type == "dog" && spawn[i].isTouch == false) {
          //target
          if (
            spawn[i].x + spawn[i].radius >= handImgPosX &&
            spawn[i].x - spawn[i].radius <= handImgPosX + handRadius * 2 &&
            spawn[i].y + spawn[i].radius >= handImgPosY &&
            spawn[i].y - spawn[i].radius <= handImgPosY + handRadius * 2
          ) {
            total++;
            recordTimeTouch.push(calcTouchTime(handX, handY, "target")); //calc touch time
            spawn[i].isTouch = true;
            //console.log("touched dog");
            updateGUI(spawn[i].type, i);
            targetNearby.volume = 0.8;
            targetNearby.play();
            dogsCaptured++;
            if (total == numOfTarget) {
              display_win();
            }
          } else if (
            spawn[i].x + spawn[i].radius * 3 > handImgPosX &&
            spawn[i].x - spawn[i].radius * 3 <= handImgPosX + handRadius * 2 &&
            spawn[i].y + spawn[i].radius * 3 > handImgPosY &&
            spawn[i].y - spawn[i].radius * 3 <= handImgPosY + handRadius * 2
            //!isNearTarget
          ) {
            //console.log("near target");
            targetNearby.volume = 0.2;
            targetNearby.play();
            //isNearTarget = true;
          }
        } else if (spawn[i].type == "trap" && spawn[i].isTouch == false) {
          if (
            spawn[i].x + spawn[i].radius >= handImgPosX &&
            spawn[i].x - spawn[i].radius <= handImgPosX + handRadius * 2 &&
            spawn[i].y + spawn[i].radius >= handImgPosY &&
            spawn[i].y - spawn[i].radius <= handImgPosY + handRadius * 2
          ) {
            recordTimeTouch.push(calcTouchTime(handX, handY, "trap")); //calc touch time
            updateGUI(spawn[i].type, i);
            stopDetect();
            trapNearby.volume = 0.8;
            trapNearby.play();

            if (dogReset === true) {
              for (var j = 0; j < spawn.length; j++) {
                if (spawn.type != "trap") {
                  spawn[j].regenerateXY(cwidth, cheight);
                }
              }
            }
            if (catReset === true) {
              spawn[i].regenerateXY(cwidth, cheight);
            }
            spawn[i].isTouch = true; //cat isTouch
            catsCaptured++;
          } else if (
            spawn[i].x + spawn[i].radius * 3 > handImgPosX &&
            spawn[i].x - spawn[i].radius * 3 <= handImgPosX + handRadius * 2 &&
            spawn[i].y + spawn[i].radius * 3 > handImgPosY &&
            spawn[i].y - spawn[i].radius * 3 <= handImgPosY + handRadius * 2
          ) {
            //console.log("near cat");
            trapNearby.volume = 0.2;
            trapNearby.play();
          }
        } else if (
          spawn[i].type == "hint" &&
          spawn[i].isTouch == false &&
          hintSpawn == true &&
          spawn[i].isDespawn == false
        ) {
          if (
            spawn[i].x + spawn[i].radius >= handImgPosX &&
            spawn[i].x - spawn[i].radius <= handImgPosX + handRadius * 2 &&
            spawn[i].y + spawn[i].radius >= handImgPosY &&
            spawn[i].y - spawn[i].radius <= handImgPosY + handRadius * 2
          ) {
            recordTimeTouch.push(calcTouchTime(handX, handY, "hint")); //calc touch time
            spawn[i].isTouch = true;
            //console.log("touched hint");
            hintAudio.play();
            hintImg.style.display = "none";
            hintSpawn = false;
            hintEnd = false;
            updateGUI(spawn[i].type, i);
            isLoaded = false;
            hintsCaptured++;
          }
        }
      }
    }
  });
}

function stopDetect() {
  clearInterval(detection_loop);
  detection_loop = setInterval(runDetection, 100);
}

function calcTouchTime(x, y, touchType) {
  let date = Math.floor(Date.now() / 1000);
  //console.log(recordTimeTouch);
  return {
    x: x,
    y: y,
    timestamp: date,
    touchtype: touchType,
  };
}

var countDiv = document.getElementById("timer");
var secpass = function () {
  "use strict";

  var min = Math.floor(sec / 60); //remaining minutes
  var remSec = sec % 60; //remaining seconds

  if (remSec < 10) {
    remSec = "0" + remSec;
  }
  if (min < 10) {
    min = "0" + min;
  }
  countDiv.innerHTML = min + ":" + remSec;

  if (sec > 0 && isGift == false) {
    sec = sec - 1;
    if (sec <= totalSec * 0.25) {
      BGM.pause();
      fastBGM.play();
    }
  }
  if (sec == 0) {
    if (total == numOfTarget) {
      display_win();
    } else display_lose();
  }
};
var countDown = setInterval(function () {
  "use strict";

  //start countdown for 1 min
  if (begin == 1) {
    secpass();
  }
}, 1000);

var objTouchScore = 0;

var name;

function stop_game() {
  clearInterval(detection_loop);
  clearInterval(camerachecking);
  clearInterval(countDown);
  clearInterval(draw_loop);
  //Disables camera access
  handTrack.stopVideo(video);
  cameramedia.getTracks().forEach(function (track) {
    track.stop();
  });
}

function display_win() {
  isLoaded = false;
  if (game_end) return;
  game_end = true;
  var second_threshold = 40;
  var remainingSec = sec - accumulate_bonus_time;
  if (remainingSec > second_threshold) remainingSec = second_threshold;
  else if (remainingSec < 0) remainingSec = 0;

  score = total * 100 + (400 * remainingSec) / second_threshold;
  let jsonData = dlData();
  window.parent.wsCreateScore(score, jsonData);

  stop_game();
  //document.querySelector("#highscore").innerHTML = score;
  document.querySelector("#score").innerHTML = score;
  document.querySelector("#divGameCompleted > img").src = completeSrc;

  //Share button link
  let campaign_name = document.getElementById("cmname").innerHTML;
  document.getElementById("share").addEventListener("click", function () {
    let fburl =
      "https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Ffuyoh-ads.com%2Fcovid-19%3Fogtitle%3DHighscore%2Bin%2B" +
      campaign_name +
      "%26ogdescription%3DFuyohhhhhh%2521%2BI%2Bjust%2Bscored%2B" +
      score +
      "%2Bin%2B" +
      "Find%2Bthe%2BInvisible%2BDog" +
      "%2521%26ogimage%3Dhttps%253A%252F%252Ffuyoh-ads.com%252Fgame_web%252Fimage%252F06-findtheinvisibledog.jpg&amp;src=sdkpreparse";
    window.open(
      fburl,
      "_blank",
      "height=300,width=400,left=550,top=10,resizable=yes,scrollbars=yes"
    );
  });

  //End game transitions
  let brightness = 1.0;
  let rgba = 0;
  let dim = setInterval(() => {
    document.querySelector("#brocollicontainer").style.filter =
      "brightness(" + brightness + ")";
    document.querySelector("#brocollicontainer").style.backgroundColor =
      "rgba(0,0,0," + rgba + ")";
    brightness -= 0.1;
    rgba += 0.1;

    if (brightness <= 0.3 && rgba >= 0.7) clearInterval(dim);
  }, 10);

  document.querySelector("main").style.animation = "fadeout 1.5s";
  setTimeout(() => {
    document.querySelector("main").style.display = "none";
    document.querySelector(".container-fullScreen").style.display = "flex";
    var complete_bgm = document.getElementById("complete");
    complete_bgm.play();
    document.getElementById("divGameCompleted").style.display = "block";
    setTimeout(function () {
      complete_bgm.pause();
      scoreboard.play();
      startConfetti(); // start confetti for scoreboard
      document.getElementById("gameover").style.display = "flex";
    }, 3000);
  }, 1000);

  //dlData();
  fastBGM.pause();
  BGM.pause();

  /*

    document.getElementById("button_h").onclick = function () {
      let name = prompt("Enter name to store score in scoreboard:");
      if(!name){
       name="Player";
      }

      $.ajax({
      type:"POST",
        url:"pass.php",
        data:{name:name, score:score},
        success:(window.location.href="scoreboard.php"),
      });

      window.location.href = "scoreboard.php";
    };
    */
}

/**
 * Handle data collection aspect for this game
 * @example
 * // downloads a JSON file containing important game and user information
 * dlData();
 * @retruns {JSON} Returns a JSON file used for data collection
 */
function dlData() {
  var data = new Object();
  //Game setting
  data.gameSetting = {
    numOfDogs: numOfTarget,
    numOfCats: numOfTrap,
    numOfHint: numOfGift,
    totalSec: totalSec,
  };
  //=============================
  //Gameplay
  data.gamePlay = {
    spawnStatus: spawn,
    handLocation: (function () {
      for (let x = 0; x < handLocations.length; x++) {
        if (handLocations[x].includes(null)) {
          handLocations.splice(x, 1);
        }
      }
      return handLocations;
    })(),
    touchtime: recordTimeTouch,
    finalScore: {
      dogsCaptured: dogsCaptured,
      catsCaptured: catsCaptured,
      hintsCaptured: hintsCaptured,
      score: score,
      secondsLeft: sec,
    },
  };
  //=============================//
  //Performance
  data.avgfps = (function () {
    var sum = perfTime.reduce((sum, val) => (sum += val));
    return Math.round((sum / perfTime.length) * 1000) / 1000;
  })();
  data.performance = {
    starttime: stDate,
    loadingTimeTaken: loadingTimeEllapse,
    avgfps: data.avgfps,
    median: (function () {
      const sorted = perfTime.sort();
      const midElement = Math.ceil(perfTime.length / 2);
      const median =
        perfTime.length % 2 == 0
          ? (perfTime[midElement] + perfTime[midElement - 1]) / 2
          : perfTime[midElement - 1];
      return Math.round(median * 1000) / 1000;
    })(),
    stddev: (function () {
      var numerator = perfTime.reduce(
        (numerator, v) => (numerator += (v - data.avgfps) ** 2)
      );
      numerator /= perfTime.length;
      numerator = Math.sqrt(numerator);
      return Math.round(numerator * 1000) / 1000;
    })(),
  };
  delete data.avgfps;
  //=============================//
  //GUIs data
  data.GUIs = {
    handSize: {
      handImgWidth: handImg.width,
      handImgHeight: handImg.height,
    },
    playcanvassize: {
      playcanvaswidth: canvas.width,
      playcanvasheight: canvas.height,
    },
    windowsize: {
      windowwidth: window.innerWidth,
      windowheight: window.innerHeight,
    },
  };
  //=============================

  /*$.ajax({
      type: "POST",
      url: "gamedata.php",
      data: {
        startTime: data.starttime,
        performance: data.performance,
        handDetection: data.handDetection,
        loadingTimeTaken: data.loadingTimeTaken,
      },
      // on success do nothing
    });
    */

  // const text = JSON.stringify(data);
  // const name = "data.json";
  // const type = "text/plain";
  //
  // const a = document.createElement("a");
  // const file = new Blob([text], { type: type });
  // a.href = URL.createObjectURL(file);
  // a.download = name;
  // document.body.appendChild(a);
  // a.click();
  // a.remove();

  return data; // this can be passed to xhttp or ajax, contains all the information needed
}

function display_lose() {
  isLoaded = false;
  if (game_end) return;
  game_end = true;
  //Scoring
  score = total * 100;
  let jsonData = dlData();
  window.parent.wsCreateScore(score, jsonData);
  stop_game();

  document.querySelector("#score").innerHTML = score;
  //document.querySelector("#highscore").innerHTML = score;
  document.querySelector("#divGameCompleted > img").src = timesUpSrc;

  //Share button link
  let campaign_name = document.getElementById("cmname").innerHTML;
  document.getElementById("share").addEventListener("click", function () {
    let fburl =
      "https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Ffuyoh-ads.com%2Fcovid-19%3Fogtitle%3DHighscore%2Bin%2B" +
      campaign_name +
      "%26ogdescription%3DFuyohhhhhh%2521%2BI%2Bjust%2Bscored%2B" +
      score +
      "%2Bin%2B" +
      "Find%2Bthe%2BInvisible%2BDog" +
      "%2521%26ogimage%3Dhttps%253A%252F%252Ffuyoh-ads.com%252Fgame_web%252Fimage%252F06-findtheinvisibledog.jpg&amp;src=sdkpreparse";
    window.open(
      fburl,
      "_blank",
      "height=300,width=400,left=550,top=10,resizable=yes,scrollbars=yes"
    );
  });

  //End game transitions
  let brightness = 1.0;
  let rgba = 0;
  let dim = setInterval(() => {
    document.querySelector("#brocollicontainer").style.filter =
      "brightness(" + brightness + ")";
    document.querySelector("#brocollicontainer").style.backgroundColor =
      "rgba(0,0,0," + rgba + ")";
    brightness -= 0.05;
    rgba += 0.05;

    if (brightness <= 0.3 && rgba >= 0.7) clearInterval(dim);
  }, 10);

  document.querySelector("main").style.animation = "fadeout 1.5s";
  setTimeout(() => {
    document.querySelector("main").style.display = "none";
    document.querySelector(".container-fullScreen").style.display = "flex";
    var complete_bgm = document.getElementById("complete");
    complete_bgm.play();
    document.getElementById("divGameCompleted").style.display = "block";
    setTimeout(function () {
      complete_bgm.pause();
      scoreboard.play();
      startConfetti(); // start confetti for scoreboard
      document.getElementById("gameover").style.display = "flex";
    }, 3000);
  }, 1000);

  //dlData();
  fastBGM.pause();
  BGM.pause();

  /*

    document.getElementById("button_h").onclick = function () {
      let name = prompt("Enter name to store score in scoreboard:");
      if(!name){
       name="Player";
      }

      $.ajax({
      type:"POST",
        url:"pass.php",
        data:{name:name, score:score},
        success:(window.location.href="scoreboard.php"),
      });

      window.location.href = "scoreboard.php";
    };
    */
}

var controlX = cwidth / 2;
var controlY = cheight / 2;
var handImgPosX;
var handImgPosY;

function handAnimation() {
  let offset = (doganimate.width * 2) / 60;
  if (handX > controlX) {
    controlX += offset;
  } else if (handX + offset < controlX) {
    controlX -= offset;
  }

  if (handY > controlY) {
    controlY += offset;
  } else if (handY + offset < controlY) {
    controlY -= offset;
  }
}

function draw() {
  c.clearRect(0, 0, canvas.width, canvas.height);
  //requestAnimationFrame(draw);
  c.lineWidth = 2;
  spawn.forEach((item) => {
    if (item.type != "hint") {
      // enable to view spawn circle
      // c.beginPath();
      // c.arc(item.x, item.y, objRadius, 0, Math.PI * 2);
      // if (item.type == "dog") {
      //   c.strokeStyle = "blue";
      // } else if (item.type == "trap") {
      //   c.strokeStyle = "white";
      // }
      // c.stroke();
    } else {
      // c.beginPath();
      // c.arc(item.x, item.y, objRadius, 0, Math.PI * 2);
      // c.strokeStyle = "yellow";
      // c.stroke();

      if (
        item.isSpawn == false &&
        hintEnd == false &&
        hintStart == true &&
        sec >= totalSec * 0.1 &&
        sec <= totalSec * 0.9 &&
        isLoaded == true
      ) {
        hintEnd = true;
        //console.log("enter hint");

        setTimeout(function () {
          if (sec >= totalSec * 0.1) {
            hintImg.style.display = "block";
            hintimgcontainer.style.left = item.x - handImg.height / 2 + "px";
            hintimgcontainer.style.top = item.y - handImg.height / 2 + "px";
            hintimgcontainer.style.display = "block";
            hintSpawn = true;
            item.isSpawn = true;
            item.isDespawn = false;

            setTimeout(function () {
              item.isDespawn = true;
              if (item.isTouch == false) {
                hintImg.style.display = "none";
                hintimgcontainer.style.display = "none";
                hintEnd = false;
                hintSpawn = false;
              }
            }, 3000);
          }
        }, hintRandomTime(
          (hintAvailableSec / numOfGift / 3) * 1000,
          (hintAvailableSec / numOfGift) * 1000
        ));
      }
    }
  });

  handAnimation();
  handImgPosX = controlX - handimgcontainer.clientWidth / 2;
  handImgPosY = controlY - handimgcontainer.clientHeight / 2;

  c.drawImage(handImg, handImgPosX, handImgPosY, handImg.width, handImg.height);
}

var animateID;

function updateGUI(type, i) {
  if (type === "dog") {
    //basket doggy
    let basketDoggyContainer = document.querySelector("#basketdoggycontainer");
    let img = document.createElement("img");
    let dogSize;

    if (numOfTarget <= 6) dogSize = 0.5;
    else if (numOfTarget <= 8) dogSize = 0.4;
    else dogSize = 0.3;

    //dog appear
    let locX = spawn[i].x - (ccontainer.clientHeight * 0.2) / 2;
    let locY = spawn[i].y - (ccontainer.clientHeight * 0.2) / 2;

    let tX = locX;
    let tY =
      locY +
      document.querySelector("#infocontainer").clientHeight +
      document.querySelector(".container-item2").clientHeight;

    gctx.drawImage(
      targetImg,
      locX,
      locY,
      ccontainer.clientHeight * 0.2,
      ccontainer.clientHeight * 0.2
    );
    setTimeout(() => {
      gctx.clearRect(0, 0, ccontainer.clientWidth, ccontainer.clientHeight);
      animateDog(tX, tY);
      dctx.clearRect(0, 0, fullSize.clientWidth, fullSize.clientHeight);
      setTimeout(function () {
        img.className = "basketdoggy";
        img.src = "Assets/Props/img-09.png";
        img.style.height = basketDoggyContainer.clientHeight * dogSize + "px";
        img.style.width = basketDoggyContainer.clientHeight * dogSize + "px";
        basketDoggyContainer.appendChild(img);
      }, 1000);
    }, 1000);
  } else if (type === "trap") {
    //cat appear
    let locX = spawn[i].x - (ccontainer.clientHeight * 0.2) / 2;
    let locY = spawn[i].y - (ccontainer.clientHeight * 0.2) / 2;

    setTimeout(function () {
      gctx.drawImage(
        trapImg,
        locX,
        locY,
        ccontainer.clientHeight * 0.2,
        ccontainer.clientHeight * 0.2
      );
      isLoaded = false;
      document.querySelector("#canvascontainer").style.filter =
        "grayscale(100%)";
      document.querySelector("#canvascontainer").style.filter =
        "brightness(30%)";
      //document.querySelector("#blackscreen").style.display = "block";
      document.querySelector("#freezemessagecontainer").style.display = "flex";

      let freezeCounter = 5;
      document.querySelector("#freezetimer").innerHTML =
        freezeCounter + " seconds";
      let freezeTimer = setInterval(() => {
        freezeCounter--;
        document.querySelector("#freezetimer").innerHTML =
          freezeCounter + " seconds";
      }, 1000);

      setTimeout(function () {
        gctx.clearRect(0, 0, ccontainer.clientWidth, ccontainer.clientHeight);
        document.querySelector("#canvascontainer").style.filter =
          "grayscale(0)";
        document.querySelector("#canvascontainer").style.filter =
          "brightness(100%)";
        //document.querySelector("#blackscreen").style.display = "none";
        document.querySelector("#freezemessagecontainer").style.display =
          "none";
        clearInterval(freezeTimer);

        if (total != numOfTarget && sec != 0) isLoaded = true;

        spawn.forEach((index) => {
          if (index.type == "trap") {
            index.isTouch = false;
          }
        });
      }, 5000);
    }, 100);
  } else {
    hintImg.style.display = "none";
    openingHint.style.display = "flex";
    isGift = true;
    var hint = Math.round(Math.random() * 2);
    switch (hint) {
      case 0:
        hintMessage = "SHOW DOGS!";
        break;
      case 1:
        hintMessage = "SHOW CATS!";
        break;
      case 2:
        hintMessage = "ADDED 20SEC!";
        break;
    }
    setTimeout(function () {
      openingHint.style.display = "none";
      openedHint.style.display = "flex";
      document.querySelector("#hintmessage").innerHTML = hintMessage;
      document.querySelector("#hintmessage").style.display = "block";
      document.querySelector("#hintad").style.display = "block";

      setTimeout(function () {
        document.querySelector("#hintad").style.display = "none";
        document.querySelector("#hintmessage").style.display = "none";
        openedHint.style.display = "none";
        isLoaded = true;
        isGift = false;
        giftRandomizer(hint);
      }, 3000);
    }, 1000);
  }
}

function giftRandomizer(hint) {
  switch (hint) {
    case 0: //region of dogs
      let randomX;
      let randomY;
      for (let i = 0, len = spawn.length; i < len; i++) {
        if (spawn[i].isTouch === false && spawn[i].type == "dog") {
          randomX = spawn[i].x;
          randomY = spawn[i].y;
        }
      }

      gctx.beginPath();
      gctx.roundRect(
        randomX - 50,
        randomY - 50,
        ccontainer.clientHeight * 0.4,
        ccontainer.clientHeight * 0.4,
        20
      );
      gctx.fillStyle = "rgba(101, 196, 106, .7)";
      gctx.fill();
      setTimeout(() => {
        gctx.clearRect(0, 0, canvas.width, canvas.height);
      }, 15000);
      break;
    case 1: //region of cats
      gctx.beginPath();
      gctx.roundRect(
        spawn[trapIndex].x - 50,
        spawn[trapIndex].y - 50,
        ccontainer.clientHeight * 0.4,
        ccontainer.clientHeight * 0.4,
        20
      );
      gctx.fillStyle = "rgba(221, 36, 71, .7)";
      gctx.fill();
      setTimeout(() => {
        gctx.clearRect(0, 0, canvas.width, canvas.height);
      }, 15000);
      break;
    case 2: //extra time
      sec += 20;
      accumulate_bonus_time += 20;
      break;
  }
}

function animateDog(tX, tY) {
  var imgX = ccontainer.clientHeight * 0.2;
  var imgY = ccontainer.clientHeight * 0.2;
  dctx.clearRect(0, 0, fullSize.clientHeight, fullSize.clientWidth);
  dctx.drawImage(targetImg, tX, tY, imgX, imgY);

  var angle;

  if (tX >= doganimate.width * 0.4 && tX <= doganimate.width * 0.5) {
    angle = 90;
  } else {
    if (tX > doganimate.width / 2) {
      angle = 110;
    } else {
      angle = 70;
    }
  }
  //put the angle in radians
  var rads = (angle * Math.PI) / 180;
  var speed = doganimate.width * 2;
  //calculate the x and y components of the velocity in pixels per frame
  //speed is in pixels per second, so divide by 60 to get pixels per frame
  var vx = (Math.cos(rads) * speed) / 60;
  var vy = (Math.sin(rads) * speed) / 60;
  var animX = tX;
  var animY = tY;
  // requestAnimationFrame(function() {
  //     moveDog(animX, animY, vx, vy, imgX, imgY);
  // });

  let interval = setInterval(function () {
    if (animX >= doganimate.width * 0.42 && animX <= doganimate.width * 0.45) {
      vx = 0;
    }

    animX = animX + vx;
    animY = animY - vy;

    dctx.clearRect(0, 0, fullSize.clientWidth, fullSize.clientHeight);
    dctx.drawImage(targetImg, animX, animY, imgX, imgY);

    if (animY < 50) {
      clearInterval(interval);
      dctx.clearRect(0, 0, fullSize.clientWidth, fullSize.clientHeight);
    }
  }, 1000 / 60);
}

// function moveDog(animX, animY, vx, vy, imgX, imgY) {
//     if (animY < 50) {
//         dctx.clearRect(0, 0, fullSize.clientWidth, fullSize.clientHeight);
//         return;
//     }
//     if (animX >= doganimate.width * 0.42 && animX <= doganimate.width * 0.45) {
//         vx = 0;
//     }
//     animX = animX + vx;
//     animY = animY - vy;
//     dctx.clearRect(0, 0, fullSize.clientWidth, fullSize.clientHeight);
//     dctx.drawImage(targetImg, animX, animY, imgX, imgY);
//     requestAnimationFrame(function() {
//         moveDog(animX, animY, vx, vy, imgX, imgY);
//     });
// }
