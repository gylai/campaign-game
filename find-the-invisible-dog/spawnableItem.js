/** Class for spawnable item such as dogs and cat */
class spawnableItem {
  /**
   * Create an instance of spawnableItem
   * @param {string} type 
   * @param {number} containerWidth 
   * @param {number} containerHeight 
   * @param {number} radius 
   */
  constructor(type,containerWidth,containerHeight,radius)
  {
    this.type = type;
    this.isTouch = false; 
    this.x = Math.random() * ((containerWidth * 0.9) - (containerWidth * 0.1)) + (containerWidth * 0.1);
    this.y = Math.random() * ((containerHeight * 0.9) - (containerHeight * 0.1)) + (containerHeight * 0.1);
    this.radius = radius;
    this.distanceX = 0;
    this.distanceY = 0;
      if (type == 'hint') {
          this.isSpawn = false;  //if hint is spawned once, it wont spawn again
          this.isDespawn = true; //if hint is despawned then player cannot touch it
      }
  }
  /**
   * Regenerate X and Y coordinate for a single spawnableItem
   * @param {number} containerWidth 
   * @param {number} containerHeight 
   */
  regenerateXY(containerWidth, containerHeight)
  {
      this.x = Math.random() * ((containerWidth * 0.9) - (containerWidth * 0.1)) + (containerWidth * 0.1);
      this.y = Math.random() * ((containerHeight * 0.9) - (containerHeight * 0.1)) + (containerHeight * 0.1);
  } 

  /**
   * Calculate absolute distance of one spawnableItem with hand X and Y coordinate
   * @param {number} handX 
   * @param {number} handY 
   */
  calculateDistanceHandToObject(handX,handY)
  {
    this.distanceX = Math.abs(this.x - handX);
    this.distanceY = Math.abs(this.y - handY);
  }
}

/**
 * function to calculate the true distance apart from each object
 * using euclidean distance
 * @param {object} object1
 * @param {object} object2
 * @return {Number} Returns distance between 2 objects' coordinate
 */
function calcEuclDistance(object1,object2)
{
  return Math.sqrt(((object2.x - object1.x)**2) + ((object2.y - object1.y)**2));
}
