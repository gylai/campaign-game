<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="-1" />

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Find The Invisible Dog</title>
    <link rel="stylesheet" href="stylegame.css" />
    <link rel="stylesheet" href="animation.css" />
    <script src="https://cdn.jsdelivr.net/npm/handtrackjs@0.0.13/dist/handtrack.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="../confetti/confetti.js"></script>
    <link rel="stylesheet" href="../css/loading.css">
</head>

<body>
    <!------------- FROM INDEX.HTML ------------>
    <div class="loading">Loading...</div>

    <script>
        var user_resized = false; // Restrict the resize trigger function only trigger once
        window.onresize = () => {
            if (user_resized) return;
            user_resized = true;
            if (
                confirm(
                    "You resized your screen\nThe page will be reloaded to ensure your best user experience"
                )
            ) {
                location.reload(true);
                // window.location.href = window.location.href;
            } else {
                location.reload(true);
                // window.location.href = window.location.href;
            }
            //spawn = null;
        };
    </script>
    <div id="index">
        <div id="pregamecontainer">

            <div id="splashscreen">
                <video autoplay muted loop>
                    <source type="video/mp4" />
                    Your browser does not support the video tag
                </video>
            </div>
            <button id="proceed">
                start game
            </button>
            <button id="exit" onclick="parent.location.href='https://fuyoh-ads.com/covid-19/dashboard/';">
                exit
            </button>
            <video id="tutorial">
                <source type="video/mp4">
                Your browser does not support the video tag.
            </video>

        </div>
    </div>

    <!-- Miscellaneous -->

    <audio id="complete" src="../audio/complete.mp3"></audio>


    <div id="blackscreen">
        <p></p>
    </div>


    <!-- Bushes/Brocolli in the background -->
    <div id="brocollicontainer">
        <img id="brocolli1" class="brocolli" />
        <img id="brocolli2" class="brocolli" />
        <img id="brocolli3" class="brocolli-1" />
        <img id="brocolli4" class="brocolli" />
        <img id="brocolli5" class="brocolli" />
        <img id="brocolli6" class="brocolli-1" />
        <img id="brocolli7" class="brocolli-1" />
    </div>

    <div id="hardware-container">
        <div id="camera-instruction">
            <img class="camera-range" src="Assets/Hardware/camera_range.png">
            <br><br>
            <p>Recommended to play this game with a range of 1.5m from your webcam</p>
        </div>
        <div id="num-person-instruction">
            <img class="num-person" src="Assets/Hardware/num_of_person.png">
            <br><br>
            <p>Please ensure ONLY ONE PLAYER with SUFFICIENT ROOM LIGHTING for the camera detection</p>
        </div>
        <div id="btn-container">
            <button id="btnSkipInstruction">Continue</button>
        </div>
    </div>

    <!------------------------------------------>
    <!-- Loading Screen -->
    <div id="loadingscreen">
        <img />
    </div>

    <!-- Game over screen-->
    <div class="container-fullScreen divGameCompleted" id="divGameCompleted">
        <img id="img-scoreboard-bg" class="img-scoreboard" />
    </div>

    <!-- Scoreboard -->
    <div class="container-fullScreen" id="gameover">
        <img id="gameoverui" />
        <div id="gameovercontent">
            <p id="score-txt">score</p>
            <span id="score">0</span>

            <button id="share" data-layout="button" data-size="small">
                <a target="_blank" class="fb-xfbml-parse-ignore" id="fbshare-link" style="color:#FFF; text-decoration: none;">
                    share
                </a>
            </button>

            <button id="continue" onclick="parent.location.href='https://fuyoh-ads.com/covid-19/dashboard/';">
                continue
            </button>
        </div>
    </div>

    <main>
        <!-- Loading bar on the loading screen -->
        <img id="loadingbar" src="img/loadingbar21.gif" />

        <!-- Game containers -->
        <div id="gui-container">
            <div id="count" class="container-item1">
                <p id="timer">00:00</p>
                <img id="timergui" />
            </div>

            <div class="container-item2">
                <div id="basketdoggycontainer"></div>
                <div id="emptydogcontainer"></div>
                <img id="basketgui" />
            </div>

            <div class="container-item3">
                <div id="vcanvascontainer">
                    <canvas id="vcanvas">
                        <video id="video"></video>
                    </canvas> <!-- video canvas -->
                </div>
                <img id="vcanvasgui" />
            </div>
            <canvas id="doganimate"></canvas> <!-- black canvas -->
            <div class="container-item4">
                <div id="fence"><img id="fencegui" /></div>
                <div id="infocontainer">
                    <img id="infoboard" />
                    <div id="adscontainer">
                        <img id="ads" class="ads" />
                    </div>
                </div>
                <div id="canvascontainer">
                    <canvas id="guicanvas"></canvas> <!-- green canvas-->
                    <canvas id="canvas"></canvas>
                    <div id="handimgcontainer"><img id="handimg"></div>
                    <div id="hintimgcontainer"><img id="hintimg" /><img id="openinghint" /></div>
                    <div id="targetimgcontainer"><img id="targetimg" /></div>
                    <div id="trapimgcontainer"><img id="trapimg" /></div>
                    <img id="canvasbg" />
                </div>
            </div>
        </div>

        <!-- Used for display messages (Freezing and hints) -->
        <div id="messagecontainer">
            <img id="openedhint" />
            <img id="hintad" />
            <p id="hintmessage"></p>
            <div id="freezemessagecontainer">
                <img id="freezemessage">
                <p id="freezetimer"></p>
            </div>
        </div>
    </main>

    <p id="cmname" style="display:none;"></p>
    <script src="loadItem.js"></script>
    <script src="loadCanvas.js"></script>
    <script src="state.js"></script>
    <script src="spawnableItem.js"></script>
    <script src="loadHandtrack.js?version=<?= time() ?>"></script>
    <script src='https://kit.fontawesome.com/a076d05399.js'></script>

</body>

</html>


<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v11.0" nonce="N8VteFjv">
</script>
<script>
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    var url = new URL(window.location.href);
    var campaign_name = url.searchParams.get("campaign_name");
    if (campaign_name) {
        document.getElementById("cmname").innerHTML = campaign_name;
    } else {
        document.getElementById("cmname").innerHTML = "Find%2Bthe%2BInvisible%2BDog";
    }
</script>