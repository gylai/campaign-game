//==================================================//
var Engine = Matter.Engine,
    Render = Matter.Render,
    Runner = Matter.Runner,
    Bodies = Matter.Bodies,
    Body = Matter.Body,
    Events = Matter.Events,
    Composite = Matter.Composite;

var engine = Engine.create();
var render = Render.create({
    canvas: document.getElementById("canvas"),
    engine: engine,
    options: {
        width: canvas.width,
        height: canvas.height,
        wireframes: false,
        background: "./Assets/Assets/bg-04.png",
    },
});
// render.canvas.style.objectFit = "fill";
// render.canvas.style.background = "no-repeat"s

engine.world.gravity.y = 0;
Render.run(render);
var runner = Runner.create();
Runner.run(runner, engine);
render.canvas.style.backgroundSize = `100% 100%`;
// create paddle
// default value for !4k
var paddleX = (screen.width > 3839) ? 2.1 : 0.7;
var paddleY = (screen.height > 2159) ? 1.5 : 0.5;
var normalImgxScale = (screen.width > 3839) ? 0.3 : 0.1;
var normalImgyScale = (screen.height > 2159) ? 0.3 : 0.1;
var shortLongImgxScale = (screen.width > 3839) ? 0.45 : 0.15;
var shortLongImgyScale = (screen.height > 2159) ? 0.3 : 0.1;
var paddleBodyScaleX = (screen.width > 3839) ? 510 : 170;
var paddleBodyScaleY = (screen.height > 2159) ? 30 : 10;
var objRadius = (screen.width > 3839) ? 60 : 20;
var paddleMultiplier = 0.3;

var ground = Bodies.rectangle(
    document.querySelector("body").clientWidth / 2,
    document.querySelector("body").clientHeight * 0.9,
    paddleBodyScaleX,
    paddleBodyScaleY, {
        collisionFilter: {
            mask: 0x0001,
        },
        isSensor: true,
        isStatic: true,
        render: {
            sprite: {
                xScale: paddleX, // 0.7
                yScale: paddleY, // 0.5
                texture: "./Assets/Assets/paddleShort.png",
            },
        },
    }
);
Composite.add(engine.world, [ground]);
var circles = [];
//==================================================
var ranObj;
var ranval;
//declare image
// img 1 - img 5 & boom -> size: 1000x1000 px
// long & short -> size: 1000x300 px
var img1 = new Image();
img1.src = img1php;
var img2 = new Image();
img2.src = img2php;
var img3 = new Image();
img3.src = img3php;
var img4 = new Image();
img4.src = img4php;
var img5 = new Image();
img5.src = img5php;

var long = new Image();
long.src = "./img/long_new.png";
var short = new Image();
short.src = "./img/short_new.png";
var boom = new Image();
boom.src = "./img/boom_new.png";
//declare audio
var scoreboard = new Audio();
scoreboard.src = "../audio/scoreboard(edited).mp3";

//========= UNCOMMENT FOR FETCHING IMAGE FROM SERVER =======//
// var xhttp = new XMLHttpRequest();
// xhttp.onreadystatechange = function () {
//   if (this.readyState == 4 && this.status == 200) {
//     console.log(this.responseText);

//     let json = JSON.parse(this.responseText);

//     if (json) {
//       let settings = json.data;

//       console.log(settings);

//       for (var i = 0; i < settings.length; i++) {
//         let data = settings[i];

//         if (data.type == "fixed_image") {
//           let images = data.data.images;
//           img1.src = images[0];
//           img2.src = images[1];
//           img3.src = images[2];
//           img4.src = images[3];
//           img5.src = images[4];
//         }
//       }
//     }
//   }
// };
// xhttp.open(
//   "GET",
//   "https://fuyoh-ads.com/game_web/campaign_game/settings?activity_id=1",
//   true
// );
// xhttp.send();
//=====================================================//

var img = [];
var num = 5;
var obj = [];
var i = 0;
var counterPaddle = 1;
var timer2sec = false;

//==================================================
//Class to hold object spawning
class _Object {
    constructor(x, y, img, id, touchtype) {
        this.x = x;
        this.y = y;
        this.img = img;
        this.id = id;
        this.touchtype = touchtype;

        if (this.img == long || this.img == short) {
            this.xscale = shortLongImgxScale;
            this.yscale = shortLongImgyScale;
        } else {
            this.xscale = normalImgxScale;
            this.yscale = normalImgyScale;
            // image 1000x1000 0.1
            // image 1000x1000 0.1
        }
        if (this.touchtype == "long" || this.touchtype == "short") {
            this.circle = Bodies.rectangle(this.x, this.y, this.img.width * shortLongImgxScale, this.img.width * shortLongImgyScale, {
                frictionAir: 0,
                cloosionFilter: {
                    mask: 0x0001,
                },
                render: {
                    sprite: {
                        xScale: this.xscale,
                        yScale: this.yscale,
                        texture: img.currentSrc,
                    },
                },
                label: this.touchtype,
            });
            Body.setVelocity(this.circle, {
                x: 0,
                y: 0.02,
            });
        } else {
            this.circle = Bodies.circle(this.x, this.y, objRadius, {
                // frictionAir: Math.random() * 0.1,
                frictionAir: 0,
                // frictionAir: Math.random() * (0.1 - 0.01) + 0.01,
                // timeScale: Math.random() * (1-0.5) + 0.5,
                collisionFilter: {
                    mask: 0x0001,
                },
                render: {
                    sprite: {
                        xScale: this.xscale,
                        yScale: this.yscale,
                        texture: img.currentSrc,
                    },
                },
                label: this.touchtype,
            });
            Body.setVelocity(this.circle, {
                x: 0,
                y: 0.02,
            });
        }
    }


    //Draw image at set parameters
    draw() {

        Composite.add(engine.world, this.circle);
        Body.applyForce(
            this.circle, {
                x: this.circle.position.x,
                y: this.circle.position.y,
            }, { x: 0, y: 0.02 }
        );
    }

    //Set the detection range of the paddle
    /*
      1. detect() no longer being used as causing multiple trigger on Events.on method.
      2. Detection is located outside of classes since MJS detection does not depends on object instances,
      but it depends on the Pairs object created by MJS itself.
     */

    isOffScreen() {
        return this.circle.body.position > window.innerHeight + 100;
    }
}

//state detection
var startGen = setInterval(() => {
    if (isDrop && begin == 1 && !timer2sec) {
        gen();
    }
}, 500);
Engine.update(engine);

//change background color when the timer left 15 seconds
var hurry = setInterval(() => {
    //if (mm == 0 && ss == 15) {
    if (currentsec <= 20) {
        at20sec = Math.floor(Date.now() / 1000);
        _bgm.pause();
        _hurry.volume = 0.3;
        _hurry.play();
        document.querySelector("#canvas").style.animation = "hurryborder normal 1s infinite linear";
        document.getElementById("timer").style.color = "#bc2986";
        clearInterval(hurry);
    }
}, 100);

function gen() {
    clearInterval(startGen);
    //Generating spawns
    //control the spawn time gap using spawnDelay
    let isGenerate = setInterval(() => {
        //Check if there are still remaining imgs inside noImgs[]
        let isZero = 0;
        for (let x = 0; x < noImgs.length; x++) {
            isZero += noImgs[x];
        }

        if (isEnd || isZero == 0) {
            clearInterval(isGenerate);
        } else {
            checkQuantity();
            //ranval = random value
            if (ranval == 0) {
                img[i] = img1;
                var id = "img1";
                noImgs[ranval]--;
                // console.log("img1 : " + noImgs[ranval]);
            } else if (ranval == 1) {
                img[i] = img2;
                var id = "img2";
                noImgs[ranval]--;
                // console.log("img2 : " + noImgs[ranval]);
            } else if (ranval == 2) {
                img[i] = img3;
                var id = "img3";
                noImgs[ranval]--;
                // console.log("img3 : " + noImgs[ranval]);
            } else if (ranval == 3) {
                img[i] = img4;
                var id = "img4";
                noImgs[ranval]--;
                // console.log("img4 : " + noImgs[ranval]);
            } else if (ranval == 4) {
                img[i] = img5;
                var id = "img5";
                noImgs[ranval]--;
                // console.log("img5 : " + noImgs[ranval]);
            } else if (ranval == 5) {
                img[i] = long;
                var id = "long";
                noImgs[ranval]--;
                // console.log("long : " + noImgs[ranval]);
            } else if (ranval == 6) {
                img[i] = short;
                var id = "short";
                noImgs[ranval]--;
                // console.log("short : " + noImgs[ranval]);
            } else if (ranval == 7) {
                img[i] = boom;
                var id = "boom";
                noImgs[ranval]--;
                // console.log("boom : " + noImgs[ranval]);
            }

            var x = Math.random() * (canvas.width - 300) + 150;
            var y = Math.random() * 40;
            obj[i] = new _Object(x, y, img[i], spawn.length + 1, id);

            //Data collection
            spawn.push(Object.assign({}, { x: obj[i].x, y: obj[i].y, img: obj[i].img, id: obj[i].id, touchtype: obj[i].touchtype }));
            spawn[spawn.length - 1]["spawn-timestamp"] = Math.floor(
                Date.now() / 1000
            );
            spawn[spawn.length - 1]["touched-timestamp"] = "";
            spawn[spawn.length - 1].touch = false;
            delete spawn[spawn.length - 1].img;
            delete spawn[spawn.length - 1].y;
            i++;
        }
        //counter to make the array looping for looping in update()
        if (i == num) {
            i = 0;
        }
        if (isDrop) {
            for (let j = 0; j < num; j++) {
                if (obj[j] && obj[j].draw !== undefined) {
                    // console.log(obj[j]);
                    obj[j].draw();
                    // console.log('stuck');
                }
            }
        }
    }, spawnDelay);
}
//==================================================
// Object Detection
//==================================================
Events.on(engine, "collisionStart", function(event) {
    var pairs = event.pairs;
    // console.log("enter detect event");
    for (var idx = 0; idx < pairs.length; idx++) {
        var pair = pairs[idx];
        if (pair.bodyA == ground) {
            if (
                pair.bodyB.label == "img1" ||
                pair.bodyB.label == "img2" ||
                pair.bodyB.label == "img3" ||
                pair.bodyB.label == "img4" ||
                pair.bodyB.label == "img5"
            ) {
                document.getElementById("status").style.top =
                    canvas.height - 140 + "px";
                document.getElementById("status").style.left = ground.position.x + "px";
                document.getElementById("status").style.color = "lightgreen";
                document.getElementById("sparkling").style.display = "block";
                document.getElementById("sparkling").style.top =
                    canvas.height - canvas.height * 0.15 + "px";
                document.getElementById("sparkling").style.left =
                    ground.position.x + "px";

                setTimeout(() => {
                    document.getElementById("sparkling").style.display = "none";
                }, 500);
            }
            if (pair.bodyB.label == "img1") {
                img1count++;
                clearTimeout(timeOutDelay);
                timeOutDelay = setTimeout(() => {
                    document.getElementById("status").innerHTML =
                        "+" + Math.round(noImgsScore[0]);
                }, 50);
                clearTimeout(timeOutStatus);
                timeOutStatus = setTimeout(() => {
                    document.getElementById("status").innerHTML = "";
                }, 500);
                point = point >= 1000 ? 1000 : point += noImgsScore[0];
            }
            if (pair.bodyB.label == "img2") {
                img2count++;
                clearTimeout(timeOutDelay);
                timeOutDelay = setTimeout(() => {
                    document.getElementById("status").innerHTML =
                        "+" + Math.round(noImgsScore[1]);
                }, 50);
                clearTimeout(timeOutStatus);
                timeOutStatus = setTimeout(() => {
                    document.getElementById("status").innerHTML = "";
                }, 500);
                point = point >= 1000 ? 1000 : point += noImgsScore[1];
            }
            if (pair.bodyB.label == "img3") {
                img3count++;
                clearTimeout(timeOutDelay);
                timeOutDelay = setTimeout(() => {
                    document.getElementById("status").innerHTML =
                        "+" + Math.round(noImgsScore[2]);
                }, 50);
                clearTimeout(timeOutStatus);
                timeOutStatus = setTimeout(() => {
                    document.getElementById("status").innerHTML = "";
                }, 500);
                point = point >= 1000 ? 1000 : point += noImgsScore[2];
            }
            if (pair.bodyB.label == "img4") {
                img4count++;
                clearTimeout(timeOutDelay);
                timeOutDelay = setTimeout(() => {
                    document.getElementById("status").innerHTML =
                        "+" + Math.round(noImgsScore[3]);
                }, 50);
                clearTimeout(timeOutStatus);
                timeOutStatus = setTimeout(() => {
                    document.getElementById("status").innerHTML = "";
                }, 500);
                point = point >= 1000 ? 1000 : point += noImgsScore[3];
            }
            if (pair.bodyB.label == "img5") {
                img5count++;
                clearTimeout(timeOutDelay);
                timeOutDelay = setTimeout(() => {
                    document.getElementById("status").innerHTML =
                        "+" + Math.round(noImgsScore[4]);
                }, 50);
                clearTimeout(timeOutStatus);
                timeOutStatus = setTimeout(() => {
                    document.getElementById("status").innerHTML = "";
                }, 500);
                point = point >= 1000 ? 1000 : point += noImgsScore[4];
            }
            if (pair.bodyB.label == "boom") {
                boomCount++;
                point -= boomPoint;
                if (point < 0) point = 0;

                document.getElementById("status").style.color = "crimson";
                document.getElementById("status").style.top = canvas.height - 140 + "px";
                document.getElementById("status").style.left = ground.position.x + "px";

                document.getElementById("boom").style.display = "block";
                document.getElementById("boom").style.top =
                    canvas.height - canvas.height * 0.15 + "px";
                document.getElementById("boom").style.left = ground.position.x + "px";

                setTimeout(() => {
                    document.getElementById("boom").style.display = "none";
                }, 500);

                document.getElementById("status").style.color = "crimson";
                clearTimeout(timeOutDelay);
                timeOutDelay = setTimeout(() => {
                    document.getElementById("status").innerHTML =
                        "-" + Math.round(boomPoint);
                }, 50);

                clearTimeout(timeOutStatus);
                timeOutStatus = setTimeout(() => {
                    document.getElementById("status").innerHTML = "";
                }, 500);
            }
            if (pair.bodyB.label == "long") {
                longCount++;
                if (counterPaddle >= 0 && counterPaddle < 3) {
                    counterPaddle++;
                    Body.scale(ground, 1.5 * counterPaddle, 1);
                    ground.render.sprite.xScale = paddleX + (paddleMultiplier * counterPaddle);
                }
                if (counterPaddle < 0) {
                    counterPaddle++;
                    console.log(counterPaddle);
                    Body.scale(ground, 1.5, 1);
                    ground.render.sprite.xScale = paddleX + (paddleMultiplier * counterPaddle);
                }

                document.getElementById("status").style.top = canvas.height - 140 + "px";
                document.getElementById("status").style.left = ground.position.x + "px";
                document.getElementById("status").style.color = "cyan";
                clearTimeout(timeOutDelay);
                timeOutDelay = setTimeout(() => {
                    document.getElementById("status").innerHTML = "Length Increased";
                }, 50);

                clearTimeout(timeOutStatus);
                timeOutStatus = setTimeout(() => {
                    document.getElementById("status").innerHTML = "";
                }, 500);
            }
            if (pair.bodyB.label == "short") {
                shortCount++;
                console.log(ground.position.x);
                if (counterPaddle > 0) {
                    Body.scale(ground, 1 / (1.5 * counterPaddle), 1); // 1
                    counterPaddle--; // 1
                    ground.render.sprite.xScale = paddleX + (paddleMultiplier * counterPaddle); // 0
                }
                if (counterPaddle == 0) {
                    counterPaddle--; // 1
                    Body.scale(ground, 1 / 1.5, 1); // 1
                    ground.render.sprite.xScale = paddleX + (paddleMultiplier * counterPaddle); // 0
                }
                console.log(ground.position.x);

                document.getElementById("status").style.color = "violet";
                document.getElementById("status").style.top = canvas.height - 140 + "px";
                document.getElementById("status").style.left = ground.position.x + "px";
                clearTimeout(timeOutDelay);
                timeOutDelay = setTimeout(() => {
                    document.getElementById("status").innerHTML = "Length Decreased";
                }, 50);

                clearTimeout(timeOutStatus);
                timeOutStatus = setTimeout(() => {
                    document.getElementById("status").innerHTML = "";
                }, 500);
            }
            // console.log("removed ", pair.bodyB.label);
            pair.bodyB.collisionFilter.mask = 0x0008;
            Composite.remove(engine.world, pair.bodyB);
            pair.bodyB.render.visible = false;
            // Engine.update(engine);
        }
        if (pair.bodyB == ground) {
            if (
                pair.bodyA.label == "img1" ||
                pair.bodyA.label == "img2" ||
                pair.bodyA.label == "img3" ||
                pair.bodyA.label == "img4" ||
                pair.bodyA.label == "img5"
            ) {
                document.getElementById("status").style.top =
                    canvas.height - 140 + "px";
                document.getElementById("status").style.left = ground.position.x + "px";
                document.getElementById("status").style.color = "lightgreen";
                document.getElementById("sparkling").style.display = "block";
                document.getElementById("sparkling").style.top =
                    canvas.height - canvas.height * 0.15 + "px";
                document.getElementById("sparkling").style.left =
                    ground.position.x + "px";

                setTimeout(() => {
                    document.getElementById("sparkling").style.display = "none";
                }, 500);
            }
            if (pair.bodyA.label == "img1") {
                img1count++;
                clearTimeout(timeOutDelay);
                timeOutDelay = setTimeout(() => {
                    document.getElementById("status").innerHTML =
                        "+" + Math.round(noImgsScore[0]);
                }, 50);
                clearTimeout(timeOutStatus);
                timeOutStatus = setTimeout(() => {
                    document.getElementById("status").innerHTML = "";
                }, 500);
                point = point >= 1000 ? 1000 : point += noImgsScore[0];
            }
            if (pair.bodyA.label == "img2") {
                img2count++;
                clearTimeout(timeOutDelay);
                timeOutDelay = setTimeout(() => {
                    document.getElementById("status").innerHTML =
                        "+" + Math.round(noImgsScore[1]);
                }, 50);
                clearTimeout(timeOutStatus);
                timeOutStatus = setTimeout(() => {
                    document.getElementById("status").innerHTML = "";
                }, 500);
                point = point >= 1000 ? 1000 : point += noImgsScore[1];
            }
            if (pair.bodyA.label == "img3") {
                img3count++;
                clearTimeout(timeOutDelay);
                timeOutDelay = setTimeout(() => {
                    document.getElementById("status").innerHTML =
                        "+" + Math.round(noImgsScore[2]);
                }, 50);
                clearTimeout(timeOutStatus);
                timeOutStatus = setTimeout(() => {
                    document.getElementById("status").innerHTML = "";
                }, 500);
                point = point >= 1000 ? 1000 : point += noImgsScore[2];
            }
            if (pair.bodyA.label == "img4") {
                img4count++;
                clearTimeout(timeOutDelay);
                timeOutDelay = setTimeout(() => {
                    document.getElementById("status").innerHTML =
                        "+" + Math.round(noImgsScore[3]);
                }, 50);
                clearTimeout(timeOutStatus);
                timeOutStatus = setTimeout(() => {
                    document.getElementById("status").innerHTML = "";
                }, 500);
                point = point >= 1000 ? 1000 : point += noImgsScore[3];
            }
            if (pair.bodyA.label == "img5") {
                img5count++;
                clearTimeout(timeOutDelay);
                timeOutDelay = setTimeout(() => {
                    document.getElementById("status").innerHTML =
                        "+" + Math.round(noImgsScore[4]);
                }, 50);
                clearTimeout(timeOutStatus);
                timeOutStatus = setTimeout(() => {
                    document.getElementById("status").innerHTML = "";
                }, 500);
                point = point >= 1000 ? 1000 : point += noImgsScore[4];
            }
            if (pair.bodyA.label == "boom") {
                boomCount++;
                point -= boomPoint;
                if (point < 0) point = 0;

                document.getElementById("status").style.color = "crimson";
                document.getElementById("status").style.top = canvas.height - 140 + "px";
                document.getElementById("status").style.left = ground.position.x + "px";

                document.getElementById("boom").style.display = "block";
                document.getElementById("boom").style.top =
                    canvas.height - canvas.height * 0.15 + "px";
                document.getElementById("boom").style.left = ground.position.x + "px";

                setTimeout(() => {
                    document.getElementById("boom").style.display = "none";
                }, 500);

                document.getElementById("status").style.color = "crimson";
                clearTimeout(timeOutDelay);
                timeOutDelay = setTimeout(() => {
                    document.getElementById("status").innerHTML =
                        "-" + Math.round(boomPoint);
                }, 50);

                clearTimeout(timeOutStatus);
                timeOutStatus = setTimeout(() => {
                    document.getElementById("status").innerHTML = "";
                }, 500);
            }
            if (pair.bodyA.label == "long") {
                longCount++;
                if (counterPaddle >= 0 && counterPaddle < 3) {
                    counterPaddle++;
                    Body.scale(ground, 1.5 * counterPaddle, 1);
                    ground.render.sprite.xScale = paddleX + (paddleMultiplier * counterPaddle);
                }
                if (counterPaddle < 0) {
                    counterPaddle++;
                    console.log(counterPaddle);
                    Body.scale(ground, 1.5, 1);
                    ground.render.sprite.xScale = paddleX + (paddleMultiplier * counterPaddle);
                }

                document.getElementById("status").style.top = canvas.height - 140 + "px";
                document.getElementById("status").style.left = ground.position.x + "px";
                document.getElementById("status").style.color = "cyan";
                clearTimeout(timeOutDelay);
                timeOutDelay = setTimeout(() => {
                    document.getElementById("status").innerHTML = "Length Increased";
                }, 50);

                clearTimeout(timeOutStatus);
                timeOutStatus = setTimeout(() => {
                    document.getElementById("status").innerHTML = "";
                }, 500);
            }
            if (pair.bodyA.label == "short") {
                shortCount++;
                console.log(ground.position.x);
                if (counterPaddle >= 0) {
                    Body.scale(ground, 1 / (1.5 * counterPaddle), 1);
                    counterPaddle--;
                    ground.render.sprite.xScale = paddleX + (paddleMultiplier * counterPaddle);
                }
                if (counterPaddle == 0) {
                    counterPaddle--; // 1
                    Body.scale(ground, 1 / 1.5, 1); // 1
                    ground.render.sprite.xScale = paddleX + (paddleMultiplier * counterPaddle); // 0
                }
                console.log(ground.position.x);

                document.getElementById("status").style.color = "violet";
                document.getElementById("status").style.top = canvas.height - 140 + "px";
                document.getElementById("status").style.left = ground.position.x + "px";
                clearTimeout(timeOutDelay);
                timeOutDelay = setTimeout(() => {
                    document.getElementById("status").innerHTML = "Length Decreased";
                }, 50);

                clearTimeout(timeOutStatus);
                timeOutStatus = setTimeout(() => {
                    document.getElementById("status").innerHTML = "";
                }, 500);
            }
            // console.log("removed ", pair.bodyA.label);
            pair.bodyA.collisionFilter.mask = 0x0009;
            Composite.remove(engine.world, pair.bodyA);
            pair.bodyA.render.visible = false;
            // Engine.update(engine);
        }
    }

    document.getElementById("points").innerHTML = "Scores : " + Math.round(point);
});
//==================================================
function checkQuantity() {
    ranval = Math.floor(Math.random() * 8);

    let isImg = false;
    while (isImg === false) {
        // console.log("stuck while loop");
        if (noImgs[ranval] == 0) {
            ranval = Math.floor(Math.random() * 8);
        } else isImg = true;
    }
}

function forceMove(body, endX, endY, pct) {
    let dx = endX - body.position.x;
    let dy = endY - body.position.y;

    let x = body.position.x + (dx * pct) / 100;
    let y = body.position.y + (dy * pct) / 100;

    Body.setPosition(body, {
        x: x,
        y: y,
    });
}

function updatePaddle(targetX) {
    let pct = 0;
    Events.on(engine, "beforeUpdate", function(event) {
        if (pct < 101) {
            pct = pct + 2;
            forceMove(ground, targetX, canvas.height * 0.9, pct);
        }
    });
}

//=========================================================
function dlData() {
    var data = {};
    //=============================
    //Game setting
    data.gameSetting = {
        totalSec: totalsec,
        totalImg: spawnTime,
        maximumPoint: setMaxPoint,
    };
    //=============================
    //Gameplay
    data.gameplay = {
        handLocation: handLocations, //handLocations
        spawnDetail: {
            spawningTimeGap: spawnDelay,
            spawnStatus: spawn,
            quantity: {
                noimg1: {
                    quantity: totalImg[0],
                    touched: img1count,
                    perk: "+" + noImgsScore[0] + "point",
                },
                noimg2: {
                    quantity: totalImg[1],
                    touched: img2count,
                    perk: "+" + noImgsScore[1] + "point",
                },
                noimg3: {
                    quantity: totalImg[2],
                    touched: img3count,
                    perk: "+" + noImgsScore[2] + "point",
                },
                noimg4: {
                    quantity: totalImg[3],
                    touched: img4count,
                    perk: "+" + noImgsScore[3] + "point",
                },
                noimg5: {
                    quantity: totalImg[4],
                    touched: img5count,
                    perk: "+" + noImgsScore[4] + "point",
                },
                longimg: {
                    quantity: totalImg[5],
                    touched: longCount,
                    perk: "increase length",
                },
                shortimg: {
                    quantity: totalImg[6],
                    touched: shortCount,
                    perk: "decrease length",
                },
                boomimg: {
                    quantity: totalImg[7],
                    touched: boomCount,
                    perk: "-" + boomPoint + "point",
                },
            },
        },
        timestamp: {
            pageLoadTime: pageloadtime,
            gameStartTime: starttime,
            at20Sec: at20sec,
            endTime: endtime,
        },
        score: point,
    };
    //=============================
    //Performance
    data.averagefps = (function() {
        var sum = perfTime.reduce((sum, val) => (sum += val));
        return Math.round((sum / perfTime.length) * 1000) / 1000;
    })();
    data.performance = {
        loadingTimeTaken: loadingTimeEllapse,
        median: (function() {
            const sorted = perfTime.sort();
            const midElement = Math.ceil(perfTime.length / 2);
            const med =
                perfTime.length % 2 == 0 ?
                (perfTime[midElement] + perfTime[midElement - 1]) / 2 :
                perfTime[midElement - 1];
            return Math.round(med * 1000) / 1000;
        })(),
        avgfps: data.averagefps,
        stddev: (function() {
            var numerator = perfTime.reduce(
                (numerator, v) => (numerator += (v - data.averagefps) ** 2)
            );
            numerator /= perfTime.length;
            numerator = Math.sqrt(numerator);
            return Math.round(numerator * 1000) / 1000;
        })(),
    };
    delete data.averagefps;
    //=============================
    //GUIs data
    data.GUIs = {
        playcanvassize: {
            playcanvaswidth: canvas.width,
            playcanvasheight: canvas.height,
        },
        windowsize: {
            windowwidth: window.innerWidth,
            windowheight: window.innerHeight,
        },
    };
    //=============================
    //=============================

    // $.ajax({
    //     type: "POST",
    //     url: "gamedata.php",
    //     data: {
    //         startTime: data.starttime,
    //         performance: data.performance,
    //         handDetection: data.handDetection,
    //         loadingTimeTaken: data.loadingTimeTaken,
    //     },
    //     // on success do nothing
    // });

    // const text = JSON.stringify(data);
    // const name = "data.json";
    // const type = "text/plain";

    // const a = document.createElement("a");
    // const file = new Blob([text], { type: type });
    // a.href = URL.createObjectURL(file);
    // a.download = name;
    // document.body.appendChild(a);
    // a.click();
    // a.remove();
    averagefps = null;
    return data;
}

function calcTouchTime(x, y, touchType, id) {
    //For data collection
    let date = Math.floor(Date.now() / 1000);
    //console.log(recordTimeTouch);

    for (let i = 0; i < spawn.length; i++) {
        if (spawn[i].id == id) {
            spawn[i]["touched-timestamp"] = date;
            spawn[i]["touch"] = true;
        }
    }

    return {
        x: x,
        y: y,
        "unix-timestamp": date,
        touchtype: touchType,
    };
}

// use to check paddle length while camera is off
// function debugPaddle(condition, counter)
// {
//   if (condition == "s"){
//     if (counter > 0) {
//       Body.scale(ground, 1 / (1.5 * counter), 1);
//       counter--;
//       ground.render.sprite.xScale = 0.7 + 0.5 * counter;
//     }
//   }

//   if (condition == "i")
//   {
//     if (counter >= 0 && counter <=2) {
//       Body.scale(ground, 1.5 * counter, 1);
//       counter++;
//       ground.render.sprite.xScale = 0.7 + 0.5 * counter;
//     }
//   }
// }