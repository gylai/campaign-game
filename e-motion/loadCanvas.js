const video = document.querySelector("#video");

//video.width = window.innerWidth;

var gameScreen = document.querySelector("body");
var canvas = document.querySelector("#canvas");
var c = canvas.getContext("2d");

var vcanvas = document.querySelector("#vcanvas");
var vctx = vcanvas.getContext("2d");

var splash = document.getElementById("splash");

var speed = 1;
//var responsiveAddHeight = canvas.height > canvas.width ? canvas.height * 0.03 : canvas.height * 0.02;
var responsiveAddHeight = canvas.height * 0.008;
var responsiveInterval = 250;
var padLength = canvas.height > canvas.width ? canvas.width * 0.3 : canvas.height * 0.3;
var padMulti = 1; //paddle length multiplier
var padDec = false; // paddle decrease;
var x2multi = false;
var x3multi = false;

var spawnSize = canvas.height > canvas.width ? canvas.width * 0.1 : canvas.height * 0.1;

// if (canvas.height > canvas.width) {
//   speed = 8;
// 
//   if (isMobile() !== null) {
//     //responsiveAddHeight = 75;
//     //responsiveInterval = 100;
//   }
// } else speed = 5;

console.log(
    "ismobile",
    isMobile(),
    navigator.userAgent,
    responsiveAddHeight,
    responsiveInterval,
    speed
);

var model;
var handX, handY;
var paddleX;
var point = 0;

var timeOutLong;
var timeOutShort;
var timeOutStatus;
var timeOutDelay;
var longCount = 0;
var shortCount = 0;
var boomCount = 0;
var img1count = 0;
var img2count = 0;
var img3count = 0;
var img4count = 0;
var img5count = 0;


var _catch = new Audio("./sound/catch.mp3");
var _boom = new Audio("./sound/boom.mp3");
var _bgm = new Audio("./sound/bgm.wav");
var _hurry = new Audio("./sound/hurry.wav");

if (_hurry) _hurry.loop = true;
if (_bgm) _bgm.loop = true;

//states====================
var isLoaded = false;
var isStart = false;
var isSplash = false;
var isPlay = false;
var isEnd = false;
var isGameCompleted = false;
var begin = 0;
//tutorial screen============
var isProceed = false;
var isLoading = true;
//==========================
var isDrop = false;
var mm = 01;
var ss = 00;
var totalsec = (mm * 60) + ss;
var currentsec = totalsec;
mm = parseInt(mm);
ss = parseInt(ss);
mm = mm < 10 ? "0" + mm : mm;
ss = ss < 10 ? "0" + ss : ss;
var isCameraReady = false;

c.fillStyle = "grey";
c.fillRect(0, 0, canvas.width, canvas.height);

//document.getElementById("splash-text").innerHTML = "Game is loading...";

function isMobile() {
    return (
        navigator.userAgent.match(/Android/i) ||
        navigator.userAgent.match(/webOS/i) ||
        navigator.userAgent.match(/iPhone/i) ||
        navigator.userAgent.match(/iPad/i) ||
        navigator.userAgent.match(/iPod/i) ||
        navigator.userAgent.match(/BlackBerry/) ||
        navigator.userAgent.match(/Windows Phone/i) ||
        navigator.userAgent.match(/ZuneWP7/i)
    );
}

var padColor = "black";


//========================================
// game setting
// 30 imgs added + power-ups 20
// 60 total secs

var spawnTime = 0;
// noImgs used for countdown remaining quantities
var noImgs = [
    noimg1 = 8,
    noimg2 = 8,
    noimg3 = 5,
    noimg4 = 5,
    noimg5 = 4,
    nolong = 5,
    noshort = 5,
    noboom = 10
];

for (let x = 0; x < noImgs.length; x++) {
    spawnTime += noImgs[x];
}
// setMaxPoint is the maximum point a player will get excluding booms
var setMaxPoint = 1000;
var boomPoint = setMaxPoint / 20;

//noImgsScore is used for setting score based on quantity
var noImgsScore = [
    img1point = ((setMaxPoint / 5) * //divide max point by 5. (eg. 1000/5 = 200)
        2 / 4) / //multiply by 2/4 is to ensure img1point is smallest of all (eg. 200 * 2/4 = 100)
    noImgs[0], //after getting the total point of img1, distribute the points among the quantity of img1 (eg. 100 / 60(qty of img1))
    img2point = ((setMaxPoint / 5) *
        3 / 4) / //(eg. 200 * 3/4 = 150)
    noImgs[1],
    img3point = (setMaxPoint / 5) / //4/4 = 1
    noImgs[2],
    img4point = ((setMaxPoint / 5) *
        5 / 4) /
    noImgs[3],
    img5point = ((setMaxPoint / 5) *
        6 / 4) /
    noImgs[4],
];

// console.log("img1 : " + noImgsScore[0]);
// console.log("img2 : " + noImgsScore[1]);
// console.log("img3 : " + noImgsScore[2]);
// console.log("img4 : " + noImgsScore[3]);
// console.log("img5 : " + noImgsScore[4]);
// console.log("boom : " + boomPoint);

// (totalsec - 2) to ensure the final spawn finish dropping
var spawnDelay = ((totalsec - 2) / spawnTime) * 1000;
// var spawnDelay = 3000;
// console.log(spawnDelay);
// console.log(totalsec);
//===============================


//Data collection ===============
var totalImg = noImgs.slice(0);
var perfTime = [];
var spawn = [];

var handLocations = [];
var touchTimeStart;
var touchTimeEnd, touchSec, touchMinute;
var recordTimeTouch = [];

var pageloadtime = Math.floor(Date.now() / 1000);
var starttime;
var at20sec;
var endtime;


function roundRectangle(_context, _x, _y, _width, _height, _radius, _fill, _stroke) {
    if (typeof _stroke === "undefined") {
        _stroke = true;
    }
    if (typeof _radius === "undefined") {
        _radius = 5;
    }
    if (typeof _radius === "number") {
        _radius = { tl: _radius, tr: _radius, bl: _radius, br: _radius };
    } else {
        var defaultRadius = { tl: 0, tr: 0, bl: 0, br: 0 };
        for (var side in defaultRadius) {
            _radius[side] = _radius[side] || defaultRadius[side];
        }
    }

    _context.beginPath();
    _context.strokeStyle = '#00FFFF';
    _context.shadowColor = '#00FFFF';
    _context.shadowBlur = 10;
    _context.moveTo(_x + _radius.tl, _y);
    _context.lineTo(_x + _width - _radius.tr, _y);
    _context.quadraticCurveTo(_x + _width, _y, _x + _width, _y + _radius.tr);
    _context.lineTo(_x + _width, _y + _height - _radius.br);
    _context.quadraticCurveTo(_x + _width, _y + _height, _x + _width - _radius.br, _y + _height);
    _context.lineTo(_x + _radius.bl, _y + _height);
    _context.quadraticCurveTo(_x, _y + _height, _x, _y + _height - _radius.bl);
    _context.lineTo(_x, _y + _radius.tl);
    _context.quadraticCurveTo(_x, _y, _x + _radius.tl, _y);
    _context.closePath();

    if (_fill) {
        _context.fill();
    }
    if (_stroke) {
        _context.stroke();
    }

}


// port over renderPrediction from handtrackjs
function renderPred(_prediction, _canvas, _context, _mediasource) {
    _context.clearRect(0, 0, _canvas.width, _canvas.height);
    _canvas.width = _mediasource.width;
    _canvas.height = _mediasource.height;
    _canvas.style.height = parseInt(_canvas.style.width) * (_mediasource.height / _mediasource.width).toFixed(2) + "px";
    _context.save();
    _context.scale(-1, 1);
    _context.translate(-_mediasource.width, 0);
    _context.drawImage(_mediasource, 0, 0, _mediasource.width, _mediasource.height);
    _context.restore();

    for (let i = 0; i < _prediction.length; i++) {
        const pred = _prediction[i];
        // _context.beginPath();
        // _context.fillStyle = "rgba(255,255,255,0.6)";

        // _context.fillRect(pred.bbox[0] + 1, pred.bbox[1]+1, pred.bbox[2]-1, 17*1.5);
        // _context.lineWidth = 2;

        roundRectangle(_context, pred.bbox[0], pred.bbox[1], pred.bbox[2], pred.bbox[3], 3, false, true);
    }
}