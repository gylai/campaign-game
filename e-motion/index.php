<?php
header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Catching Game V1.0.0</title>
    <link rel="stylesheet" href="./canvas.css" />
    <link rel="stylesheet" href="./animation.css" />
    <link rel="stylesheet" href="../css/loading.css">

    <link rel="shortcut icon" href="./icon.ico" type="image/x-icon" />

    <script src="https://cdn.jsdelivr.net/npm/handtrackjs@0.0.13/dist/handtrack.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script type="text/javascript" src="./ntc.js"></script>
    <script src="./confetti.js"></script>

    <audio id="complete" src="../audio/complete.mp3"></audio>
</head>

<body>

    <!------------------------------------------>
    <!-- Splash screen and tutorial -->
    <div class="loading">Loading...</div>

    <div id="index">
        <div id="splashscreen">
            <video autoplay muted loop poster="Assets/SplashScreen/01.jpg">
                <source src="Assets/SplashScreen/EMotion.mp4" type="video/mp4" />
                Your browser does not support the video tag
            </video>
        </div>
        <button id="proceed">
            start game
        </button>
        <button id="exit" onclick="parent.location.href='https://fuyoh-ads.com/covid-19/dashboard';">
            exit
        </button>
        <video id="tutorial">
            <source src="Assets/tutorial.mp4" type="video/mp4">
            Your browser does not support the video tag.
        </video>
    </div>


    <!------------------------------------------>
    <!-- Loading Screen -->

    <div id="loadingscreen">
        <img src="Assets/GameOver/02.jpg" id="loadingimg" />
        <img src="img/loadingbar21.gif" id="loadingbar" />
    </div>


    <!------------------------------------------>
    <!-- Game over screen -->

    <div class="divGameCompleted container-fullScreen" id="divGameCompleted">
        <img id="img-scoreboard-bg" class="img-scoreboard" />
        <p id="gameover-txt"></p>
    </div>

    <div class="container-fullScreen" id="gameover">
        <img id="gameoverui" src="Assets/GameOver/03.jpg" />
        <div id="gameovercontent">
            <p id="score-txt">score</p>
            <span id="score">0</span>
            <button id="share" data-layout="button" data-size="small">
                <a target="_blank" class="fb-xfbml-parse-ignore" id="fbshare-link" style="color:#FFF; text-decoration: none;">
                    share
                </a>
            </button>
            <button id="continue" onclick="parent.location.href='https://fuyoh-ads.com/covid-19/dashboard';">
                continue
            </button>
        </div>
    </div>

    <!-- Hardware Instruction -->

    <div id="blackscreen">
        <p></p>
    </div>

    <div id="hardware-container">
        <div id="camera-instruction">
            <img class="camera-range" src="Assets/Hardware/camera_range.png">
            <br><br>
            <p>Recommended to play this game with a range of 1.5m from your webcam</p>
        </div>
        <div id="num-person-instruction">
            <img class="num-person" src="Assets/Hardware/num_of_person.png">
            <br><br>
            <p>Please ensure ONLY ONE PLAYER with SUFFICIENT ROOM LIGHTING for the camera detection</p>
        </div>
        <div id="btn-container">
            <button id="btnSkipInstruction">Continue</button>
        </div>
    </div>

    <!------------------------------------------>
    <!-- Game -->

    <main>
        <video class="layer1" id="video"></video>
        <div id="videocontainer">
            <canvas id="vcanvas" class="videocanvas"></canvas>
        </div>
        <div class="layer1" id="gamecontainer">
            <canvas id="canvas"></canvas>
            <h3 id="points" class="layer1">Scores : 0</h3>
            <h3 id="status"></h3>
            <img id="sparkling" src="img/sparkling.gif">
            <img id="boom" src="img/boom.gif">
        </div>
        <div class="layer1" id="timer-con">
            <h3 id="timer"></h3>
        </div>
    </main>
    <p id="cmname" style="display:none;"></p>

    <script type="text/javascript">
        var img1php = "./img/1.png";
        var img2php = "./img/2.png";
        var img3php = "./img/3.png";
        var img4php = "./img/4.png";
        var img5php = "./img/5.png";
    </script>

    <script src="./matter.js"></script>
    <script src="./loadCanvas.js"></script>
    <script src="./loadHandtrack.js.js"></script>
    <script src="./state.js"></script>
    <script src="./play.js?version=<?= time() ?>"></script>
</body>

</html>

<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v11.0" nonce="YD8oRRIX"></script>
<script>
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    var url = new URL(window.location.href);
    var campaign_name = url.searchParams.get("campaign_name");

    if (campaign_name) {
        document.getElementById("cmname").innerHTML = campaign_name;
    } else {
        document.getElementById("cmname").innerHTML = "E-Motion";
    }
</script>