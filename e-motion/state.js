var check = setInterval(() => {
    if (isStart) {
        isSplash = true;
        state();
        clearInterval(check);
    }
}, 1000);

var soundVol = 0;
//Record loading time
var loadingStartTime = performance.now(),
    loadingEndTime,
    loadingTimeEllapse;

function getLoadingTimeEllapse() {
    loadingEndTime = performance.now();
    loadingTimeEllapse = Math.round((loadingEndTime - loadingStartTime) / 1000);
    console.log("Loading Time Ellapsed : " + loadingTimeEllapse);
}

function state() {
    if (isSplash) {
        setTimeout(() => {
            setTimeout(() => {
                getLoadingTimeEllapse(); //record time taken to load the game

                isLoading = false;
                if (isProceed === true) {
                    document.querySelector("#loadingscreen").style.display = "none";
                }
            }, 1000);
            isSplash = false;
            isPlay = true;
            state();
        }, 2000);
    } else if (isPlay) {
        document.getElementById("timer-con").style.display = "flex";
        document.getElementById("timer").innerHTML = mm + " : " + ss;
        document.getElementById("points").style.display = "block";
        //document.getElementById("speed").style.display = "block";
        document.getElementById("status").style.display = "block";
        play();
    } else if (isEnd) {
        if (isGameCompleted) return;
        isGameCompleted = true;

        let final_score = Math.round(point);
        endtime = Math.floor(Date.now() / 1000);
        if (
            img1count + img2count + img3count + img4count + img5count == spawnTime &&
            boomCount == 0
        ) {
            //spawnTime is the total number of imgs
            final_score = 1000;
        }

        let jsonRecord = dlData();
        window.parent.wsCreateScore(final_score, jsonRecord);

        usermedia.getTracks().forEach(function(track) {
            track.stop();
        });
        clearInterval(camerachecking);
        handTrack.stopVideo(video);

        _hurry.pause();
        document.getElementById("timer").innerHTML = "Time's Up!";
        c.fillStyle = "white";
        c.fillRect(0, 0, canvas.width, canvas.height);

        if (final_score < 1000)
            document.querySelector("#divGameCompleted > img").src =
            "Assets/GameOver/emotion(Timesup).jpg";
        else
            document.querySelector("#divGameCompleted > img").src =
            "Assets/GameOver/emotion(Perfect).jpg";
        document.querySelector("#score").innerHTML = final_score;

        setTimeout(() => {
            let rgba = 0;
            let dim = setInterval(() => {
                document.querySelector("body").style.backgroundColor =
                    "rgba(0,0,0," + rgba + ")";
                rgba += 0.05;

                if (rgba >= 0.7) clearInterval(dim);
            }, 10);

            document.querySelector("main").style.animation = "fadeout 1.5s";
            setTimeout(() => {
                document.querySelector("main").style.display = "none";
                document.querySelector(".container-fullScreen").style.display = "flex";
                // console.log(usermedia);
                var complete_bgm = document.getElementById("complete");
                complete_bgm.play();
                document.getElementById("divGameCompleted").style.display = "block";
                setTimeout(function() {
                    complete_bgm.pause();
                    scoreboard.play();
                    startConfetti(); // start confetti for scoreboard
                    document.getElementById("gameover").style.display = "flex";
                }, 3000);
            }, 1000);

            //Share button link
            var campaign_name = document.getElementById("cmname").innerHTML;
            document.getElementById("share").addEventListener("click", function() {
                let fburl =
                    "https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Ffuyoh-ads.com%2Fcovid-19%3Fogtitle%3DHighscore%2Bin%2B" +
                    campaign_name +
                    "%26ogdescription%3DFuyohhhhhh%2521%2BI%2Bjust%2Bscored%2B" +
                    final_score +
                    "%2Bin%2B" +
                    "E-Motion" +
                    "%2521%26ogimage%3Dhttps%253A%252F%252Ffuyoh-ads.com%252Fgame_web%252Fimage%252F04-emotion.jpg&amp;src=sdkpreparse";
                window.open(
                    fburl,
                    "_blank",
                    "height=300,width=400,left=550,top=10,resizable=yes,scrollbars=yes"
                );
            });

            var playername = null;
            /*   
              var playername = prompt(
                "Your Score is: " +
                  point +
                  ". Please enter your name to store it into database.",
                "Player"
              ); 
              if (!playername) {
                playername = "Player";
              }
              console.log(
                "Data to be stored into database: " + playername + ", " + point
              );
  
              window.parent.wsCreateScore(point,data);
              window.location.href = "./scoreboard.php";
              $.ajax({
                type: "POST",
                url: "pass.php",
                data: {
                  playername: playername,
                  point: point,
                  longCount: longCount,
                  shortCount: shortCount,
                  boomCount: boomCount,
                  id1: img1count,
                  id2: img2count,
                  id3: img3count,
                  id4: img4count,
                  id5: img5count,
                },
                success: (window.location.href = "./scoreboard.php"),
              }); 
      
              location.href = "./scoreboard.php";
              */
        }, 1000);
    }
}

function play() {
    isDrop = true;

    var x = setInterval(() => {
        //console.log(mm, ss);
        mm = parseInt(mm);
        ss = parseInt(ss);
        mm = mm < 10 ? "0" + mm : mm;
        ss = ss < 10 ? "0" + ss : ss;

        if (begin == 1) {
            document.getElementById("timer").innerHTML = mm + " : " + ss;
            ss--;
            currentsec--;
            if (ss < 0) {
                mm--;
                ss = 59;
            }

            if (mm == 0 && ss <= 2) timer2sec = true;

            if (mm <= 00 && ss <= 0) {
                isPlay = false;
                isEnd = true;
                isDrop = false;
                state();
                clearInterval(x);
            }
        }
    }, 1000);
}

//==========================================
//     Splash Screen and Tutorial
//==========================================
var proceed = document.querySelector("#proceed");
var tutorial = document.querySelector("#tutorial");
var exitBtn = document.querySelector("#exit");

let loadPercentages = 0;

setTimeout(function() {
    if (loadPercentages < 99) {
        alert("We detect you are having a slow connection, you might experience longer loading time or abnormal gaming experience, please find an alternative reliable connectivity.");
    }
}, 30000);
let checkbuffer = function() {
    if (loadPercentages < 99) {
        let range = 0;
        let bf = this.buffered;
        let time = this.currentTime;
        let loadPercentage = bf.end(range) / this.duration * 100;
        loadPercentages = loadPercentage;
    } else {
        tutorial.removeEventListener('timeupdate', checkbuffer);
    }
};
tutorial.addEventListener('timeupdate', checkbuffer);

window.onload = () => {
    pregameResize();
    tutorial.play();
    _bgm.volume = 0.3;
    _bgm.loop = true;
    _bgm.play();

    // proceed.style.display = "block"; //  debug use

    handTrack.load(modelParams).then((lmodel) => {
        model = lmodel;
    });
    startVideo();

    let playTutorialVideo = setInterval(() => {
        if (tutorial.played.length > 0) {
            document.querySelector("#splashscreen").style.backgroundColor =
                "transparent";
            document.querySelector("#splashscreen").style.display = "none";
            $(".loading").hide();
            clearInterval(playTutorialVideo);
        } else {
            tutorial.play();
            _bgm.play();
            console.log("Trying to play tutorial and BGM");
        }
    }, 2000);

    document.querySelector("#splashscreen").style.backgroundColor =
        "transparent";
    document.querySelector("#splashscreen").style.display = "none";
    tutorial.style.display = "block";
};

var userResized = false;
window.onresize = () => {
    if (userResized) return;
    userResized = true;

    if (
        confirm(
            "You resized your screen\nThe page will be reloaded to ensure your best user experience"
        )
    ) {
        location.reload(true);
    } else {
        location.reload(true);
    }
};

function pregameResize() {
    // w: width, h: height
    let w = window.innerWidth;
    let h = window.innerHeight;
    let targetWidth = w;
    if (h >= (w / 16) * 9) {
        $("#body").css({
            width: "100vw",
            height: "calc(100vw/16*9)",
        });
    } else {
        (targetWidth = (h / 9) * 16),
        $("body").css({
            width: "calc(100vh/9*16)",
            height: "100vh",
        });
    }
    document.documentElement.style.setProperty(
        "--target-width",
        targetWidth + "px"
    );

    gameScreen = document.querySelector("body");
    canvas.setAttribute("width", gameScreen.clientWidth);
    canvas.setAttribute("height", gameScreen.clientHeight);

    Body.setPosition(ground, {
        x: canvas.width / 2,
        y: canvas.height * 0.9,
    });
}



tutorial.onended = () => {
    tutorial.play();
    proceed.style.display = "block";
    exitBtn.style.display = "block";
};

document.querySelector("#btnSkipInstruction").onclick = () => {
    document.querySelector("#hardware-container").style.display = "none";
    document.querySelector("#blackscreen > p").innerHTML = "Ready.. </br >3";
    setTimeout(() => {
        document.querySelector("#blackscreen > p").innerHTML = "Set.. </br >2";
        setTimeout(() => {
            document.querySelector("#blackscreen > p").innerHTML = "Go!! </br >1";
            setTimeout(() => {
                document.querySelector("#blackscreen").style.display = "none";
                isLoaded = true; //ensures game starts after loading page ends
            }, 500);
        }, 1000);
    }, 1000);
};

proceed.onclick = () => {
    if (!isCameraReady) {
        alert(
            "The game is not ready yet, please retry after a few seconds.\n" +
            "*Please allow the camera access if you haven't done so yet."
        );
        // location.href = "../error/hardware_error_page.php?hardware=camera";
        return;
    }

    isProceed = true;
    tutorial.pause();
    document.querySelector("#index").style.display = "none";
    document.querySelector("#loadingscreen").style.display = "none";
    document.querySelector("#loadingbar").style.display = "none";
    document.querySelector("#hardware-container").style.display = "grid";
    document.querySelector("#blackscreen").style.display = "flex";
};