const modelParams = {
  //flipHorizontal: true, // flip e.g for video
  imageScaleFactor: 0.7, // reduce input image size for gains in speed.
  maxNumBoxes: 1, // maximum number of boxes to detect
  iouThreshold: 0.6, // ioU threshold for non-max suppression
  scoreThreshold: 0.65, // confidence threshold for predictions.
};

navigator.getUserMedia =
  navigator.getUserMedia ||
  navigator.webkitGetUserMedia ||
  navigator.mozGetUserMedia ||
  navigator.msGetUserMedia;

var isVideo;
var camerachecking;
var usermedia;

// handTrack built in method to start video stream
function startVideo() {
  navigator.getUserMedia(
    { video: true },
    (stream) => {
      video.srcObject = stream;
      usermedia = stream;
      handTrack.startVideo(video).then((status) => {
        if (status) {
          isCameraReady = true;
          camerachecking = setInterval(checkCamera, 1000);
          setInterval(runDetection, 100);
        } else {
          console.log("HandtrackJs fail to start video");
        }
      });
    },
    (err) => {
      parent.location.href = "../error/hardware_error_page.php?hardware=camera";
      console.log(err);
    }
  );
}

function checkCamera() {
  if (usermedia.active == true) {
    //do nothing
  } else {
    if (!isGameCompleted)
      parent.location.href = "../error/hardware_error_page.php?hardware=camera";
  }
}

function runRedirectToLobby() {
  parent.location.href = "https://fuyoh-ads.com/covid-19/dashboard/";
}

function runDetection() {
  //requestAnimationFrame(runDetection);
  if (model === undefined) return;
  if (isGameCompleted) return;

  model.detect(video).then((predictions) => {
    //model.renderPredictions(predictions, vcanvas, vctx, video);
    renderPred(predictions, vcanvas, vctx, video);

    var tid = setInterval(perfTime.push(model.getFPS()), 2000);
    var tid2 = setInterval(handLocations.push([handX, handY]), 2000);

    if (predictions.length !== 0 && isLoaded == true) {
      if (begin == 0) {
        touchTimeStart = performance.now();
        starttime = Math.floor(Date.now() / 1000);
      }

      let midX = predictions[0].bbox[0] + predictions[0].bbox[2] / 2;
      let midY = predictions[0].bbox[1] + predictions[0].bbox[3] / 2;
      handX = canvas.width * (midX / video.width);
      handY = canvas.height * (midY / video.height);
      begin = 1;
      updatePaddle(handX);
    }
    if (predictions == 0) {
      isStart = true;
    }
  });
}

/* // Temporary Only
function changeDetectionSpeed() {
  var e = document.getElementById("detectspeed");
  var speed = e.options[e.selectedIndex].value;
  console.log("speed is ", speed);
  setInterval(runDetection, parseInt(speed));
}
 */
