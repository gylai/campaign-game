<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Touchscreen Advertising - Flappy Bird</title>
    <meta name='viewport' 
     content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'>
    <link rel="stylesheet" href="js/vendor/bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" href="css/main.css" />



    <style>
.zoom {
  padding: 50px;
  transition: transform .2s; /* Animation */
  width: 100%;
  height: 100%;
  transform: scale(1.7);
}
body {
overflow: -moz-scrollbars-vertical;
overflow-x: hidden;
overflow-y: hidden;
background-color:#78c4cc;
background-image: url("images/bg-02.png");
background-repeat:round;
background-size:cover;
}
#gameover {
    z-index: 20;
    height: 100%;
    width: 100%;
    position: fixed;
    align-items: center;
    justify-content: center;
    animation: 3s fadeIn;
    -webkit-animation-name: finishScreenAnimate;
    -webkit-animation-duration: 0.6s;
    animation-name: finishScreenAnimate;
}
 #complete {
    z-index: 10;
    /* height:0%; */
    width: 100%;
    position: fixed;
    align-items: center;
    justify-content: center;
    animation: 2s fadeIn;
    -webkit-animation-name: finishScreenAnimate;
    -webkit-animation-duration: 0.6s;
    animation-name: finishScreenAnimate;
    /* transform:scale(0.8);
    margin-left:-150px; */
    
}
@keyframes fadeIn {
  99% {
    visibility: hidden;
  }
  100% {
    visibility: visible;
  }
}
#gameover > img {
    position: fixed;
    /* object-fit: contain; */
    width: 100%;
    height: 100%;
}
#timesup {
    z-index:19;
}
@-webkit-keyframes gameoveranimate {
    from {
        top: -300px;
        opacity: 0
    }

    to {
        top: 0;
        opacity: 1
    }
}
@keyframes finishScreenAnimate {
    from {
        top: -300px;
        opacity: 0
    }

    to {
        top: 0;
        opacity: 1;
        display:block

    }
}
#gameovercontent {
    position: fixed;
    margin-top:6em;
    /* width: 40vw;
    height: 20vw;
    display: grid;
    grid-column-gap: 1vw;
    padding-top: 6em;
    padding-left: 27em; */
    font-family: "Machine Gunk";
    color: #4F382E;
    
}
    #p1 {
        grid-column: span 2;
        margin-top:160px;
        width: 100%;
        height: 100%;
        color: #4F382E;
        margin-left: 6.2em;
        font-family: "Machine Gunk";
        font-size:38px;
        font-weight:bold;
        letter-spacing:3px;
    }

    @font-face{
        font-family:"Machine Gunk";
        src:url("css/Machine Gunk.otf");
    }

    #score {
        grid-column: span 2;
        font-size: 7.5vw;
        text-align: center;
        font-family: "Machine Gunk";
        color: #4F382E;
        /* margin-left: 110px; */
        margin-top:-0.4em;
        letter-spacing:5px;
    }

    #p2 {
        margin-top:-20px;
      margin-right: 5px;
        font-family: "Machine Gunk";
        font-size: 0.8em;
        color: #B2927E;
        text-align: right;
        letter-spacing:2px;
    }
    
    /* #highscore {
        font-size: 2.5em;
        margin-top:-30px;
        text-align: left;
        font-family: "Machine Gunk";
        color: #4F382E;
        margin-left:-8px;
    } */

    #gameovercontent button {
        border-radius: 15px;
       padding: 20px 90px;
    }
    #gameovercontent button p {
        font-family: "Machine Gunk";
        font-size: 1.5rem;
        color: white;
        margin-top:-10px;
        letter-spacing:3px;
    }
#gameovercontent #share {
    height:50px;
    width: 180px;
    background-image: linear-gradient(to bottom, #7E5B4A, #4F382E);
    border:brown;
    margin-left:100px;
    /*
    shadow: #000000 0.1vw;
    border: #FFFFFF 0.1vw, #000000 0.1vw;
    */
    margin-top:10px;
}


#gameovercontent #share p{
    margin-left:-35px;
}
#gameovercontent #continue {
    height:50px;
    width: 180px;
    background-image: linear-gradient(to bottom, #FEC22E, #FE7235);
    border:orange;
    margin-top:10px;
}
#gameovercontent #continue p{
    margin-left:-55px;
}
.border-img{
    background-image: url("./images/bg-10.png");
    background-repeat:round;
    background-size:cover;
    transform: scale(0.7);
    /* margin-left:110px; */
}
#ads{
     margin-left:-60px;
    margin-top:-3px; 
    height:100%;
    width: 100%;
    position: absolute;
    left:75%;
    top:48%;
}
#ads-image{
    position: fixed;
    margin-left:-340px;
    transform: rotate(0.7deg); 
    margin-top:-10px;
}


  </style>
  <style>
        /* Absolute Center Spinner */
        .loading {
            position: fixed;
            z-index: 999;
            height: 2em;
            width: 2em;
            overflow: show;
            margin: auto;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
        }

        /* Transparent Overlay */
        .loading:before {
            content: '';
            display: block;
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: radial-gradient(rgba(20, 20, 20,.8), rgba(0, 0, 0, .8));

            background: -webkit-radial-gradient(rgba(20, 20, 20,.8), rgba(0, 0, 0,.8));
        }

        /* :not(:required) hides these rules from IE9 and below */
        .loading:not(:required) {
            /* hide "loading..." text */
            font: 0/0 a;
            color: transparent;
            text-shadow: none;
            background-color: transparent;
            border: 0;
        }

        .loading:not(:required):after {
            content: '';
            display: block;
            font-size: 10px;
            width: 1em;
            height: 1em;
            margin-top: -0.5em;
            -webkit-animation: spinner 150ms infinite linear;
            -moz-animation: spinner 150ms infinite linear;
            -ms-animation: spinner 150ms infinite linear;
            -o-animation: spinner 150ms infinite linear;
            animation: spinner 150ms infinite linear;
            border-radius: 0.5em;
            -webkit-box-shadow: rgba(255,255,255, 0.75) 1.5em 0 0 0, rgba(255,255,255, 0.75) 1.1em 1.1em 0 0, rgba(255,255,255, 0.75) 0 1.5em 0 0, rgba(255,255,255, 0.75) -1.1em 1.1em 0 0, rgba(255,255,255, 0.75) -1.5em 0 0 0, rgba(255,255,255, 0.75) -1.1em -1.1em 0 0, rgba(255,255,255, 0.75) 0 -1.5em 0 0, rgba(255,255,255, 0.75) 1.1em -1.1em 0 0;
            box-shadow: rgba(255,255,255, 0.75) 1.5em 0 0 0, rgba(255,255,255, 0.75) 1.1em 1.1em 0 0, rgba(255,255,255, 0.75) 0 1.5em 0 0, rgba(255,255,255, 0.75) -1.1em 1.1em 0 0, rgba(255,255,255, 0.75) -1.5em 0 0 0, rgba(255,255,255, 0.75) -1.1em -1.1em 0 0, rgba(255,255,255, 0.75) 0 -1.5em 0 0, rgba(255,255,255, 0.75) 1.1em -1.1em 0 0;
        }

        /* Animation */

        @-webkit-keyframes spinner {
            0% {
                -webkit-transform: rotate(0deg);
                -moz-transform: rotate(0deg);
                -ms-transform: rotate(0deg);
                -o-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                -moz-transform: rotate(360deg);
                -ms-transform: rotate(360deg);
                -o-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }
        @-moz-keyframes spinner {
            0% {
                -webkit-transform: rotate(0deg);
                -moz-transform: rotate(0deg);
                -ms-transform: rotate(0deg);
                -o-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                -moz-transform: rotate(360deg);
                -ms-transform: rotate(360deg);
                -o-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }
        @-o-keyframes spinner {
            0% {
                -webkit-transform: rotate(0deg);
                -moz-transform: rotate(0deg);
                -ms-transform: rotate(0deg);
                -o-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                -moz-transform: rotate(360deg);
                -ms-transform: rotate(360deg);
                -o-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }
        @keyframes spinner {
            0% {
                -webkit-transform: rotate(0deg);
                -moz-transform: rotate(0deg);
                -ms-transform: rotate(0deg);
                -o-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                -moz-transform: rotate(360deg);
                -ms-transform: rotate(360deg);
                -o-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }
    </style>
  
  <audio id="completebgm" src="../audio/complete.mp3"></audio>
  <audio id="endingbgm" src="../audio/scoreboard(edited).mp3"></audio>
  </head>

  <body>
  <div class="loading">Loading&#8230;</div>
        <div id="ads" class=" ">
            <img src="images/bg-04.png" style="width:380px;height:280px;margin-top:-105px;margin-left:-25px;">
            <img id="ads-image" src="images/advertisement.png"  style="width:301px;height:157px" />
        </div>
      
        <div id="complete" class="w-100" style="display:none">
            <!-- <img id="gameoverui"  src="images/bg-09.jpg"/> -->
            <img id="fbcompleteImg" src="images/fbcomplete.jpg" style="display:none" class="w-100 h-100" />
            <img id="fbtimesupImg" src="images/fbtimesup.jpg" style="display:none" class="w-100 h-100" />
        </div>
            <!-- Game over screen-->
        <div id="gameover"   style="display:none" >
            <img id="gameoverui" class="w-100" src="images/bg-09.jpg"/>
            <div class="w-100 h-100 d-flex justify-content-center align-items-center">
                <div id="gameovercontent" class="scoreboard-content " style="padding-bottom:100px">
                
                    <div class="row text-center ">
                        <div class="col-12" style="font-size:2rem"> HIGH SCORE</div>
                        <div class="col-12" id="highscore" style="font-size:7rem"> </div>
                        <div class="col-6 text-right" >
                            <a target="_blank" class="fb-xfbml-parse-ignore" id="fbshare-link">
                                <button id="share" data-layout="button" data-size="small"><p>SHARE</p></button>
                            </a>
                        </div>
                        <div class="col-6 text-left" > <button id="continue"><a  onclick="parent.location.href='https://fuyoh-ads.com/covid-19/dashboard/';"  style="text-decoration:none"><p>CONTINUE</p></a></button></div>
                    </div>

          
            <!-- 
                    <div class="">
                        <p id="p1">SCORE</p>
                        <span id="score"></span>
                        <p id="p2">HIGH SCORE<br />(ALL TIME)</p><span id="highscore">0</span>
                    </div>
                    
                    
                    <button id="share"><p>SHARE</p></button>
                    <button id="continue"><a href="game.php"><p>CONTINUE</p></a></button>
                    <br> -->
                
                </div>
            </div>

            </div>
        </div>
        <div class=" d-flex justify-content-center align-items-center h-100 zoom"> 
            <div id="timer"></div>
         
            <div class="border-img">
                <canvas id="bird"  width="320" height="465" style="position: relative; margin: 3.0rem; padding-top:3px; border-radius: 10px;"></canvas>
             
            </div>
        </div>
        <span id="cmname" style="display:none"></span>

    <script src="js/vendor/modernizr-3.11.2.min.js"></script>

  </body>
</html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.slim.js"></script>
<script src="js/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="js/config.js"></script>
<script src="js/main.js"></script>
<script src="js/confetti.js"></script>
<script src="game.js?v=1.0"></script>
<script>

window.onload = function() {
	$(".loading").hide();
    startGame();
};

$('.no-zoom').bind('touchend', function(e) {
  e.preventDefault();
  // Add your code here. 
  $(this).click();
  // This line still calls the standard click event, in case the user needs to interact with the element that is being clicked on, but still avoids zooming in cases of double clicking.
})
</script>


<script async defer crossorigin="anonymous" src="https://connect.facebook.net/zh_CN/sdk.js#xfbml=1&version=v11.0" nonce="azQ05r8d"></script>
<script>(function(d, s, id) {
var js, fjs = d.getElementsByTagName(s)[0];
if (d.getElementById(id)) return;
js = d.createElement(s); js.id = id;
js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

    var url = new URL(window.location.href);
    var campaign_name = url.searchParams.get("campaign_name");
    if(campaign_name)
    {
        document.getElementById("cmname").innerHTML = campaign_name;
    }
    else{
        document.getElementById("cmname").innerHTML = "Flappy Bird";
    }
</script>

