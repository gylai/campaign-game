<!DOCTYPE html>
<html>
<head>
	<title>
	Flappy Bird
	</title>

  <link rel="stylesheet" href="css/animation.css" />
  <style>
    @font-face {
      font-family: "MachineGunk";
      src: url("./font/MachineGunk-nyqg.ttf") format('truetype');
    }
    .click{
        color:black;
        animation: 3s fadeIn;
        animation-fill-mode: forwards;
        visibility: hidden;
        }

        @keyframes fadeIn {
        99% {
         visibility: hidden;
            }

        100% {
          visibility: visible;
             }
        }
      body{
      background-image: url("images/01-flappybird.jpg");
      display: flex;
      background-size: cover;
      height: 100vh; /* viewport height = 100%, adjustment not recommended */
      width: 100vw; /* viewport width = 100%, adjustment not recommended */
      justify-content: center;
      align-items: center;
      margin: 0;
      overflow: -moz-scrollbars-vertical;
      overflow-x: hidden;
      overflow-y: hidden;
      }
     main {
     position: relative;
     height: 100vmin; /* minimum viewport 100%, can be adjusted */
     width: 100vmin;
     }
     p {
     /* font-family: 'Machine Gunk'; */
     color: #56362C;
     font-size: 3vw;
     text-align: center;
     position: absolute;
     top: 0%;
     }
     #splashscreen {
     height: 100%;
     width: 100%;
     z-index: 20;
     position: absolute;
     background-color: black
     }
     #splashscreen > video {
     background-color: black;
     object-fit: contain;
     width: 100%;
     height: 100%;
     }

     #loading {
     z-index: 21;
     height: auto;
     width: 30%;
     object-fit: contain;
     position: absolute;
     filter: sepia(100%) contrast(200%);
     }

    #tutorial{
      object-fit: contain;
      width: 100%;
      height: 100%;
      align-items: center;
    }

    .container{
    position: relative;
    }

   .player-buttons{
    height: 128px;
    left: 95%;
    margin: -365px -50px 0 -0px;
    position: fixed;
    top: 50%;
    width: 128px;
    z-index: 1;
    animation: 3s fadeIn;
    animation-fill-mode: forwards;
    visibility: hidden;
    }

    @font-face{
    font-family:"Machine Gunk";
    src:url("css/Machine Gunk.otf");
    }

   .player-buttons #proceed{
   background-image: linear-gradient(#FEC22E, #FE7235);
   border-radius: 17px 17px 17px 17px;
   width: 200px;
   height: 50px;
   bottom:360px;
   color:white;
   opacity: 0.4;
   transition: opacity 0.3s;
   -webkit-transition: opacity 0.3s;
   /* animation: slidein 1s; */
   font-family: "Machine Gunk";
   margin-left:-120px;
   font-size:1em;
   cursor:pointer;
   }

  .player-buttons #proceed:hover {
    opacity: 1;
    font-size:1.3em;
  }

#play:hover{
  opacity:1;
  font-size:1.5em;
}
  .exit-buttons{
    height: 128px;
    right: 95%;
    margin: -365px -50px 0 0px;
    position: fixed;
    top: 50%;
    width: 128px;
    z-index: 1;
    animation: 3s fadeIn;
    animation-fill-mode: forwards;
    visibility: hidden;
    }

    @font-face{
    font-family:"Machine Gunk";
    src:url("css/Machine Gunk.otf");
    }

   .exit-buttons .content{
    background-image: linear-gradient(#FEC22E, #FE7235);
    border-radius: 17px 17px 17px 17px;
    width: 200px;
    height: 50px;
    bottom:360px;
     color:white;
    opacity: 0.4;
    transition: opacity 0.3s;
    -webkit-transition: opacity 0.3s;
    /* animation: slidein 1s; */
    font-family: "Machine Gunk";
    /*margin-left:-30px;
    margin-top:-120em;*/
    font-size:1em;
    cursor:pointer;
   }

  .exit-buttons .content:hover {
    opacity: 1;
    font-size:1.3em;
  }


  #exit:hover{
      opacity:1;
      font-size:1.5em;
  }

  @media screen and (max-width: 1500px) {
    .exit-buttons{
      height: 128px;
      right: 95%;
      margin: -290px -50px 0 0px;
      position: fixed;
      top: 50%;
      width: 128px;
      z-index: 1;
      animation: 3s fadeIn;
      animation-fill-mode: forwards;
      visibility: hidden;
    }
   .player-buttons{
      height: 128px;
      left: 95%;
      margin: -290px -50px 0 -0px;
      position: fixed;
      top: 50%;
      width: 128px;
      z-index: 1;
      animation: 3s fadeIn;
      animation-fill-mode: forwards;
      visibility: hidden;
    }
  }

  </style>
    <style>
        /* Absolute Center Spinner */
        .loading {
            position: fixed;
            z-index: 999;
            height: 2em;
            width: 2em;
            overflow: show;
            margin: auto;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
        }

        /* Transparent Overlay */
        .loading:before {
            content: '';
            display: block;
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: radial-gradient(rgba(20, 20, 20,.8), rgba(0, 0, 0, .8));

            background: -webkit-radial-gradient(rgba(20, 20, 20,.8), rgba(0, 0, 0,.8));
        }

        /* :not(:required) hides these rules from IE9 and below */
        .loading:not(:required) {
            /* hide "loading..." text */
            font: 0/0 a;
            color: transparent;
            text-shadow: none;
            background-color: transparent;
            border: 0;
        }

        .loading:not(:required):after {
            content: '';
            display: block;
            font-size: 10px;
            width: 1em;
            height: 1em;
            margin-top: -0.5em;
            -webkit-animation: spinner 150ms infinite linear;
            -moz-animation: spinner 150ms infinite linear;
            -ms-animation: spinner 150ms infinite linear;
            -o-animation: spinner 150ms infinite linear;
            animation: spinner 150ms infinite linear;
            border-radius: 0.5em;
            -webkit-box-shadow: rgba(255,255,255, 0.75) 1.5em 0 0 0, rgba(255,255,255, 0.75) 1.1em 1.1em 0 0, rgba(255,255,255, 0.75) 0 1.5em 0 0, rgba(255,255,255, 0.75) -1.1em 1.1em 0 0, rgba(255,255,255, 0.75) -1.5em 0 0 0, rgba(255,255,255, 0.75) -1.1em -1.1em 0 0, rgba(255,255,255, 0.75) 0 -1.5em 0 0, rgba(255,255,255, 0.75) 1.1em -1.1em 0 0;
            box-shadow: rgba(255,255,255, 0.75) 1.5em 0 0 0, rgba(255,255,255, 0.75) 1.1em 1.1em 0 0, rgba(255,255,255, 0.75) 0 1.5em 0 0, rgba(255,255,255, 0.75) -1.1em 1.1em 0 0, rgba(255,255,255, 0.75) -1.5em 0 0 0, rgba(255,255,255, 0.75) -1.1em -1.1em 0 0, rgba(255,255,255, 0.75) 0 -1.5em 0 0, rgba(255,255,255, 0.75) 1.1em -1.1em 0 0;
        }

        /* Animation */

        @-webkit-keyframes spinner {
            0% {
                -webkit-transform: rotate(0deg);
                -moz-transform: rotate(0deg);
                -ms-transform: rotate(0deg);
                -o-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                -moz-transform: rotate(360deg);
                -ms-transform: rotate(360deg);
                -o-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }
        @-moz-keyframes spinner {
            0% {
                -webkit-transform: rotate(0deg);
                -moz-transform: rotate(0deg);
                -ms-transform: rotate(0deg);
                -o-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                -moz-transform: rotate(360deg);
                -ms-transform: rotate(360deg);
                -o-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }
        @-o-keyframes spinner {
            0% {
                -webkit-transform: rotate(0deg);
                -moz-transform: rotate(0deg);
                -ms-transform: rotate(0deg);
                -o-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                -moz-transform: rotate(360deg);
                -ms-transform: rotate(360deg);
                -o-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }
        @keyframes spinner {
            0% {
                -webkit-transform: rotate(0deg);
                -moz-transform: rotate(0deg);
                -ms-transform: rotate(0deg);
                -o-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                -moz-transform: rotate(360deg);
                -ms-transform: rotate(360deg);
                -o-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }
    </style>
</head>
<body>
<div class="loading">Loading&#8230;</div>

	<!-- <div class="hint">
	Click to play &#9836;
	</div>	 -->

  <div class="exit-buttons" id="exit" style="display:none">
    <a  onclick="parent.location.href='https://fuyoh-ads.com/covid-19/dashboard/';"><button class="content" >Exit</button></a>
  </div>


  <div class="player-buttons" id="play" style="display:none">
    <button id="proceed" >Start game</button>
  </div>

	<audio autoplay id="BGM" src="" loop>
		<source type="audio/mpeg">
		Your browser does not support the audio tag.
	</audio>

	<div id="splashscreen">
		<video autoplay muted loop poster="images/01-flappybird.jpg">
			<source src="images/FlappyBird.mp4" type="video/mp4" />
			Your browser does not support the video tag
		</video>
	</div>

  <div class="container">
	<video id="tutorial" autoplay muted loop >

		<source src="tutorial/FlappyBird.mp4" type="video/mp4">
		Your browser does not support the video tag.
	</video>

  </div>

	<script>
        let loadPercentages = 0;

		window.onload = () => {
			// setTimeout(function () {
			// 	document.querySelector("#splashscreen").style.backgroundColor = "transparent";
			// 	document.querySelector("#splashscreen").style.display = "none";
			// }, 1500);
		}

		var tutorial = document.querySelector("#tutorial");

		tutorial.onended = () => {
        // alert("here")
        //     document.getElementById("proceed").style.display = "block";
        //     document.getElementById("exit").style.display = "block";
        
        
		}

        var loading = false;
        tutorial.addEventListener('timeupdate', function () {
            if(!loading) {
                loading = true;
                console.log("Timer Counting....")
                setTimeout(function () {
                    if(loadPercentages < 98) {
                        alert("We detect you are having a slow connection, you might experience longer loading time or abnormal gaming experience, please find an alternative reliable connectivity.")
                        allowToPlay = false;
                    }
                }, 30000);
            }
            if (this.duration && loadPercentages <98)
            {
                let range = 0;
                let bf = this.buffered;
                let time = this.currentTime;

                while (!(bf.start(range) <= time && time <= bf.end(range))) {
                    range += 1;
                }
                let loadStartPercentage = bf.start(range) / this.duration;
                let loadEndPercentage = bf.end(range) / this.duration;
                let loadPercentage = (loadEndPercentage - loadStartPercentage) * 100;
                console.log(loadPercentage)

                loadPercentages = loadPercentage;
            }
            else
            {
                $(".loading").hide();

        
                document.querySelector("#splashscreen").style.backgroundColor =
                    "transparent";
                document.querySelector("#splashscreen").style.display = "none";

                document.getElementById("exit").style.display = "block";    
                document.getElementById("play").style.display = "block";
              
            }
        });



        document.getElementById("proceed").onclick = () => {
      var url = new URL(window.location.href);
      var campaign_name = url.searchParams.get("campaign_name");
      let campaign_name_value="campaign_name="
      if(campaign_name)
      {
        campaign_name_value+=campaign_name
      }

      window.location.href = "game.php?"+campaign_name_value;
      document.getElementById("proceed").style.display = "none";
      document.getElementById("exit").style.display = "none";
    }

    document.getElementById("exit").onclick = () => {
    onclick="parent.location.href='https://fuyoh-ads.com/covid-19/dashboard/';"
      document.getElementById("proceed").style.display = "none";
      document.getElementById("exit").style.display = "none";
    }

	</script>
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.slim.js"></script>

</body>
</html>
