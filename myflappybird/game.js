// SELECT CVS
const cvs = document.getElementById("bird");
const ctx = cvs.getContext("2d");

const complete_bgm = document.getElementById("completebgm");
const endingbgm = document.getElementById("endingbgm");

let countDownTimer = 120; //seconds
// GAME VARS AND CONSTS
let frames = 0;
const DEGREE = Math.PI / 180;

let spriteSrc = "./images/sprite.png";
//let logoSrc = "./images/unifi.png";
let logoSrc = ""; //tips image

// GAME STATE
const state = {
  current: 0,
  getReady: 0,
  game: 1,
  over: 2,
};

let totalRestart = 0;
let game_condition = "timesup";
attempt = 0;

//store in json
let totalJump = 0;
let totalpipes = 0;
let jump = 0;
let pipe_attempt = 0;
let total_score = 0;
let roundData = [];

let roundStart = "";

let xRatio = 1;
let yRatio = 1;
let wRatio = 3;
let hRatio = 3;

getWindowRatio(); //resize button

// START BUTTON COORD
let startBtn = {
  x: 120 * xRatio,
  y: 263 * yRatio,
  w: 83 * wRatio,
  h: 29 * hRatio,
};

window.onresize = function (event) {
  getWindowRatio();

  startBtn = {
    x: 120 * xRatio,
    y: 263 * yRatio,
    w: 83 * wRatio,
    h: 29 * hRatio,
  };
};

// SCORE
const score = {
  best: 0,
  value: 0,

  draw: function () {
    ctx.fillStyle = "white";
    ctx.strokeStyle = "#000";

    if (state.current == state.game) {
      ctx.lineWidth = 2;
      ctx.font = "35px Machine Gunk";
      ctx.fillText(this.value, cvs.width / 2, 60);
      ctx.strokeText(this.value, cvs.width / 2, 60);
    } else if (state.current == state.over) {
      // SCORE VALUE
      ctx.font = "25px Machine Gunk";
      ctx.fillText(this.value, 210, 186);
      ctx.strokeText(this.value, 210, 186);
      // BEST SCORE
      ctx.fillText(this.best, 210, 228);
      ctx.strokeText(this.best, 210, 228);
    }
  },

  reset: function () {
    this.value = 0;
  },
};

function loadAjaX(url, callback) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      console.log(this.responseText);

      let json = JSON.parse(this.responseText);

      if (json) {
        let settings = json.data;

        console.log(settings);

        for (var i = 0; i < settings.length; i++) {
          let data = settings[i];

          if (data.type == "radio") {
            let selection = data.data.selection;

            setBirdStyle(selection);
          } else if (data.type == "fixed_image") {
            // logoSrc = data.data.images[0];
            // setLogo(logo);
          }
        }
      }

      callback(true);
    }
  };
  xhttp.open("GET", url, true);
  xhttp.send();
}

loadAjaX(
  "https://fuyoh-ads.com/game_web/campaign_game/settings?activity_id=4",
  function () {
    initGameSetting();
  }
);

// LOAD SOUNDS
const SCORE_S = new Audio();
SCORE_S.src = "./audio/sfx_point.wav";

const FLAP = new Audio();
FLAP.src = "./audio/sfx_flap.wav";

const HIT = new Audio();
HIT.src = "./audio/sfx_hit.wav";

const SWOOSHING = new Audio();
SWOOSHING.src = "./audio/sfx_swooshing.wav";

const DIE = new Audio();
DIE.src = "./audio/sfx_die.wav";

function initGameSetting() {
  // LOAD SPRITE IMAGE
  let sprite = new Image();
  sprite.src = spriteSrc;

  let logo = new Image();
  logo.src = logoSrc;

  // CONTROL THE GAME
  cvs.addEventListener("click", function (evt) {
    switch (state.current) {
      case state.getReady:
        attempt++;
        state.current = state.game;
        score.value = 0;
        SWOOSHING.play();

        //record start date
        roundStart = new Date(
          new Date().getTime() - new Date().getTimezoneOffset() * 60 * 1000
        )
          .toJSON()
          .slice(0, 19)
          .replace("T", " ");

        break;
      case state.game:
        jump++;
        if (bird.y - bird.radius <= 0) return;
        bird.flap();
        FLAP.play();
        break;
      case state.over:
        let rect = cvs.getBoundingClientRect();
        let clickX = evt.clientX - rect.left;
        let clickY = evt.clientY - rect.top;

        // CHECK IF WE CLICK ON THE START BUTTON
        // if(clickX >= startBtn.x && clickX <= startBtn.x + startBtn.w && clickY >= startBtn.y && clickY <= startBtn.y + startBtn.h){

        pipes.reset();
        bird.speedReset();
        //score.reset();
        state.current = state.getReady;
        //}
        break;
    }
  });

  // BACKGROUND
  const bg = {
    sX: 0,
    sY: 0,
    w: 275,
    h: 226,
    x: 0,
    y: cvs.height - 226,

    draw: function () {
      ctx.drawImage(
        sprite,
        this.sX,
        this.sY,
        this.w,
        this.h,
        this.x,
        this.y,
        this.w,
        this.h
      );

      ctx.drawImage(
        sprite,
        this.sX,
        this.sY,
        this.w,
        this.h,
        this.x + this.w,
        this.y,
        this.w,
        this.h
      );
    },
  };

  // FOREGROUND
  const fg = {
    sX: 276,
    sY: 0,
    w: 224,
    h: 10,
    x: 0,
    y: cvs.height - 10,

    dx: 2,

    draw: function () {
      ctx.drawImage(
        sprite,
        this.sX,
        this.sY,
        this.w,
        this.h,
        this.x,
        this.y,
        this.w,
        this.h
      );

      ctx.drawImage(
        sprite,
        this.sX,
        this.sY,
        this.w,
        this.h,
        this.x + this.w,
        this.y,
        this.w,
        this.h
      );
    },

    update: function () {
      if (state.current == state.game) {
        this.x = (this.x - this.dx) % (this.w / 2);
      }
    },
  };

  // BIRD
  const bird = {
    animation: [
      { sX: 276, sY: 112 },
      { sX: 276, sY: 139 },
      { sX: 276, sY: 164 },
      { sX: 276, sY: 139 },
    ],
    x: 50,
    y: 150,
    w: 34,
    h: 26,

    radius: 12,

    frame: 0,

    gravity: 0.18,
    jump: 3.5,
    speed: 0,
    rotation: 0,

    draw: function () {
      let bird = this.animation[this.frame];

      ctx.save();
      ctx.translate(this.x, this.y);
      ctx.rotate(this.rotation);
      ctx.drawImage(
        sprite,
        bird.sX,
        bird.sY,
        this.w,
        this.h,
        -this.w / 2,
        -this.h / 2,
        this.w,
        this.h
      );

      ctx.restore();
    },

    flap: function () {
      this.speed = -this.jump;
    },

    update: function () {
      // if(localStorage.getItem("countDownTimer") <= 0) {
      //     timeout()
      // }
      // IF THE GAME STATE IS GET READY STATE, THE BIRD MUST FLAP SLOWLY
      this.period = state.current == state.getReady ? 10 : 5;
      // WE INCREMENT THE FRAME BY 1, EACH PERIOD
      this.frame += frames % this.period == 0 ? 1 : 0;
      // FRAME GOES FROM 0 To 4, THEN AGAIN TO 0
      this.frame = this.frame % this.animation.length;

      if (state.current == state.getReady) {
        this.y = 150; // RESET POSITION OF THE BIRD AFTER GAME OVER
        this.rotation = 0 * DEGREE;
      } else {
        this.speed += this.gravity;
        this.y += this.speed;

        if (this.y + this.h / 2 >= cvs.height - fg.h) {
          this.y = cvs.height - fg.h - this.h / 2;
          if (state.current == state.game) {
            state.current = state.over;

            // 1 round end test
            storeJSONdata();

            DIE.play();
            totalRestart++;

            let data = { totalJump, totalRestart };
            console.log(data);

            //window.parent.wsCreateScore(score.value,data)
          }
        }

        // IF THE SPEED IS GREATER THAN THE JUMP MEANS THE BIRD IS FALLING DOWN
        if (this.speed >= this.jump) {
          this.rotation = 90 * DEGREE;
          this.frame = 1;
        } else {
          this.rotation = -25 * DEGREE;
        }
      }
    },
    speedReset: function () {
      this.speed = 0;
    },
  };

  // GET READY MESSAGE
  const getReady = {
    sX: 0,
    sY: 228,
    w: 173,
    h: 152,
    x: cvs.width / 2 - 173 / 2,
    y: 80,

    draw: function () {
      if (state.current == state.getReady) {
        ctx.drawImage(
          sprite,
          this.sX,
          this.sY,
          this.w,
          this.h,
          this.x,
          this.y,
          this.w,
          this.h
        );
      }
    },
  };

  // GAME OVER MESSAGE
  const gameOver = {
    sX: 175,
    sY: 228,
    w: 225,
    h: 202,
    x: cvs.width / 2 - 225 / 2,
    y: 90,

    draw: function () {
      if (state.current == state.over) {
        //1 round end

        ctx.drawImage(
          sprite,
          this.sX,
          this.sY,
          this.w,
          this.h,
          this.x,
          this.y,
          this.w,
          this.h
        );
      }
    },
  };

  // PIPES
  const pipes = {
    position: [],

    top: {
      sX: 553,
      sY: 0,
    },
    bottom: {
      sX: 502,
      sY: 0,
    },

    w: 53,
    h: 400,
    gap: 150,
    maxYPos: -150,
    dx: 2,

    draw: function () {
      for (let i = 0; i < this.position.length; i++) {
        let p = this.position[i];

        let topYPos = p.y;
        let bottomYPos = p.y + this.h + this.gap;

        // top pipe
        ctx.drawImage(
          sprite,
          this.top.sX,
          this.top.sY,
          this.w,
          this.h,
          p.x,
          topYPos,
          this.w,
          this.h
        );

        // bottom pipe
        ctx.drawImage(
          sprite,
          this.bottom.sX,
          this.bottom.sY,
          this.w,
          this.h,
          p.x,
          bottomYPos,
          this.w,
          this.h
        );
      }
    },

    update: function () {
      if (state.current !== state.game) return;

      if (frames % 100 == 0) {
        this.position.push({
          x: cvs.width,
          y: this.maxYPos * (Math.random() + 1),
        });
      }
      for (let i = 0; i < this.position.length; i++) {
        let p = this.position[i];

        let bottomPipeYPos = p.y + this.h + this.gap;

        // COLLISION DETECTION
        // TOP PIPE
        if (
          bird.x + bird.radius > p.x &&
          bird.x - bird.radius < p.x + this.w &&
          bird.y + bird.radius > p.y &&
          bird.y - bird.radius < p.y + this.h
        ) {
          if (state.current == state.game) {
            state.current = state.over;

            // 1 round end test2
            storeJSONdata();

            HIT.play();
            totalRestart++;

            let data = { totalJump, totalRestart };
            console.log(data);

            //window.parent.wsCreateScore(score.value,data)
          }
        }
        // BOTTOM PIPE
        if (
          bird.x + bird.radius > p.x &&
          bird.x - bird.radius < p.x + this.w &&
          bird.y + bird.radius > bottomPipeYPos &&
          bird.y - bird.radius < bottomPipeYPos + this.h
        ) {
          if (state.current == state.game) {
            state.current = state.over;

            // 1 round end test
            storeJSONdata();
            HIT.play();
            totalRestart++;

            let data = { totalRestart };
            console.log(data);

            //window.parent.wsCreateScore(score.value,data)
          }
        }

        // MOVE THE PIPES TO THE LEFT
        p.x -= this.dx;

        // if the pipes go beyond canvas, we delete them from the array
        if (p.x + this.w <= 0) {
          this.position.shift();
          score.value += 50;
          pipe_attempt++;
          SCORE_S.play();
          score.best = Math.max(score.value, score.best);

          if (score.value >= 1000) {
            //score.value += 50;
            clearInterval(startCountDownGame);
            endGameAction(true);
          }

          //localStorage.setItem("best", score.best);
        }
      }
    },

    reset: function () {
      this.position = [];
    },
  };

  const medal = {
    sX: 359,
    sY: 157,
    w: 45,
    h: 45,
    x: 72,
    y: 175,

    draw: function () {
      if (
        state.current == state.over &&
        score.value >= 10 &&
        score.value < 20
      ) {
        ctx.drawImage(
          sprite,
          this.sX,
          this.sY,
          this.w,
          this.h,
          this.x,
          this.y,
          this.w,
          this.h
        );
      }
      if (
        state.current == state.over &&
        score.value >= 20 &&
        score.value < 30
      ) {
        ctx.drawImage(
          sprite,
          this.sX,
          this.sY - 46,
          this.w,
          this.h,
          this.x,
          this.y,
          this.w,
          this.h
        );
      }
      if (
        state.current == state.over &&
        score.value >= 30 &&
        score.value < 40
      ) {
        ctx.drawImage(
          sprite,
          this.sX - 48,
          this.sY,
          this.w,
          this.h,
          this.x,
          this.y,
          this.w,
          this.h
        );
      }
      if (state.current == state.over && score.value >= 40) {
        ctx.drawImage(
          sprite,
          this.sX - 48,
          this.sY - 46,
          this.w,
          this.h,
          this.x,
          this.y,
          this.w,
          this.h
        );
      }
    },
  };

  // DRAW
  function draw() {
    ctx.fillStyle = "#70c5ce";
    //ctx.fillRect(0, 50, cvs.width, cvs.height);
    //ctx.fillRect(50, 0, cvs.width, cvs.height);
    ctx.fillRect(0, 0, cvs.width, cvs.height);
    bg.draw();
    pipes.draw();
    fg.draw();
    bird.draw();
    getReady.draw();
    gameOver.draw();
    medal.draw();
    score.draw();
    ctx.drawImage(logo, cvs.width / 2 - 300 / 2, cvs.height - 100);
    // ctx.drawImage(logo, cvs.width/2-100/2, cvs.height-(100), 100, 100);
  }

  // UPDATE
  function update() {
    bird.update();
    fg.update();
    pipes.update();
  }

  // LOOP
  function loop() {
    update();
    draw();
    frames++;

    refreshcountDownTimer();

    requestAnimationFrame(loop);
  }

  loop();

  // delaycountDownTimer();
}

//resize button size and position
function getWindowRatio() {
  //default
  xRatio = 1;
  yRatio = 1;
  wRatio = 1;
  hRatio = 1;

  var windowWidth =
    window.innerWidth ||
    document.documentElement.clientWidth ||
    document.body.clientWidth;
  var windowHeight =
    window.innerHeight ||
    document.documentElement.clientHeight ||
    document.body.clientHeight;
  //&&windowHeight=="1920"

  console.log("detect width " + windowWidth);
  if (windowWidth <= "1080" || windowWidth >= "1000") {
    //match with css     @media only screen and (min-width: 1080px) and (min-height: 1920px )
    xRatio = 3.2;
    yRatio = 3.4;
    wRatio = 2.4;
    hRatio = 2.4;
  }
}

function wsGameService(type, callback) {
  state.current = state.over;
  let data = {};

  switch (type) {
    case "end_game":
      data.score = score.value;
      data.data = { totalJump, totalRestart };
      callback(data);
      break;
    default:
      alert("invalid request");
  }
}

function setBirdStyle(selection) {
  for (var i = 0; i < selection.length; i++) {
    let data = selection[i];
    if (data.checked == true) {
      spriteSrc = "./images/" + data.value;
    }
    console.log(spriteSrc);
  }
}

function endGameAction(completeGame = false) {
  state.current = state.over;
  //diplay finish div
  document.getElementById("complete").style.display = "block";

  if (completeGame) {
    //reach 1000
    document.getElementById("fbcompleteImg").style.display = "block";
  } else {
    document.getElementById("fbtimesupImg").style.display = "block";
  }
  //document.getElementById("complete").style.display = "block";

  if (score.best >= 1000) {
    score.best = 1000;
  }

  let json = {
    result: {
      score: score.best,
      total_jump: totalJump,
      total_pipe_attempt: totalpipes,
    },
    round: roundData,
  };
  console.log(json);

  if (window.location !== window.parent.location) {
    window.parent.wsCreateScore(score.best, json);
  }

  complete_bgm.play();
  //endingbgm.play();

  var campaign_name = document.getElementById("cmname").innerHTML;
  document.getElementById("share").addEventListener("click", function () {
    let fburl =
      "https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Ffuyoh-ads.com%2Fcovid-19%3Fogtitle%3DHighscore%2Bin%2B" +
      campaign_name +
      "%26ogdescription%3DFuyohhhhhh%2521%2BI%2Bjust%2Bscored%2B" +
      Math.round(point) +
      "%2Bin%2B" +
      "Flappy Bird" +
      "%2521%26ogimage%3Dhttps%253A%252F%252Ffuyoh-ads.com%252Fgame_web%252Fimage%252F04-emotion.jpg&amp;src=sdkpreparse";
    window.open(
      fburl,
      "_blank",
      "height=300,width=400,left=550,top=10,resizable=yes,scrollbars=yes"
    );
  });

  //Share button link
  var campaign_name = document.getElementById("cmname").innerHTML;
  document.getElementById("share").addEventListener("click", function () {
    let fburl =
      "https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Ffuyoh-ads.com%2Fcovid-19%3Fogtitle%3DHighscore%2Bin%2B" +
      campaign_name +
      "%26ogdescription%3DFuyohhhhhh%2521%2BI%2Bjust%2Bscored%2B" +
      score.best +
      "%2Bin%2B" +
      "Flappy Bird" +
      "%2521%26ogimage%3Dhttps%253A%252F%252Ffuyoh-ads.com%252Fgame_web%252Fimage%252F01-flappybird.jpg&amp;src=sdkpreparse";
    window.open(
      fburl,
      "_blank",
      "height=300,width=400,left=550,top=10,resizable=yes,scrollbars=yes"
    );
  });

  // document
  //   .getElementById("share")
  //   .setAttribute(
  //     "data-href",
  //     "https://fuyoh-ads.com/campaign_landing_page?ogtitle=Highscore in " +
  //       campaign_name +
  //       "&amp;ogdescription=Fuyohhhhhh! I just scored " +
  //       score.best +
  //       " in Flappy Bird!&amp;ogimage=https://fuyoh-ads.com/game_web/image/01-flappybird.jpg"
  //   );
  // document
  //   .getElementById("fbshare-link")
  //   .setAttribute(
  //     "href",
  //     "https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Ffuyoh-ads.com%2Fcovid-19%3Fogtitle%3DHighscore%2Bin%2B" +
  //       campaign_name +
  //       "%26ogdescription%3DFuyohhhhhh%2521%2BI%2Bjust%2Bscored%2B" +
  //       score.best +
  //       "%2Bin%2BFlappy%2BBird%2521%26ogimage%3Dhttps%253A%252F%252Ffuyoh-ads.com%252Fgame_web%252Fimage%252F01-flappybird.jpg&amp;src=sdkpreparse"
  //   );

  setTimeout(function () {
    complete_bgm.pause();
    var gameending_bgm = document.getElementById("endingbgm");
    gameending_bgm.play();
    startConfetti();
    $("#gameover").show();

    storeJSONdata();
  }, 2000);

  //display best score
  $("#highscore").html(score.best);
  $("#score").html(score.value);
}

//count down countDownTimer
let startCountDownGame;
function startGame(){
  startCountDownGame = setTimeout(
    endGameAction,
    countDownTimer * 1000 + 1000
  );

  
  // Update the count down every 1 second
  var updateScoreView = setInterval(function () {
    let minutes = parseInt(countDownTimer / 60) || 00;
    let seconds = countDownTimer % 60;

    currentTime =
      (minutes < 10 ? "0" + minutes : minutes) +
      ":" +
      (seconds < 10 ? "0" + seconds : seconds);

    countDownTimer--;
    // If the count down is finished, write some text
    if (countDownTimer == 0) {
      console.log("time up");
      clearInterval(updateScoreView);
    }
  }, 1000);

}


let currentTime = "";
function refreshcountDownTimer() {
  ctx.fillStyle = "#FFF";
  ctx.strokeStyle = "black";
  ctx.lineWidth = 3;
  ctx.font = "35px Machine Gunk";

  ctx.fillText(currentTime, 230, 30);
  ctx.strokeText(currentTime, 230, 30);
}

function storeJSONdata() {
  let Data = {
    jump: jump,
    pipe_attempt: pipe_attempt,
    score: score.value,
    start_time: roundStart,
    end_time: new Date(
      new Date().getTime() - new Date().getTimezoneOffset() * 60 * 1000
    )
      .toJSON()
      .slice(0, 19)
      .replace("T", " "),
  };

  roundData.push(Data);
  totalJump += jump;
  totalpipes += pipe_attempt;
  total_score += score.value;
  pipe_attempt = 0;
  jump = 0;
}
